# JaVelo - Programming project
JaVelo is an application that can generate routes best suited for cycling in Switzerland. It was 
the project of the course "Pratique de la Programmation Orientée Objet" (Object-oriented 
programming practice) at EPFL, 2022. ([français](readme.md)).
![Application](javelo.png)
**Remark:** the data given in the folder "osm-data" only cover the west part of Switzerland. We 
will update the content once we get access to the entirety of the data. 

## Functionalities
The app can generate a route by clicking on two different position on the map. A pane containing 
the route's profile will open showing you its length and information about the elevation. A 
point can be highlighted with the cursor (see the black line or white circle on the image).

The start point is green, the end point red. You can add multiple waypoints (they will appear 
blue). If a point is placed on the route itself, it will be a waypoint. Points can be moved by 
dragging them.

The map can obviously be moved by dragging, and the zoom level can be changed with the scroll 
wheel. The route can be exported to GPX using the "Fichier" menu on top.

The instructions are [here](https://cs108.epfl.ch/archive/22/archive.html) (under "Projet", French only).

## Running the program
You will need JavaFX 18. Modify the run configuration for "JaVelo" such that they are similar to 
these (VM options):
```
--module-path /Users/wolfiy/software/java/javafx-sdk-18/lib/ --add-modules javafx.controls
```
then click run. For detailed instructions (French only, but lots of images): 
[installation de JavaFX](https://cs108.epfl.ch/archive/22/g/openjfx.html).
