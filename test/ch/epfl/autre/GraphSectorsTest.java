package ch.epfl.autre;

import ch.epfl.javelo.data.GraphNodes;
import ch.epfl.javelo.data.GraphSectors;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.SwissBounds;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GraphSectorsTest {


    @Test
    void testSectWithNormalValue2() {
        GraphSectors s = graphBuild2();

        ArrayList<GraphSectors.Sector> sectExpected = new ArrayList<GraphSectors.Sector>();

        sectExpected.add(new GraphSectors.Sector((258 - 128) * 2, (258 - 128) * 2 + 2));
        sectExpected.add(new GraphSectors.Sector(258 * 2, 258 * 2 + 2));
        sectExpected.add(new GraphSectors.Sector((257 - 128) * 2, (257 - 128) * 2 + 2));
        sectExpected.add(new GraphSectors.Sector(257 * 2, 257 * 2 + 2));

        List<GraphSectors.Sector> actual = s.sectorsInArea(new PointCh(SwissBounds.MIN_E + (2726.5625 * 2) + 5, SwissBounds.MIN_N + 2 * 1726.5625 + 5), 6);
        /*
        assertEquals(expected.get(0), actual.get(0));
        assertEquals(expected.get(1), actual.get(1));
        assertEquals(expected.get(2), actual.get(2));
        assertEquals(expected.get(3), actual.get(3));
        */

        assertEquals(4, actual.size());

        boolean toTest = false;

        for (int j = 0; j < sectExpected.size(); j++) {
            toTest = false;
            for (GraphSectors.Sector i : actual) {
                if (i.equals(actual.get(j))) {
                    toTest = true;
                }
            }
            assertTrue(toTest);
        }

    }

    @Test
    void testSectWithNormalValue3() {
        GraphSectors s = graphBuild2();

        ArrayList<GraphSectors.Sector> sectExpected = new ArrayList<GraphSectors.Sector>();

        sectExpected.add(new GraphSectors.Sector((390 - 128) * 2, (390 - 128) * 2 + 2));
        sectExpected.add(new GraphSectors.Sector(390 * 2, 390 * 2 + 2));
        sectExpected.add(new GraphSectors.Sector((389 - 128) * 2, (389 - 128) * 2 + 2));
        sectExpected.add(new GraphSectors.Sector(389 * 2, 389 * 2 + 2));

        List<GraphSectors.Sector> actual = s.sectorsInArea(new PointCh(SwissBounds.MIN_E + (2726.5625 * 5) + 8, SwissBounds.MIN_N + 3 * 1726.5625 + 8), 9);
        /*
        assertEquals(expected.get(0), actual.get(0));
        assertEquals(expected.get(1), actual.get(1));
        assertEquals(expected.get(2), actual.get(2));
        assertEquals(expected.get(3), actual.get(3));
        */

        assertEquals(4, actual.size());

        boolean toTest = false;

        for (int j = 0; j < sectExpected.size(); j++) {
            toTest = false;
            for (GraphSectors.Sector i : actual) {
                if (i.equals(actual.get(j))) {
                    toTest = true;
                }
            }
            assertTrue(toTest);
        }

    }

    @Test
    void testSectWithNormalValue4() {
        GraphSectors s = graphBuild2();

        ArrayList<GraphSectors.Sector> sectExpected = new ArrayList<GraphSectors.Sector>();

        sectExpected.add(new GraphSectors.Sector((258 - 128) * 2, (258 - 128) * 2 + 2));
        sectExpected.add(new GraphSectors.Sector(258 * 2, 258 * 2 + 2));
        sectExpected.add(new GraphSectors.Sector((257 - 128) * 2, (257 - 128) * 2 + 2));
        sectExpected.add(new GraphSectors.Sector(257 * 2, 257 * 2 + 2));

        List<GraphSectors.Sector> actual = s.sectorsInArea(new PointCh(SwissBounds.MIN_E + (2726.5625 * 2) + 10, SwissBounds.MIN_N + 2 * 1726.562 + 8), 11);
        /*
        assertEquals(expected.get(0), actual.get(0));
        assertEquals(expected.get(1), actual.get(1));
        assertEquals(expected.get(2), actual.get(2));
        assertEquals(expected.get(3), actual.get(3));
        */

        assertEquals(4, actual.size());

        boolean toTest = false;

        for (int j = 0; j < sectExpected.size(); j++) {
            toTest = false;
            for (GraphSectors.Sector i : actual) {
                if (i.equals(actual.get(j))) {
                    toTest = true;
                }
            }
            assertTrue(toTest);
        }

    }

    GraphSectors graphBuild() {
        ByteBuffer byteBuff = ByteBuffer.allocate(128 * 128 * 6);

        for (int i = 0; i < byteBuff.capacity() / 6; i++) {
            byteBuff.putInt(i);
            byteBuff.putShort((short) 1);
        }

        return new GraphSectors(byteBuff);
    }

    GraphSectors graphBuild2() {
        ByteBuffer byteBuff = ByteBuffer.allocate(128 * 128 * 6);

        for (int i = 0; i < byteBuff.capacity() / 6; i++) {
            byteBuff.putInt(2 * i);
            byteBuff.putShort((short) 1);
        }

        return new GraphSectors(byteBuff);
    }


    @Test
    void methodsGraphSectorsWorksGivenTest2() {
        ByteBuffer buffer = ByteBuffer.wrap(new byte[]{
                0, 0, 0, 16, 0, 20});
        GraphSectors ns = new GraphSectors(buffer);
        int idStartNode = buffer.getInt(0);

        int idEndNode = idStartNode + Short.toUnsignedInt(buffer.getShort(4));
        GraphSectors.Sector sector = new GraphSectors.Sector(idStartNode, idEndNode);
        List<GraphSectors.Sector> expected0 = new ArrayList<GraphSectors.Sector>();
        expected0.add(sector);
        List<GraphSectors.Sector> actual0 = ns.sectorsInArea(new PointCh(2485100, 1075100), 100);
        assertEquals(expected0, actual0);

        ByteBuffer buffer1 = ByteBuffer.wrap(new byte[]{
                0, 0, 0, 16, 0, 20, 0, 0, 0, 21, 0, 12});
        GraphSectors ns1 = new GraphSectors(buffer1);
        int idEndNode1 = buffer1.getInt(6) + Short.toUnsignedInt(buffer1.getShort(10));
        GraphSectors.Sector sector1 = new GraphSectors.Sector(buffer1.getInt(6), idEndNode1);
        List<GraphSectors.Sector> expected1 = new ArrayList<GraphSectors.Sector>();
        expected1.add(sector1);
        List<GraphSectors.Sector> actual1 = ns1.sectorsInArea(new PointCh(2_488_050, 1_076_050), 50);
        assertEquals(expected1, actual1);

        //ns.sectorsInArea(new PointCh(2505000, 1085000), 5000);
    }

    @Test
    void methodsGraphSectorsWorksGivenTest() {
        IntBuffer b = IntBuffer.wrap(new int[]{
                2_600_000 << 4,
                1_200_000 << 4,
                0x2_000_1234
        });
        GraphNodes ns = new GraphNodes(b);
        assertEquals(1, ns.count());
        assertEquals(2_600_000, ns.nodeE(0));
        assertEquals(1_200_000, ns.nodeN(0));
        assertEquals(2, ns.outDegree(0));
        assertEquals(0x1234, ns.edgeId(0, 0));
        assertEquals(0x1235, ns.edgeId(0, 1));
    }

    @Test
    void methodsGraphSectorsWorksAyaTest() {
        IntBuffer b = IntBuffer.wrap(new int[]{
                2_536_263 << 4,
                1_215_736 << 4,
                0x2_918_1873,
                1_297_183 << 4,
                2_015_772 << 4,
                0x1_803_0925
        });
        GraphNodes ns = new GraphNodes(b);
        assertEquals(2, ns.count());
        assertEquals(2_536_263, ns.nodeE(0));
        assertEquals(1_215_736, ns.nodeN(0));
        assertEquals(2, ns.outDegree(0));
        assertEquals(0x918_1873, ns.edgeId(0, 0));
        assertEquals(Integer.valueOf(Integer.toHexString(0x918_1873 + 1), 16), ns.edgeId(0, 1));
        assertEquals(1_297_183, ns.nodeE(1));
        assertEquals(2_015_772, ns.nodeN(1));
        assertEquals(1, ns.outDegree(1));
        assertEquals(0x803_0925, ns.edgeId(1, 0));
        //assertEquals(Integer.valueOf(Integer.toHexString(0x803_0925 + 1),16), ns.edgeId(1, 1));
        //assertThrows(AssertionError.class, ns.edgeId(1,1));
        //Piazza @259
    }

    @Test
    void methodsGraphSectorsWorksAyaTest2() {
        IntBuffer b = IntBuffer.wrap(new int[]{
                8_501_018 << 4,
                9_005_704 << 4,
                0x6_000_1029,
                3_108_826 << 4,
                4_010_002 << 4,
                0x2_800_0015,
                3_108_826 << 4,
                4_010_002 << 4,
                0x9_800_0010
        });
        GraphNodes ns = new GraphNodes(b);
        assertEquals(3, ns.count());
        assertEquals(8_501_018, ns.nodeE(0));
        assertEquals(9_005_704, ns.nodeN(0));
        assertEquals(6, ns.outDegree(0));
        assertEquals(0x1029, ns.edgeId(0, 0));
        assertEquals(Integer.valueOf(Integer.toHexString(0x1029 + 1), 16), ns.edgeId(0, 1));
        assertEquals(3_108_826, ns.nodeE(1));
        assertEquals(4_010_002, ns.nodeN(1));
        assertEquals(2, ns.outDegree(1));
        assertEquals(0x800_0015, ns.edgeId(1, 0));
        assertEquals(Integer.valueOf(Integer.toHexString(0x800_0015 + 1), 16), ns.edgeId(1, 1));
        assertEquals(3_108_826, ns.nodeE(2));
        assertEquals(4_010_002, ns.nodeN(2));
        assertEquals(9, ns.outDegree(2));
        assertEquals(0x800_0010, ns.edgeId(2, 0));
        assertEquals(Integer.valueOf(Integer.toHexString(0x800_0010 + 1), 16), ns.edgeId(2, 1));
    }

    @Test
    public void sectorsInAreaTest() {

        //Initialization of ByteBuffer.
        ByteBuffer b = ByteBuffer.allocate(98304);
        for (int i = 0; i < 16384; ++i) {
            b.putInt(i);
            b.putShort((short) 0);
        }
        GraphSectors gs = new GraphSectors(b);
        // Every sector's first node's value is the index of the sector in the buffer of sectors. Every sector contains exactly 0 nodes.


        //First test: we want to get only the sector #8256, located in the middle.
        //For that we use the fact that the equality between record objects was modified so that each attribute gets compared instead of the references.
        PointCh center1 = new PointCh(SwissBounds.MIN_E + 64 * (2.730 * 1000),
                SwissBounds.MIN_N + 64 * (1.730 * 1000));
        assertEquals(new GraphSectors.Sector(8256, 8256), gs.sectorsInArea(center1, 0).get(0)); //OK


        //Second test: we want to get only the sector in the top right corner (last one according to its index).
        assertEquals(new GraphSectors.Sector(16383, 16383), gs.sectorsInArea(new PointCh(SwissBounds.MIN_E + 127.5 * (2.730 * 1000),
                SwissBounds.MIN_N + 127.5 * (1.730 * 1000)), 0).get(0)); //OK


        //Third test: what if the drawn square gets passed the borders defined by the class Swissbound ?
        //If no error is thrown while using an absurdly high distance value (1000000000 meters), this may indicate that you treat this case appropriately.

        gs.sectorsInArea(center1, 1000000000);

        // We draw a square from the bottom left corner of the grid. We are supposed to list the sectors 0, 1, 128 and 129 without errors.

        PointCh center = new PointCh(SwissBounds.MIN_E + 0.1 * (2.730 * 1000),
                SwissBounds.MIN_N + 0.1 * (1.730 * 1000));
        assertEquals(new GraphSectors.Sector(0, 0), gs.sectorsInArea(center, 3000).get(0));

        assertEquals(new GraphSectors.Sector(1, 1), gs.sectorsInArea(center, 3000).get(1));

        assertEquals(new GraphSectors.Sector(128, 128), gs.sectorsInArea(center, 3000).get(2));

        assertEquals(new GraphSectors.Sector(129, 129), gs.sectorsInArea(center, 3000).get(3));

        // Hope you found this test useful ! - Léo.


        /* Additional test that is not based on the instructions. I wanted to verify that we could not input a negative value as a distance.*/
        assertThrows(IllegalArgumentException.class, () -> {
            gs.sectorsInArea(new PointCh(SwissBounds.MIN_E,SwissBounds.MIN_N),-1);
        });

    }
}