package ch.epfl.autre;

import ch.epfl.javelo.Bits;
import ch.epfl.javelo.data.GraphNodes;
import org.junit.jupiter.api.Test;

import java.nio.IntBuffer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GraphNodeTest {

    @Test
    public void countTest(){
        IntBuffer b = IntBuffer.wrap(new int[]{
                2_600_000 << 4,
                1_200_000 << 4,
                0x2_000_1234
        });
        GraphNodes ns = new GraphNodes(b);
        assertEquals(1, ns.count());

        IntBuffer b1 = IntBuffer.wrap(new int[]{
                2_600_000 << 4,
                1_200_000 << 4,
                0x2_100_1934,
                2_600_000 << 4,
                1_200_000 << 4,
                0x2_100_1924,
                2_600_000 << 4,
                1_200_000 << 4,
                0x2_100_1914,
                2_600_000 << 4,
                1_200_000 << 4,
                0x2_100_1714
        });

        GraphNodes ns1 = new GraphNodes(b1);
        assertEquals(4, ns1.count());
    }

    @Test
    public void nodeETest(){

        IntBuffer b = IntBuffer.wrap(new int[]{
                2_600_000 << 4,
                1_200_000 << 4,
                0x2_000_1234
        });
        GraphNodes ns = new GraphNodes(b);
        assertEquals(2_600_000, ns.nodeE(0));

        IntBuffer b1 = IntBuffer.wrap(new int[]{
                2_600_000 << 4,
                1_200_000 << 4,
                0x2_100_1934,
                2_610_000 << 4,
                1_200_000 << 4,
                0x2_100_1924,
                2_660_000 << 4,
                1_200_000 << 4,
                0x2_100_1914,
                2_625_000 << 4,
                1_200_000 << 4,
                0x2_100_1714
        });

        GraphNodes ns1 = new GraphNodes(b1);
        assertEquals(2_625_000, ns1.nodeE(3));
    }

    @Test
    public void nodeNTest(){

        IntBuffer b = IntBuffer.wrap(new int[]{
                2_600_000 << 4,
                1_200_000 << 4,
                0x2_000_1234
        });
        GraphNodes ns = new GraphNodes(b);
        assertEquals(1_200_000, ns.nodeN(0));

        IntBuffer b1 = IntBuffer.wrap(new int[]{
                2_600_000 << 4,
                1_280_000 << 4,
                0x2_100_1934,
                2_610_000 << 4,
                1_260_000 << 4,
                0x2_100_1924,
                2_660_000 << 4,
                1_205_000 << 4,
                0x2_100_1914,
                2_625_000 << 4,
                1_208_000 << 4,
                0x2_100_1714
        });

        GraphNodes ns1 = new GraphNodes(b1);
        assertEquals(1_205_000, ns1.nodeN(2));
    }

    @Test
    public void outDegreeTest(){

        IntBuffer b = IntBuffer.wrap(new int[]{
                2_600_000 << 4,
                1_200_000 << 4,
                0x2_000_1234
        });
        GraphNodes ns = new GraphNodes(b);
        assertEquals(2, ns.outDegree(0));

        IntBuffer b1 = IntBuffer.wrap(new int[]{
                2_600_000 << 4,
                1_280_000 << 4,
                0x2_100_1934,
                2_610_000 << 4,
                1_260_000 << 4,
                0b0101_0000_0000_0000_0000_0000_0000_0000,
                2_660_000 << 4,
                1_205_000 << 4,
                0x9_100_1914,
                2_625_000 << 4,
                1_208_000 << 4,
                0x2_100_1714
        });

        GraphNodes ns1 = new GraphNodes(b1);
        assertEquals(9, ns1.outDegree(2));
        assertEquals(5, ns1.outDegree(1));
    }

    @Test
    public void edgeIdNormalValuesTest(){

        IntBuffer b = IntBuffer.wrap(new int[]{
                2_600_000 << 4,
                1_200_000 << 4,
                0x2_000_1234
        });
        GraphNodes ns = new GraphNodes(b);
        assertEquals(0x1234, ns.edgeId(0, 0));
        assertEquals(0x1235, ns.edgeId(0, 1));

        IntBuffer b1 = IntBuffer.wrap(new int[]{
                2_600_000 << 4,
                1_280_000 << 4,
                0x2_100_1934,
                2_610_000 << 4,
                1_260_000 << 4,
                0b0101_0000_0000_0000_0000_0000_0000_0010,
                2_660_000 << 4,
                1_205_000 << 4,
                0x9_100_1914,
                2_625_000 << 4,
                1_208_000 << 4,
                0x2_100_1714
        });
        GraphNodes ns1 = new GraphNodes(b1);
        assertEquals(6, ns1.edgeId(1, 4));
        assertEquals(2, ns1.edgeId(1, 0));

    }
}
