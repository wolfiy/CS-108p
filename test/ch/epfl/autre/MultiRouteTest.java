package ch.epfl.autre;

import ch.epfl.javelo.Functions;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.SwissBounds;
import ch.epfl.javelo.routing.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleUnaryOperator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MultiRouteTest {

    @Test
    public void MultiRouteConstructionWorking(){


        List<Edge> edges = new ArrayList<Edge>();
        List<PointCh> points = new ArrayList<PointCh>();

        float[] typicalYasuoPlayer = {0.0f,-15f,8f,178f};

        PointCh pt1 = new PointCh(SwissBounds.MAX_E,SwissBounds.MIN_N);
        PointCh pt2 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt3 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt4 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt5 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt6 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt7 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt8 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt9 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt10 = new PointCh(SwissBounds.MAX_E-13100,SwissBounds.MIN_N);

        Edge edge1 = new Edge(0,1, pt1,pt2, 5800, Functions.sampled(typicalYasuoPlayer,5800));
        Edge edge2 = new Edge(1,2, pt3, pt4, 2300, Functions.sampled(typicalYasuoPlayer,2300));
        Edge edge3 = new Edge(2,3, pt5, pt6, 1100, Functions.sampled(typicalYasuoPlayer,1100));
        Edge edge4 = new Edge(3,4, pt7, pt8, 2200, Functions.sampled(typicalYasuoPlayer,2200));
        Edge edge5 = new Edge(4,5, pt9, pt10, 1700, Functions.sampled(typicalYasuoPlayer,1700));

        points.add(pt1);
        points.add(pt2);
        points.add(pt4);
        points.add(pt6);
        points.add(pt8);
        points.add(pt10);

        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);

        List<Route> segments = new ArrayList<>();

        SingleRoute sr1 = new SingleRoute(edges.subList(0,1));
        SingleRoute sr2 = new SingleRoute(edges.subList(1,2));

        List<Route> componentsOfMulti = new ArrayList<>();

        SingleRoute sr3 = new SingleRoute(edges.subList(2,3));
        SingleRoute sr4 = new SingleRoute(edges.subList(3,4));

        componentsOfMulti.add(sr3);
        componentsOfMulti.add(sr4);

        segments.add(sr1);
        segments.add(sr2);

        MultiRoute comp = new MultiRoute(componentsOfMulti);

        segments.add(comp);

        MultiRoute mt = new MultiRoute(segments);

        assertThrows(IllegalArgumentException.class, ()-> {

            List<Route> emptyOne = new ArrayList<>();

            MultiRoute mtBug = new MultiRoute(emptyOne);

        });

    }

    @Test
    public void MultiRouteImmuabilityWorking(){


        List<Edge> edges = new ArrayList<Edge>();
        List<PointCh> points = new ArrayList<PointCh>();

        float[] typicalYasuoPlayer = {0.0f,-15f,8f,178f};

        PointCh pt1 = new PointCh(SwissBounds.MAX_E,SwissBounds.MIN_N);
        PointCh pt2 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt3 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt4 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt5 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt6 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt7 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt8 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt9 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt10 = new PointCh(SwissBounds.MAX_E-13100,SwissBounds.MIN_N);

        Edge edge1 = new Edge(0,1, pt1,pt2, 5800, Functions.sampled(typicalYasuoPlayer,5800));
        Edge edge2 = new Edge(1,2, pt3, pt4, 2300, Functions.sampled(typicalYasuoPlayer,2300));
        Edge edge3 = new Edge(2,3, pt5, pt6, 1100, Functions.sampled(typicalYasuoPlayer,1100));
        Edge edge4 = new Edge(3,4, pt7, pt8, 2200, Functions.sampled(typicalYasuoPlayer,2200));
        Edge edge5 = new Edge(4,5, pt9, pt10, 1700, Functions.sampled(typicalYasuoPlayer,1700));

        points.add(pt1);
        points.add(pt2);
        points.add(pt4);
        points.add(pt6);
        points.add(pt8);
        points.add(pt10);

        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);

        List<Route> segments = new ArrayList<>();

        SingleRoute sr1 = new SingleRoute(edges.subList(0,2));
        SingleRoute sr2 = new SingleRoute(edges.subList(2,4));

        segments.add(sr1);
        segments.add(sr2);

        MultiRoute comp = new MultiRoute(segments);

        segments.add(comp);

        MultiRoute mt = new MultiRoute(segments);

        Edge attToModify = mt.edges().get(0);

        attToModify = new Edge(0,1, pt1,pt2, 848489, Functions.sampled(typicalYasuoPlayer,848489));


        assertEquals(mt.edges().get(0),edge1);

    }

    @Disabled
    @Test
    public void MultiRouteIndexOfSegmentAtWorking(){


        List<Edge> edges = new ArrayList<Edge>();
        List<PointCh> points = new ArrayList<PointCh>();

        float[] typicalYasuoPlayer = {0.0f,-15f,8f,178f};

        PointCh pt1 = new PointCh(SwissBounds.MAX_E,SwissBounds.MIN_N);
        PointCh pt2 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt3 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt4 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt5 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt6 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt7 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt8 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt9 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt10 = new PointCh(SwissBounds.MAX_E-13100,SwissBounds.MIN_N);

        Edge edge1 = new Edge(0,1, pt1,pt2, 5800, Functions.sampled(typicalYasuoPlayer,5800));
        Edge edge2 = new Edge(1,2, pt3, pt4, 2300, Functions.sampled(typicalYasuoPlayer,2300));
        Edge edge3 = new Edge(2,3, pt5, pt6, 1100, Functions.sampled(typicalYasuoPlayer,1100));
        Edge edge4 = new Edge(3,4, pt7, pt8, 2200, Functions.sampled(typicalYasuoPlayer,2200));
        Edge edge5 = new Edge(4,5, pt9, pt10, 1700, Functions.sampled(typicalYasuoPlayer,1700));

        Edge edge6 = new Edge(3,4, pt7, pt8, 1000, Functions.sampled(typicalYasuoPlayer,1000));
        Edge edge7 = new Edge(4,5, pt9, pt10, 1000, Functions.sampled(typicalYasuoPlayer,1000));

        points.add(pt1);
        points.add(pt2);
        points.add(pt4);
        points.add(pt6);
        points.add(pt8);
        points.add(pt10);

        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);

        edges.add(edge6);
        edges.add(edge7);

        List<Route> segments = new ArrayList<>();

        SingleRoute sr1 = new SingleRoute(edges.subList(0,1));
        SingleRoute sr2 = new SingleRoute(edges.subList(1,2));

        List<Route> componentsOfMulti = new ArrayList<>();

        SingleRoute sr3 = new SingleRoute(edges.subList(2,3));
        SingleRoute sr4 = new SingleRoute(edges.subList(3,5));

        componentsOfMulti.add(sr3);
        componentsOfMulti.add(sr4);

        segments.add(sr1);
        segments.add(sr2);

        MultiRoute comp = new MultiRoute(componentsOfMulti);

        segments.add(comp);

        MultiRoute mt = new MultiRoute(segments);

        assertEquals(0,mt.indexOfSegmentAt(5400));
        assertEquals(0,mt.indexOfSegmentAt(5800));
        assertEquals(0,mt.indexOfSegmentAt(-12000));
        assertEquals(1,mt.indexOfSegmentAt(7800));
        assertEquals(2,mt.indexOfSegmentAt(8105));
        assertEquals(2,mt.indexOfSegmentAt(8405));
        assertEquals(3,mt.indexOfSegmentAt(11300));
        assertEquals(3,mt.indexOfSegmentAt(13500));

        List<Route> inceptionRoute = new ArrayList<>();

        inceptionRoute.add(sr1);
        inceptionRoute.add(sr2);

        List<Route> segmentCouche2 = new ArrayList<>();

        segmentCouche2.add(sr2);
        segmentCouche2.add(sr3);

        List<Route> segmentCouche3 = new ArrayList<>();

        SingleRoute sr5 = new SingleRoute(edges.subList(5,6));
        SingleRoute sr6 = new SingleRoute(edges.subList(6,7));

        segmentCouche3.add(sr5);
        segmentCouche3.add(sr6);

        MultiRoute couche3 = new MultiRoute(segmentCouche3);

        segmentCouche2.add(couche3);

        MultiRoute couche2 = new MultiRoute(segmentCouche2);

        inceptionRoute.add(couche2);
        inceptionRoute.add(sr5);

        MultiRoute couche1 = new MultiRoute(inceptionRoute);

        assertEquals(2,couche1.indexOfSegmentAt(10300));
        assertEquals(3,couche1.indexOfSegmentAt(11400));

        assertEquals(4,couche1.indexOfSegmentAt(11600));
        assertEquals(5,couche1.indexOfSegmentAt(12700));
        assertEquals(6,couche1.indexOfSegmentAt(13600));
        assertEquals(6,couche1.indexOfSegmentAt(15000));

    }

    @Test
    public void MultiRouteLengthWorking(){


        List<Edge> edges = new ArrayList<Edge>();
        List<PointCh> points = new ArrayList<PointCh>();

        float[] typicalYasuoPlayer = {0.0f,-15f,8f,178f};

        PointCh pt1 = new PointCh(SwissBounds.MAX_E,SwissBounds.MIN_N);
        PointCh pt2 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt3 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt4 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt5 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt6 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt7 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt8 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt9 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt10 = new PointCh(SwissBounds.MAX_E-13100,SwissBounds.MIN_N);

        Edge edge1 = new Edge(0,1, pt1,pt2, 5800, Functions.sampled(typicalYasuoPlayer,5800));
        Edge edge2 = new Edge(1,2, pt3, pt4, 2300, Functions.sampled(typicalYasuoPlayer,2300));
        Edge edge3 = new Edge(2,3, pt5, pt6, 1100, Functions.sampled(typicalYasuoPlayer,1100));
        Edge edge4 = new Edge(3,4, pt7, pt8, 2200, Functions.sampled(typicalYasuoPlayer,2200));
        Edge edge5 = new Edge(4,5, pt9, pt10, 1700, Functions.sampled(typicalYasuoPlayer,1700));

        Edge edge6 = new Edge(3,4, pt7, pt8, 1000, Functions.sampled(typicalYasuoPlayer,1000));
        Edge edge7 = new Edge(4,5, pt9, pt10, 1000, Functions.sampled(typicalYasuoPlayer,1000));

        points.add(pt1);
        points.add(pt2);
        points.add(pt4);
        points.add(pt6);
        points.add(pt8);
        points.add(pt10);

        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);

        edges.add(edge6);
        edges.add(edge7);

        List<Route> segments = new ArrayList<>();

        SingleRoute sr1 = new SingleRoute(edges.subList(0,1));
        SingleRoute sr2 = new SingleRoute(edges.subList(1,2));

        List<Route> componentsOfMulti = new ArrayList<>();

        SingleRoute sr3 = new SingleRoute(edges.subList(2,3));
        SingleRoute sr4 = new SingleRoute(edges.subList(3,5));

        componentsOfMulti.add(sr3);
        componentsOfMulti.add(sr4);

        segments.add(sr1);
        segments.add(sr2);

        MultiRoute comp = new MultiRoute(componentsOfMulti);

        segments.add(comp);

        MultiRoute mt = new MultiRoute(segments);

        assertEquals(13100,mt.length());

        List<Route> inceptionRoute = new ArrayList<>();

        inceptionRoute.add(sr1);
        inceptionRoute.add(sr2);

        List<Route> segmentCouche2 = new ArrayList<>();

        segmentCouche2.add(sr2);
        segmentCouche2.add(sr3);

        List<Route> segmentCouche3 = new ArrayList<>();

        SingleRoute sr5 = new SingleRoute(edges.subList(5,6));
        SingleRoute sr6 = new SingleRoute(edges.subList(6,7));

        segmentCouche3.add(sr5);
        segmentCouche3.add(sr6);

        MultiRoute couche3 = new MultiRoute(segmentCouche3);

        segmentCouche2.add(couche3);

        MultiRoute couche2 = new MultiRoute(segmentCouche2);

        inceptionRoute.add(couche2);
        inceptionRoute.add(sr5);

        MultiRoute couche1 = new MultiRoute(inceptionRoute);

        assertEquals(14500,couche1.length());
        assertEquals(5400,couche2.length());
        assertEquals(2000,couche3.length());

    }

    @Test
    public void MultiRoutePointsWorking(){


        List<Edge> edges = new ArrayList<Edge>();
        List<PointCh> points = new ArrayList<PointCh>();

        float[] typicalYasuoPlayer = {0.0f,-15f,8f,178f};

        PointCh pt1 = new PointCh(SwissBounds.MAX_E,SwissBounds.MIN_N);
        PointCh pt2 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt3 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt4 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt5 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt6 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt7 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt8 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt9 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt10 = new PointCh(SwissBounds.MAX_E-13100,SwissBounds.MIN_N);

        Edge edge1 = new Edge(0,1, pt1,pt2, 5800, Functions.sampled(typicalYasuoPlayer,5800));
        Edge edge2 = new Edge(1,2, pt3, pt4, 2300, Functions.sampled(typicalYasuoPlayer,2300));
        Edge edge3 = new Edge(2,3, pt5, pt6, 1100, Functions.sampled(typicalYasuoPlayer,1100));
        Edge edge4 = new Edge(3,4, pt7, pt8, 2200, Functions.sampled(typicalYasuoPlayer,2200));
        Edge edge5 = new Edge(4,5, pt9, pt10, 1700, Functions.sampled(typicalYasuoPlayer,1700));

        Edge edge6 = new Edge(3,4, pt7, pt8, 1000, Functions.sampled(typicalYasuoPlayer,1000));
        Edge edge7 = new Edge(4,5, pt9, pt10, 1000, Functions.sampled(typicalYasuoPlayer,1000));

        points.add(pt1);
        points.add(pt2);
        points.add(pt4);
        points.add(pt6);
        points.add(pt8);
        points.add(pt10);

        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);

        edges.add(edge6);
        edges.add(edge7);

        List<Route> segments = new ArrayList<>();

        SingleRoute sr1 = new SingleRoute(edges.subList(0,1));
        SingleRoute sr2 = new SingleRoute(edges.subList(1,2));

        segments.add(sr1);
        segments.add(sr2);

        MultiRoute mt = new MultiRoute(segments);

        List<PointCh> expectedPoints = new ArrayList<>();

        expectedPoints.add(pt1);
        expectedPoints.add(pt3);
        expectedPoints.add(pt4);

        assertEquals(expectedPoints,mt.points());

    }

    @Test
    public void MultiElevationAtWorking(){


        List<Edge> edges = new ArrayList<Edge>();
        List<PointCh> points = new ArrayList<PointCh>();

        float[] typicalYasuoPlayer = {0.0f,-15f,8f,178f};

        PointCh pt1 = new PointCh(SwissBounds.MAX_E,SwissBounds.MIN_N);
        PointCh pt2 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt3 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt4 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt5 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt6 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt7 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt8 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt9 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt10 = new PointCh(SwissBounds.MAX_E-13100,SwissBounds.MIN_N);

        Edge edge1 = new Edge(0,1, pt1,pt2, 5800, Functions.sampled(typicalYasuoPlayer,5800));
        Edge edge2 = new Edge(1,2, pt3, pt4, 2300, Functions.sampled(typicalYasuoPlayer,2300));
        Edge edge3 = new Edge(2,3, pt5, pt6, 1100, Functions.sampled(typicalYasuoPlayer,1100));
        Edge edge4 = new Edge(3,4, pt7, pt8, 2200, Functions.sampled(typicalYasuoPlayer,2200));
        Edge edge5 = new Edge(4,5, pt9, pt10, 1700, Functions.sampled(typicalYasuoPlayer,1700));

        Edge edge6 = new Edge(3,4, pt7, pt8, 1000, Functions.sampled(typicalYasuoPlayer,1000));
        Edge edge7 = new Edge(4,5, pt9, pt10, 1000, Functions.sampled(typicalYasuoPlayer,1000));

        points.add(pt1);
        points.add(pt2);
        points.add(pt4);
        points.add(pt6);
        points.add(pt8);
        points.add(pt10);

        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);

        edges.add(edge6);
        edges.add(edge7);

        List<Route> segments = new ArrayList<>();

        SingleRoute sr1 = new SingleRoute(edges.subList(0,1));
        SingleRoute sr2 = new SingleRoute(edges.subList(1,2));

        List<Route> componentsOfMulti = new ArrayList<>();

        SingleRoute sr3 = new SingleRoute(edges.subList(2,3));
        SingleRoute sr4 = new SingleRoute(edges.subList(3,5));

        componentsOfMulti.add(sr3);
        componentsOfMulti.add(sr4);

        segments.add(sr1);
        segments.add(sr2);

        MultiRoute comp = new MultiRoute(componentsOfMulti);

        assertEquals(sr3.elevationAt(1000),comp.elevationAt(1000));
        assertEquals(sr3.elevationAt(0),comp.elevationAt(-100));
        assertEquals(sr4.elevationAt(100),comp.elevationAt(1200));
        assertEquals(sr4.elevationAt(3900),comp.elevationAt(5500));

        segments.add(comp);

        MultiRoute mt = new MultiRoute(segments);

        List<Route> inceptionRoute = new ArrayList<>();

        inceptionRoute.add(sr1);
        inceptionRoute.add(sr2);

        List<Route> segmentCouche2 = new ArrayList<>();

        segmentCouche2.add(sr2);
        segmentCouche2.add(sr3);

        List<Route> segmentCouche3 = new ArrayList<>();

        SingleRoute sr5 = new SingleRoute(edges.subList(5,6));
        SingleRoute sr6 = new SingleRoute(edges.subList(6,7));

        segmentCouche3.add(sr5);
        segmentCouche3.add(sr6);

        MultiRoute couche3 = new MultiRoute(segmentCouche3);

        segmentCouche2.add(couche3);

        MultiRoute couche2 = new MultiRoute(segmentCouche2);

        inceptionRoute.add(couche2);
        inceptionRoute.add(sr5);

        MultiRoute couche1 = new MultiRoute(inceptionRoute);

        assertEquals(sr2.elevationAt(100),couche1.elevationAt(5900));
        assertEquals(couche2.elevationAt(100),couche1.elevationAt(8200));
        assertEquals(couche2.elevationAt(2200),couche1.elevationAt(10300));
        assertEquals(couche3.elevationAt(100),couche1.elevationAt(11600));
        assertEquals(couche3.elevationAt(1000),couche1.elevationAt(12500));
        assertEquals(couche2.elevationAt(2400),couche1.elevationAt(10500));
        assertEquals(sr5.elevationAt(100),couche1.elevationAt(13600));
        assertEquals(sr5.elevationAt(900),couche1.elevationAt(14400));
        assertEquals(sr5.elevationAt(1000),couche1.elevationAt(14500));
        assertEquals(sr5.elevationAt(1000),couche1.elevationAt(15600));


    }

    @Test
    public void MultiNodeClosestToWorking(){


        List<Edge> edges = new ArrayList<Edge>();
        List<PointCh> points = new ArrayList<PointCh>();

        float[] typicalYasuoPlayer = {0.0f,-15f,8f,178f};

        PointCh pt1 = new PointCh(SwissBounds.MAX_E,SwissBounds.MIN_N);
        PointCh pt2 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt3 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt4 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt5 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt6 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt7 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt8 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt9 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt10 = new PointCh(SwissBounds.MAX_E-13100,SwissBounds.MIN_N);

        Edge edge1 = new Edge(0,1, pt1,pt2, 5800, Functions.sampled(typicalYasuoPlayer,5800));
        Edge edge2 = new Edge(1,2, pt3, pt4, 2300, Functions.sampled(typicalYasuoPlayer,2300));
        Edge edge3 = new Edge(2,3, pt5, pt6, 1100, Functions.sampled(typicalYasuoPlayer,1100));
        Edge edge4 = new Edge(3,4, pt7, pt8, 2200, Functions.sampled(typicalYasuoPlayer,2200));
        Edge edge5 = new Edge(4,5, pt9, pt10, 1700, Functions.sampled(typicalYasuoPlayer,1700));

        Edge edge6 = new Edge(3,4, pt7, pt8, 1000, Functions.sampled(typicalYasuoPlayer,1000));
        Edge edge7 = new Edge(4,5, pt9, pt10, 1000, Functions.sampled(typicalYasuoPlayer,1000));

        points.add(pt1);
        points.add(pt2);
        points.add(pt4);
        points.add(pt6);
        points.add(pt8);
        points.add(pt10);

        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);

        edges.add(edge6);
        edges.add(edge7);

        List<Route> segments = new ArrayList<>();

        SingleRoute sr1 = new SingleRoute(edges.subList(0,1));
        SingleRoute sr2 = new SingleRoute(edges.subList(1,2));

        List<Route> componentsOfMulti = new ArrayList<>();

        SingleRoute sr3 = new SingleRoute(edges.subList(2,3));
        SingleRoute sr4 = new SingleRoute(edges.subList(3,5));

        componentsOfMulti.add(sr3);
        componentsOfMulti.add(sr4);

        segments.add(sr1);
        segments.add(sr2);

        MultiRoute comp = new MultiRoute(componentsOfMulti);

//        assertEquals(0,comp.nodeClosestTo(550));
//        assertEquals(1,comp.nodeClosestTo(1000));
//        assertEquals(0,comp.nodeClosestTo(-100));
//        assertEquals(1,comp.nodeClosestTo(1200));
        segments.add(comp);

        MultiRoute mt = new MultiRoute(segments);

        assertEquals(13100,mt.length());

        List<Route> inceptionRoute = new ArrayList<>();

        inceptionRoute.add(sr1);
        inceptionRoute.add(sr2);

        List<Route> segmentCouche2 = new ArrayList<>();

        segmentCouche2.add(sr2);
        segmentCouche2.add(sr3);

        List<Route> segmentCouche3 = new ArrayList<>();

        SingleRoute sr5 = new SingleRoute(edges.subList(5,6));
        SingleRoute sr6 = new SingleRoute(edges.subList(6,7));

        segmentCouche3.add(sr5);
        segmentCouche3.add(sr6);

        MultiRoute couche3 = new MultiRoute(segmentCouche3);

        segmentCouche2.add(couche3);

        MultiRoute couche2 = new MultiRoute(segmentCouche2);

        inceptionRoute.add(couche2);
        inceptionRoute.add(sr5);

        MultiRoute couche1 = new MultiRoute(inceptionRoute);

//        assertEquals(0,couche1.nodeClosestTo(550));
//        assertEquals(0,couche1.nodeClosestTo(2400));
//        assertEquals(1,couche1.nodeClosestTo(3000));
//        assertEquals(2,couche1.nodeClosestTo(8000));
//        assertEquals(2,couche1.nodeClosestTo(8400));
//        assertEquals(4,couche1.nodeClosestTo(11400));
//        assertEquals(5,couche1.nodeClosestTo(12600));
//
//        assertEquals(4,couche1.nodeClosestTo(11900));
//        assertEquals(5,couche1.nodeClosestTo(12900));
//        assertEquals(6,couche1.nodeClosestTo(13700));
//        assertEquals(6,couche1.nodeClosestTo(14000));
//        assertEquals(7,couche1.nodeClosestTo(18000));

    }

//    @Test
//    public void MultiNodeClosestToWorking2(){
//
//
//        List<Edge> edges = new ArrayList<Edge>();
//        List<PointCh> points = new ArrayList<PointCh>();
//
//        float[] typicalYasuoPlayer = {0.0f,-15f,8f,178f};
//
//        PointCh pt1 = new PointCh(SwissBounds.MAX_E,SwissBounds.MIN_N);
//        PointCh pt2 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
//        PointCh pt3 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
//        PointCh pt4 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
//        PointCh pt5 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
//        PointCh pt6 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
//        PointCh pt7 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
//        PointCh pt8 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
//        PointCh pt9 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
//        PointCh pt10 = new PointCh(SwissBounds.MAX_E-13100,SwissBounds.MIN_N);
//
//        Edge edge1 = new Edge(0,1, pt1,pt2, 5800, Functions.sampled(typicalYasuoPlayer,5800));
//        Edge edge2 = new Edge(1,2, pt3, pt4, 2300, Functions.sampled(typicalYasuoPlayer,2300));
//        Edge edge3 = new Edge(2,3, pt5, pt6, 1100, Functions.sampled(typicalYasuoPlayer,1100));
//        Edge edge4 = new Edge(3,4, pt7, pt8, 2200, Functions.sampled(typicalYasuoPlayer,2200));
//        Edge edge5 = new Edge(4,5, pt9, pt10, 1700, Functions.sampled(typicalYasuoPlayer,1700));
//
//        Edge edge6 = new Edge(3,4, pt7, pt8, 1000, Functions.sampled(typicalYasuoPlayer,1000));
//        Edge edge7 = new Edge(4,5, pt9, pt10, 1000, Functions.sampled(typicalYasuoPlayer,1000));
//
//        Edge bigFamily1 = new Edge(0,1, pt1,pt2, 500, Functions.sampled(typicalYasuoPlayer,500));
//        Edge bigFamily2 = new Edge(0,1, pt1,pt2, 500, Functions.sampled(typicalYasuoPlayer,500));
//        Edge bigFamily3 = new Edge(0,1, pt1,pt2, 1000, Functions.sampled(typicalYasuoPlayer,1000));
//        Edge bigFamily4 = new Edge(0,1, pt1,pt2, 1000, Functions.sampled(typicalYasuoPlayer,1000));
//        Edge bigFamily5 = new Edge(0,1, pt1,pt2, 1000, Functions.sampled(typicalYasuoPlayer,1000));
//        Edge bigFamily6 = new Edge(0,1, pt1,pt2, 500, Functions.sampled(typicalYasuoPlayer,500));
//        Edge bigFamily7 = new Edge(0,1, pt1,pt2, 500, Functions.sampled(typicalYasuoPlayer,500));
//        Edge bigFamily8 = new Edge(0,1, pt1,pt2, 300, Functions.sampled(typicalYasuoPlayer,300));
//
//        List<Edge> bigFamilyCompleteVersion = new ArrayList<>();
//
//        bigFamilyCompleteVersion.add(bigFamily1);
//        bigFamilyCompleteVersion.add(bigFamily2);
//        bigFamilyCompleteVersion.add(bigFamily3);
//        bigFamilyCompleteVersion.add(bigFamily4);
//        bigFamilyCompleteVersion.add(bigFamily5);
//        bigFamilyCompleteVersion.add(bigFamily6);
//        bigFamilyCompleteVersion.add(bigFamily7);
//        bigFamilyCompleteVersion.add(bigFamily8);
//
//        points.add(pt1);
//        points.add(pt2);
//        points.add(pt4);
//        points.add(pt6);
//        points.add(pt8);
//        points.add(pt10);
//
//        edges.add(edge1);
//        edges.add(edge2);
//        edges.add(edge3);
//        edges.add(edge4);
//        edges.add(edge5);
//
//        edges.add(edge6);
//        edges.add(edge7);
//
//        List<Route> segments = new ArrayList<>();
//
//        SingleRoute sr1 = new SingleRoute(bigFamilyCompleteVersion);
//        SingleRoute sr2 = new SingleRoute(edges.subList(1,2));
//
//        List<Route> componentsOfMulti = new ArrayList<>();
//
//        SingleRoute sr3 = new SingleRoute(edges.subList(2,3));
//        SingleRoute sr4 = new SingleRoute(edges.subList(3,5));
//
//        componentsOfMulti.add(sr3);
//        componentsOfMulti.add(sr4);
//
//        segments.add(sr1);
//        segments.add(sr2);
//
//        MultiRoute comp = new MultiRoute(componentsOfMulti);
//
//        assertEquals(0,comp.nodeClosestTo(550));
//        assertEquals(1,comp.nodeClosestTo(1000));
//        assertEquals(0,comp.nodeClosestTo(-100));
////        assertEquals(1,comp.nodeClosestTo(1200));
//        segments.add(comp);
//
//        MultiRoute mt = new MultiRoute(segments);
//
//
//        List<Route> inceptionRoute = new ArrayList<>();
//
//        inceptionRoute.add(sr1);
//        inceptionRoute.add(sr2);
//
//        List<Route> segmentCouche2 = new ArrayList<>();
//
//        segmentCouche2.add(sr2);
//        segmentCouche2.add(sr3);
//
//        List<Route> segmentCouche3 = new ArrayList<>();
//
//        SingleRoute sr5 = new SingleRoute(edges.subList(5,6));
//        SingleRoute sr6 = new SingleRoute(edges.subList(6,7));
//
//        segmentCouche3.add(sr5);
//        segmentCouche3.add(sr6);
//
//        MultiRoute couche3 = new MultiRoute(segmentCouche3);
//
//        segmentCouche2.add(couche3);
//
//        MultiRoute couche2 = new MultiRoute(segmentCouche2);
//
//        inceptionRoute.add(couche2);
//        inceptionRoute.add(sr5);
//
//        MultiRoute couche1 = new MultiRoute(inceptionRoute);
//
//        assertEquals(1,couche1.nodeClosestTo(550));
//        assertEquals(8,couche1.nodeClosestTo(5900));
//
//
//    }

    @Test
    public void MultiPointAtWorking(){


        List<Edge> edges = new ArrayList<Edge>();
        List<PointCh> points = new ArrayList<PointCh>();

        float[] typicalYasuoPlayer = {0.0f,-15f,8f,178f};

        PointCh pt1 = new PointCh(SwissBounds.MAX_E,SwissBounds.MIN_N);
        PointCh pt2 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt3 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt4 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt5 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt6 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt7 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt8 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt9 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt10 = new PointCh(SwissBounds.MAX_E-13100,SwissBounds.MIN_N);

        Edge edge1 = new Edge(0,1, pt1,pt2, 5800, Functions.sampled(typicalYasuoPlayer,5800));
        Edge edge2 = new Edge(1,2, pt3, pt4, 2300, Functions.sampled(typicalYasuoPlayer,2300));
        Edge edge3 = new Edge(2,3, pt5, pt6, 1100, Functions.sampled(typicalYasuoPlayer,1100));
        Edge edge4 = new Edge(3,4, pt7, pt8, 2200, Functions.sampled(typicalYasuoPlayer,2200));
        Edge edge5 = new Edge(4,5, pt9, pt10, 1700, Functions.sampled(typicalYasuoPlayer,1700));

        Edge edge6 = new Edge(3,4, pt7, pt8, 1000, Functions.sampled(typicalYasuoPlayer,1000));
        Edge edge7 = new Edge(4,5, pt9, pt10, 1000, Functions.sampled(typicalYasuoPlayer,1000));

        points.add(pt1);
        points.add(pt2);
        points.add(pt4);
        points.add(pt6);
        points.add(pt8);
        points.add(pt10);

        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);

        edges.add(edge6);
        edges.add(edge7);

        List<Route> segments = new ArrayList<>();

        SingleRoute sr1 = new SingleRoute(edges.subList(0,1));
        SingleRoute sr2 = new SingleRoute(edges.subList(1,2));

        List<Route> componentsOfMulti = new ArrayList<>();

        SingleRoute sr3 = new SingleRoute(edges.subList(2,3));
        SingleRoute sr4 = new SingleRoute(edges.subList(3,5));

        componentsOfMulti.add(sr3);
        componentsOfMulti.add(sr4);

        segments.add(sr1);
        segments.add(sr2);

        MultiRoute comp = new MultiRoute(componentsOfMulti);

        segments.add(comp);

        MultiRoute mt = new MultiRoute(segments);

        assertEquals(13100,mt.length());

        List<Route> inceptionRoute = new ArrayList<>();

        inceptionRoute.add(sr1);
        inceptionRoute.add(sr2);

        List<Route> segmentCouche2 = new ArrayList<>();

        segmentCouche2.add(sr2);
        segmentCouche2.add(sr3);

        List<Route> segmentCouche3 = new ArrayList<>();

        SingleRoute sr5 = new SingleRoute(edges.subList(5,6));
        SingleRoute sr6 = new SingleRoute(edges.subList(6,7));

        segmentCouche3.add(sr5);
        segmentCouche3.add(sr6);

        MultiRoute couche3 = new MultiRoute(segmentCouche3);

        segmentCouche2.add(couche3);

        MultiRoute couche2 = new MultiRoute(segmentCouche2);

        inceptionRoute.add(couche2);
        inceptionRoute.add(sr5);

        MultiRoute couche1 = new MultiRoute(inceptionRoute);

        assertEquals(sr1.pointAt(-500),couche1.pointAt(-550));
        assertEquals(sr1.pointAt(550),couche1.pointAt(550));
        assertEquals(sr2.pointAt(200),couche1.pointAt(6000));
        assertEquals(couche2.pointAt(100),couche1.pointAt(8200));
        assertEquals(couche2.pointAt(2400),couche1.pointAt(10500));
        assertEquals(couche3.pointAt(100),couche1.pointAt(11600));
        assertEquals(sr5.pointAt(15000),couche1.pointAt(15000));


    }

    @Disabled
    @Test
    public void MultiPointClosestToWorking(){


        List<Edge> edges = new ArrayList<Edge>();
        List<PointCh> points = new ArrayList<PointCh>();

        float[] typicalYasuoPlayer = {0.0f,-15f,8f,178f};

        PointCh pt1 = new PointCh(SwissBounds.MAX_E,SwissBounds.MIN_N);
        PointCh pt2 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt3 = new PointCh(SwissBounds.MAX_E-5800,SwissBounds.MIN_N);
        PointCh pt4 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt5 = new PointCh(SwissBounds.MAX_E-8100,SwissBounds.MIN_N);
        PointCh pt6 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt7 = new PointCh(SwissBounds.MAX_E-9200,SwissBounds.MIN_N);
        PointCh pt8 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt9 = new PointCh(SwissBounds.MAX_E-11400,SwissBounds.MIN_N);
        PointCh pt10 = new PointCh(SwissBounds.MAX_E-13100,SwissBounds.MIN_N);

        Edge edge1 = new Edge(0,1, pt1,pt2, 5800, Functions.sampled(typicalYasuoPlayer,5800));
        Edge edge2 = new Edge(1,2, pt3, pt4, 2300, Functions.sampled(typicalYasuoPlayer,2300));
        Edge edge3 = new Edge(2,3, pt5, pt6, 1100, Functions.sampled(typicalYasuoPlayer,1100));
        Edge edge4 = new Edge(3,4, pt7, pt8, 2200, Functions.sampled(typicalYasuoPlayer,2200));
        Edge edge5 = new Edge(4,5, pt9, pt10, 1700, Functions.sampled(typicalYasuoPlayer,1700));

        Edge edge6 = new Edge(3,4, pt7, pt8, 1000, Functions.sampled(typicalYasuoPlayer,1000));
        Edge edge7 = new Edge(4,5, pt9, pt10, 1000, Functions.sampled(typicalYasuoPlayer,1000));

        points.add(pt1);
        points.add(pt2);
        points.add(pt4);
        points.add(pt6);
        points.add(pt8);
        points.add(pt10);

        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);

        edges.add(edge6);
        edges.add(edge7);

        List<Route> segments = new ArrayList<>();

        SingleRoute sr1 = new SingleRoute(edges.subList(0,1));
        SingleRoute sr2 = new SingleRoute(edges.subList(1,2));

        List<Route> componentsOfMulti = new ArrayList<>();

        SingleRoute sr3 = new SingleRoute(edges.subList(2,3));
        SingleRoute sr4 = new SingleRoute(edges.subList(3,5));

        componentsOfMulti.add(sr3);
        componentsOfMulti.add(sr4);

        segments.add(sr1);
        segments.add(sr2);

        MultiRoute comp = new MultiRoute(componentsOfMulti);

        segments.add(comp);

        MultiRoute mt = new MultiRoute(segments);

        assertEquals(13100,mt.length());

        List<Route> inceptionRoute = new ArrayList<>();

        inceptionRoute.add(sr1);
        inceptionRoute.add(sr2);

        List<Route> segmentCouche2 = new ArrayList<>();

        segmentCouche2.add(sr2);
        segmentCouche2.add(sr3);

        List<Route> segmentCouche3 = new ArrayList<>();

        SingleRoute sr5 = new SingleRoute(edges.subList(5,6));
        SingleRoute sr6 = new SingleRoute(edges.subList(6,7));

        segmentCouche3.add(sr5);
        segmentCouche3.add(sr6);

        MultiRoute couche3 = new MultiRoute(segmentCouche3);

        segmentCouche2.add(couche3);

        MultiRoute couche2 = new MultiRoute(segmentCouche2);

        inceptionRoute.add(couche2);
        inceptionRoute.add(sr5);

        MultiRoute couche1 = new MultiRoute(inceptionRoute);

        assertEquals(sr1.pointClosestTo(pt1),couche1.pointClosestTo(pt1));
        assertEquals(couche2.pointClosestTo(pt4),couche1.pointClosestTo(pt4));
        assertEquals(sr2.pointClosestTo(pt2).withPositionShiftedBy(5800),couche1.pointClosestTo(pt2));
        assertEquals(couche3.pointClosestTo(pt6).withPositionShiftedBy(1100),couche1.pointClosestTo(pt6));


    }

    PointCh point1 = new PointCh(2595000,1200000);
    PointCh point2 = new PointCh(2600000,1204000);
    PointCh point3 = new PointCh(2603000,1202000);
    PointCh point4 = new PointCh(2605000,1203000);
    PointCh point5 = new PointCh(2609000,1199000);
    PointCh point6 = new PointCh(2609500, 1198500);

    float[] samples1 = new float[]{200, 240, 230, 360,380};
    DoubleUnaryOperator profile1 = Functions.sampled(samples1, point1.distanceTo(point2) );
    Edge edge1 = new Edge(0, 3, point1, point2, point1.distanceTo(point2), profile1);
    float[] samples2 = new float[]{380,360, 340, 350,320,350};
    DoubleUnaryOperator profile2 = Functions.sampled(samples2, point2.distanceTo(point3));
    Edge edge2 = new Edge(3, 8,point2 ,point3,point2.distanceTo(point3), profile2);
    float[] samples3 = new float[]{350, 320, 300, 280,270,250};
    DoubleUnaryOperator profile3 = Functions.sampled(samples3,point3.distanceTo(point4));
    Edge edge3 = new Edge(8,10,point3, point4,point3.distanceTo(point4),profile3);
    float[] samples4 = new float[]{250, 300, 330, 360,380,400};
    DoubleUnaryOperator profile4 = Functions.sampled(samples4,point4.distanceTo(point5));
    Edge edge4 = new Edge(10, 18,point4 ,point5, point4.distanceTo(point5),profile4);
    // Edge edge5 = new Edge(18, 5,point5 ,point6, point5.distanceTo(point6),x ->Float.NaN);

    float[] samplesReverse4 = new float[]{400, 380, 360, 330,300,250};
    DoubleUnaryOperator profileReverse4 = Functions.sampled(samplesReverse4,point4.distanceTo(point5));
    Edge edgeReverse4 = new Edge(18, 10,point5 ,point4, point4.distanceTo(point5),profileReverse4);
    float[] samplesReverse3 = new float[]{250, 270, 280, 300,320,350};
    DoubleUnaryOperator profileReverse3 = Functions.sampled(samplesReverse3,point3.distanceTo(point4));
    Edge edgeReverse3 = new Edge(10,8,point4, point3,point3.distanceTo(point4),profileReverse3);
    float[] samplesReverse2 = new float[]{350,320, 350, 340, 360,380};
    DoubleUnaryOperator profileReverse2 = Functions.sampled(samplesReverse2, point2.distanceTo(point3));
    Edge edgeReverse2 = new Edge(8, 3,point3 ,point2,point2.distanceTo(point3), profileReverse2);
    float[] samplesReverse1 = new float[]{380, 360, 230, 240,200};
    DoubleUnaryOperator profileReverse1 = Functions.sampled(samplesReverse1, point1.distanceTo(point2) );
    Edge edgeReverse1 = new Edge(3, 0, point2, point1, point1.distanceTo(point2), profileReverse1);

    List<Edge> edges1 = new ArrayList<>();
    List<Edge> edges2 = new ArrayList<>();
    List<Edge> edges3 = new ArrayList<>();
    List<Edge> edges4 = new ArrayList<>();

    @Test
    public void indexOfSegmentAtNestedMultiRoutes(){
        edges1.add(edge1);
        edges1.add(edge2);
        SingleRoute route1 = new SingleRoute(edges1);

        edges2.add(edge3);
        edges2.add(edge4);
        SingleRoute route2 = new SingleRoute(edges2);

        edges3.add(edgeReverse4);
        edges3.add(edgeReverse3);
        SingleRoute route3 = new SingleRoute(edges3);

        edges4.add(edgeReverse2);
        edges4.add(edgeReverse1);
        SingleRoute route4 = new SingleRoute(edges4);


        List<Route> routes1 = new ArrayList<>();
        routes1.add(route1);
        routes1.add(route2);
        List<Route> routes2 = new ArrayList<>();
        routes2.add(route3);
        routes2.add(route4);

        MultiRoute multiRoute1 = new MultiRoute(routes1);
        MultiRoute multiRoute2 = new MultiRoute(routes2);

        List<Route> multiRoutes = new ArrayList<>();
        multiRoutes.add(multiRoute1);
        multiRoutes.add(multiRoute2);

        MultiRoute multiRouteSupreme = new MultiRoute(multiRoutes);
        //La méthode fonctionne-t-elle pour les cas limites ?
        assertEquals(3,multiRouteSupreme.indexOfSegmentAt(multiRouteSupreme.length()));
        assertEquals(0,multiRouteSupreme.indexOfSegmentAt(0));
        assertEquals(3,multiRouteSupreme.indexOfSegmentAt(30795));
        //Fonctionne-elle pour des valeurs pratiques ?
        assertEquals(0,multiRouteSupreme.indexOfSegmentAt(5000));
        assertEquals(2,multiRouteSupreme.indexOfSegmentAt(20902));
        //Voyons si vous avez ramené la position entre 0 et la longueur totale.
        assertEquals(0,multiRouteSupreme.indexOfSegmentAt(-4));
        assertEquals(3,multiRouteSupreme.indexOfSegmentAt(multiRouteSupreme.length()+42));

    }

    @Disabled
    @Test
    public void indexOfSegmentAtOnlySingleRoutes(){
        edges1.add(edge1);
        edges1.add(edge2);
        SingleRoute route1 = new SingleRoute(edges1);

        edges2.add(edge3);
        edges2.add(edge4);
        SingleRoute route2 = new SingleRoute(edges2);

        edges3.add(edgeReverse4);
        edges3.add(edgeReverse3);
        SingleRoute route3 = new SingleRoute(edges3);
        List<Route> routes = new ArrayList<>();
        routes.add(route1);
        routes.add(route2);
        routes.add(route3);

        MultiRoute bRoute = new MultiRoute(routes);

        assertEquals(1, bRoute.indexOfSegmentAt(13465));
        assertEquals(0, bRoute.indexOfSegmentAt(0));
        assertEquals(2, bRoute.indexOfSegmentAt(21402));
        assertEquals(2, bRoute.indexOfSegmentAt(bRoute.length()));
    }
    @Test
    public void indexOfSegmentAtInceptionMultiRoutes(){
        edges1.add(edge1);
        edges1.add(edge2);
        SingleRoute route1 = new SingleRoute(edges1);

        edges2.add(edge3);
        edges2.add(edge4);
        SingleRoute route2 = new SingleRoute(edges2);

        edges3.add(edgeReverse4);
        edges3.add(edgeReverse3);
        SingleRoute route3 = new SingleRoute(edges3);

        edges4.add(edgeReverse2);
        edges4.add(edgeReverse1);
        SingleRoute route4 = new SingleRoute(edges4);

        List<Route> routes1 = new ArrayList<>();

        routes1.add(route1);
        routes1.add(route2);

        List<Route> routes2 = new ArrayList<>();

        routes2.add(route3);
        routes2.add(route4);

        MultiRoute lucidRoute1 = new MultiRoute(routes1);
        MultiRoute lucidRoute2 = new MultiRoute(routes2);

        List<Route> multiRoutes = new ArrayList<>();
        multiRoutes.add(lucidRoute1);
        multiRoutes.add(lucidRoute2);

        MultiRoute paradoxicalSleep = new MultiRoute(multiRoutes);

        List<Route> multisRoutes = new ArrayList<>();
        multisRoutes.add(paradoxicalSleep);

        MultiRoute inceptionRoute = new MultiRoute(multisRoutes);

        assertEquals(0,inceptionRoute.indexOfSegmentAt(0));
        assertEquals(3,inceptionRoute.indexOfSegmentAt(30795));
        assertEquals(3,inceptionRoute.indexOfSegmentAt(inceptionRoute.length()));
        assertEquals(0,inceptionRoute.indexOfSegmentAt(5000));
        assertEquals(0,inceptionRoute.indexOfSegmentAt(-4));
        assertEquals(3,inceptionRoute.indexOfSegmentAt(inceptionRoute.length()+42));
        assertEquals(2,inceptionRoute.indexOfSegmentAt(20902));
    }
    @Test
    public void indexOfSegmentAtMixAndTwist(){
        edges1.add(edge1);
        edges1.add(edge2);
        SingleRoute route1 = new SingleRoute(edges1);

        edges2.add(edge3);
        edges2.add(edge4);
        SingleRoute route2 = new SingleRoute(edges2);

        edges3.add(edgeReverse4);
        edges3.add(edgeReverse3);
        SingleRoute route3 = new SingleRoute(edges3);

        edges4.add(edgeReverse2);
        edges4.add(edgeReverse1);
        SingleRoute route4 = new SingleRoute(edges4);

        List<Route> routes1 = new ArrayList<>();

        routes1.add(route1);
        routes1.add(route2);

        MultiRoute mixRoute = new MultiRoute(routes1);

        List<Route> routes2 = new ArrayList<>();
        routes2.add(mixRoute);
        routes2.add(route3);
        routes2.add(route4);

        MultiRoute twistRoute = new MultiRoute(routes2);

        assertEquals(0,twistRoute.indexOfSegmentAt(0));
        assertEquals(3,twistRoute.indexOfSegmentAt(30795));
        assertEquals(3,twistRoute.indexOfSegmentAt(twistRoute.length()));
        assertEquals(0,twistRoute.indexOfSegmentAt(5000));
        assertEquals(0,twistRoute.indexOfSegmentAt(-4));
        assertEquals(3,twistRoute.indexOfSegmentAt(twistRoute.length()+42));
        assertEquals(2,twistRoute.indexOfSegmentAt(20902));
    }
    @Test
    public void lengthTest(){
        edges1.add(edge1);
        edges1.add(edge2);
        SingleRoute route1 = new SingleRoute(edges1);

        edges2.add(edge3);
        edges2.add(edge4);
        SingleRoute route2 = new SingleRoute(edges2);

        edges3.add(edgeReverse4);
        edges3.add(edgeReverse3);
        SingleRoute route3 = new SingleRoute(edges3);

        edges4.add(edgeReverse2);
        edges4.add(edgeReverse1);
        SingleRoute route4 = new SingleRoute(edges4);


        List<Route> routes1 = new ArrayList<>();
        routes1.add(route1);
        routes1.add(route2);
        List<Route> routes2 = new ArrayList<>();
        routes2.add(route3);
        routes2.add(route4);

        MultiRoute multiRoute1 = new MultiRoute(routes1);
        MultiRoute multiRoute2 = new MultiRoute(routes2);

        List<Route> multiRoutes = new ArrayList<>();
        multiRoutes.add(multiRoute1);
        multiRoutes.add(multiRoute2);

        MultiRoute multiRouteSupreme = new MultiRoute(multiRoutes);

        assertEquals(route1.length()+route2.length()+route3.length()+route4.length(),multiRouteSupreme.length());
    }
    @Test
    public void edgesTest(){
        edges1.add(edge1);
        edges1.add(edge2);
        SingleRoute route1 = new SingleRoute(edges1);

        edges2.add(edge3);
        edges2.add(edge4);
        SingleRoute route2 = new SingleRoute(edges2);

        edges3.add(edgeReverse4);
        edges3.add(edgeReverse3);
        SingleRoute route3 = new SingleRoute(edges3);

        edges4.add(edgeReverse2);
        edges4.add(edgeReverse1);
        SingleRoute route4 = new SingleRoute(edges4);


        List<Route> routes1 = new ArrayList<>();
        routes1.add(route1);
        routes1.add(route2);
        List<Route> routes2 = new ArrayList<>();
        routes2.add(route3);
        routes2.add(route4);

        MultiRoute multiRoute1 = new MultiRoute(routes1);
        MultiRoute multiRoute2 = new MultiRoute(routes2);

        List<Route> multiRoutes = new ArrayList<>();
        multiRoutes.add(multiRoute1);
        multiRoutes.add(multiRoute2);

        MultiRoute multiRouteSupreme = new MultiRoute(multiRoutes);
        List<Edge> edges = List.of(edge1,edge2,edge3,edge4,edgeReverse4,edgeReverse3,edgeReverse2,edgeReverse1);
        assertEquals(edges,multiRouteSupreme.edges());
    }
    @Test
    public void pointsTest(){
        edges1.add(edge1);
        edges1.add(edge2);
        SingleRoute route1 = new SingleRoute(edges1);

        edges2.add(edge3);
        edges2.add(edge4);
        SingleRoute route2 = new SingleRoute(edges2);

        edges3.add(edgeReverse4);
        edges3.add(edgeReverse3);
        SingleRoute route3 = new SingleRoute(edges3);

        edges4.add(edgeReverse2);
        edges4.add(edgeReverse1);
        SingleRoute route4 = new SingleRoute(edges4);


        List<Route> routes1 = new ArrayList<>();
        routes1.add(route1);
        routes1.add(route2);
        List<Route> routes2 = new ArrayList<>();
        routes2.add(route3);
        routes2.add(route4);

        MultiRoute multiRoute1 = new MultiRoute(routes1);
        MultiRoute multiRoute2 = new MultiRoute(routes2);

        List<Route> multiRoutes = new ArrayList<>();
        multiRoutes.add(multiRoute1);
        multiRoutes.add(multiRoute2);

        MultiRoute multiRouteSupreme = new MultiRoute(multiRoutes);

        List<PointCh> points = List.of(point1,point2,point3,point4,point5,point4,point3,point2,point1);

        assertEquals(points,multiRouteSupreme.points());
    }
    @Test
    public void elevationAtTest(){
        edges1.add(edge1);
        edges1.add(edge2);
        SingleRoute route1 = new SingleRoute(edges1);

        edges2.add(edge3);
        edges2.add(edge4);
        SingleRoute route2 = new SingleRoute(edges2);

        edges3.add(edgeReverse4);
        edges3.add(edgeReverse3);
        SingleRoute route3 = new SingleRoute(edges3);

        edges4.add(edgeReverse2);
        edges4.add(edgeReverse1);
        SingleRoute route4 = new SingleRoute(edges4);


        List<Route> routes1 = new ArrayList<>();
        routes1.add(route1);
        routes1.add(route2);
        List<Route> routes2 = new ArrayList<>();
        routes2.add(route3);
        routes2.add(route4);

        MultiRoute multiRoute1 = new MultiRoute(routes1);
        MultiRoute multiRoute2 = new MultiRoute(routes2);

        List<Route> multiRoutes = new ArrayList<>();
        multiRoutes.add(multiRoute1);
        multiRoutes.add(multiRoute2);

        MultiRoute multiRouteSupreme = new MultiRoute(multiRoutes);

        assertEquals(route3.elevationAt(18302-route1.length()-route2.length()), multiRouteSupreme.elevationAt(18302),1e-6);
        assertEquals(route1.elevationAt(0), multiRouteSupreme.elevationAt(0));
        assertEquals(route4.elevationAt(route4.length()), multiRouteSupreme.elevationAt(multiRouteSupreme.length()));
        assertEquals(route3.elevationAt(route3.length()), multiRouteSupreme.elevationAt(route1.length()+route2.length()+route3.length()));
        assertEquals(route1.elevationAt(route1.length()), multiRouteSupreme.elevationAt(route1.length()));
    }
    @Test
    public void pointAtTest(){
        edges1.add(edge1);
        edges1.add(edge2);
        SingleRoute route1 = new SingleRoute(edges1);

        edges2.add(edge3);
        edges2.add(edge4);
        SingleRoute route2 = new SingleRoute(edges2);

        edges3.add(edgeReverse4);
        edges3.add(edgeReverse3);
        SingleRoute route3 = new SingleRoute(edges3);

        edges4.add(edgeReverse2);
        edges4.add(edgeReverse1);
        SingleRoute route4 = new SingleRoute(edges4);


        List<Route> routes1 = new ArrayList<>();
        routes1.add(route1);
        routes1.add(route2);
        List<Route> routes2 = new ArrayList<>();
        routes2.add(route3);
        routes2.add(route4);

        MultiRoute multiRoute1 = new MultiRoute(routes1);
        MultiRoute multiRoute2 = new MultiRoute(routes2);

        List<Route> multiRoutes = new ArrayList<>();
        multiRoutes.add(multiRoute1);
        multiRoutes.add(multiRoute2);

        MultiRoute multiRouteSupreme = new MultiRoute(multiRoutes);

        assertEquals(route3.pointAt(18302-route1.length()-route2.length()), multiRouteSupreme.pointAt(18302));
        assertEquals(route1.pointAt(0), multiRouteSupreme.pointAt(0));
        assertEquals(route4.pointAt(route4.length()), multiRouteSupreme.pointAt(multiRouteSupreme.length()));
    }

    @Disabled
    @Test
    public void nodeClosestToTest(){
        edges1.add(edge1);
        edges1.add(edge2);
        SingleRoute route1 = new SingleRoute(edges1);

        edges2.add(edge3);
        edges2.add(edge4);
        SingleRoute route2 = new SingleRoute(edges2);

        edges3.add(edgeReverse4);
        edges3.add(edgeReverse3);
        SingleRoute route3 = new SingleRoute(edges3);

        edges4.add(edgeReverse2);
        edges4.add(edgeReverse1);
        SingleRoute route4 = new SingleRoute(edges4);


        List<Route> routes1 = new ArrayList<>();
        routes1.add(route1);
        routes1.add(route2);
        List<Route> routes2 = new ArrayList<>();
        routes2.add(route3);
        routes2.add(route4);

        MultiRoute multiRoute1 = new MultiRoute(routes1);
        MultiRoute multiRoute2 = new MultiRoute(routes2);

        List<Route> multiRoutes = new ArrayList<>();
        multiRoutes.add(multiRoute1);
        multiRoutes.add(multiRoute2);

        MultiRoute multiRouteSupreme = new MultiRoute(multiRoutes);

        assertEquals(18, multiRouteSupreme.nodeClosestTo(18302));
        assertEquals(8, multiRouteSupreme.nodeClosestTo(25000));
        assertEquals(0, multiRouteSupreme.nodeClosestTo(0));
        assertEquals(0, multiRouteSupreme.nodeClosestTo(multiRouteSupreme.length()));
    }

    @Disabled
    @Test
    public void pointClosestToTest(){

        edges1.add(edge1);
        edges1.add(edge2);
        SingleRoute route1 = new SingleRoute(edges1);

        edges2.add(edge3);
        edges2.add(edge4);
        SingleRoute route2 = new SingleRoute(edges2);

        edges3.add(edgeReverse4);
        edges3.add(edgeReverse3);
        SingleRoute route3 = new SingleRoute(edges3);

        edges4.add(edgeReverse2);
        edges4.add(edgeReverse1);
        SingleRoute route4 = new SingleRoute(edges4);


        List<Route> routes1 = new ArrayList<>();
        routes1.add(route1);
        routes1.add(route2);
        List<Route> routes2 = new ArrayList<>();
        routes2.add(route3);
        routes2.add(route4);

        MultiRoute multiRoute1 = new MultiRoute(routes1);
        MultiRoute multiRoute2 = new MultiRoute(routes2);

        List<Route> multiRoutes = new ArrayList<>();
        multiRoutes.add(multiRoute1);
        multiRoutes.add(multiRoute2);

        MultiRoute multiRouteSupreme = new MultiRoute(multiRoutes);


        RoutePoint routePoint1 = new RoutePoint(point5,route1.length()+route2.length(),0);
        assertEquals(routePoint1, multiRouteSupreme.pointClosestTo(point5));

        PointCh expectedPoint = new PointCh(2609500,1199500);
        RoutePoint routePoint2 = new RoutePoint(point5,route1.length()+route2.length(),expectedPoint.distanceTo(point5));
        assertEquals(routePoint2,multiRouteSupreme.pointClosestTo(expectedPoint));

        //c'est pas toi c'est moi
        //envoyez moi des colis c'est le PointCh de ma maison (Clement sans accent parce que c'est William qui ecrit)
        PointCh exPoint = new PointCh(2534471, 1154885);
        RoutePoint routePoint3 = new RoutePoint(point1, 0,exPoint.distanceTo(point1));
        assertEquals(routePoint3, multiRouteSupreme.pointClosestTo(exPoint));



        List<Route> singleRoutes = new ArrayList<>();
        SingleRoute singleRoute1 = new SingleRoute(List.of(edge1));
        SingleRoute singleRoute2 = new SingleRoute(List.of(edge2));
        SingleRoute singleRoute3 = new SingleRoute(List.of(edge3));
        SingleRoute singleRoute4 = new SingleRoute(List.of(edge4));
        singleRoutes.add(singleRoute1);
        singleRoutes.add(singleRoute2);
        singleRoutes.add(singleRoute3);
        singleRoutes.add(singleRoute4);
        MultiRoute cRoute = new MultiRoute(singleRoutes);

        //Si celui là marche vous êtes chauds (Oui il y a des accents donc c'est l'autre loustic)
        PointCh middlePoint = new PointCh(2604723, 1193147);
        RoutePoint routePoint4 = new RoutePoint(point5, route1.length()+route2.length(),middlePoint.distanceTo(point5));
        //Si le test ne fonctionne pas pour vous, décommenter ces prints pourrait vous aider.
        //System.out.println(middlePoint.distanceTo(point1));
        //System.out.println(middlePoint.distanceTo(point2));
        //System.out.println(middlePoint.distanceTo(point3));
        //System.out.println(middlePoint.distanceTo(point4));
        //System.out.println(middlePoint.distanceTo(point5));
        assertEquals(routePoint4, cRoute.pointClosestTo(middlePoint));
    }




}
