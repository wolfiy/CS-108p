package ch.epfl.autre;

import ch.epfl.javelo.Functions;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.SwissBounds;
import ch.epfl.javelo.routing.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ElevationProfileComputerTest {

    //TEST remettre le test et passé l'attribut en public dans ElevationSample
    /*
    @Test
    void testingNormalValue(){

        float[] table1 = {41, 2, 1f, 24, 3};
        float[] table2 = {32, 41};

        Edge edge1 = new Edge(1, 2, new PointCh(3 + SwissBounds.MIN_E, 3 + SwissBounds.MIN_N), new PointCh(3 + SwissBounds.MIN_E , 5 + SwissBounds.MIN_N), 2, Functions.sampled(table2, 2));
        Edge edge2 = new Edge(2, 4, new PointCh(3 + SwissBounds.MIN_E, 5 + SwissBounds.MIN_N), new PointCh(7 + SwissBounds.MIN_E , 5 + SwissBounds.MIN_N), 4, Functions.constant(41));
        Edge edge3 = new Edge(4, 3, new PointCh(7 + SwissBounds.MIN_E, 5 + SwissBounds.MIN_N), new PointCh(7 + SwissBounds.MIN_E , 1 + SwissBounds.MIN_N), 4, Functions.sampled(table1, 4));

        List<Edge> edgeList = new ArrayList<Edge>();

        edgeList.add(edge1);
        edgeList.add(edge2);
        edgeList.add(edge3);

        Route testRoute = new SingleRoute(edgeList);

        ElevationProfile actual = ElevationProfileComputer.elevationProfile(testRoute, 1);

        float[] actualList = {32, 36.5f, 41f, 41, 41, 41, 41, 2, 1, 24, 3};

        ElevationProfile expected = new ElevationProfile(10, actualList);

        assertEquals(expected.length(), actual.length());

        for(int i = 0; i < actual.elevationSamples.length; i++){
            assertEquals(expected.elevationSamples[i], actual.elevationSamples[i]);
        }

        for(int i = 0; i < expected.elevationSamples.length; i++){
            assertEquals(expected.elevationSamples[i], actual.elevationSamples[i]);
        }
    }

    @Test
    void testingEndValue(){

        float[] table1 = {1, 2, 1f, 24, 3};
        float[] table2 = {3, 41};

        Edge edge1 = new Edge(1, 2, new PointCh(3 + SwissBounds.MIN_E, 3 + SwissBounds.MIN_N), new PointCh(3 + SwissBounds.MIN_E , 7 + SwissBounds.MIN_N), 4, Functions.constant(1));
        Edge edge2 = new Edge(2, 4, new PointCh(3 + SwissBounds.MIN_E, 5 + SwissBounds.MIN_N), new PointCh(7 + SwissBounds.MIN_E , 5 + SwissBounds.MIN_N), 4, Functions.constant(Float.NaN));
        Edge edge3 = new Edge(4, 3, new PointCh(7 + SwissBounds.MIN_E, 5 + SwissBounds.MIN_N), new PointCh(5 + SwissBounds.MIN_E , 4 + SwissBounds.MIN_N), 1, Functions.constant(1));

        List<Edge> edgeList = new ArrayList<Edge>();

        edgeList.add(edge1);
        edgeList.add(edge2);
        edgeList.add(edge3);

        Route testRoute = new SingleRoute(edgeList);

        ElevationProfile actual = ElevationProfileComputer.elevationProfile(testRoute, 1);

        float[] actualList = {1,1,1,1,1,1,1,1,1,1};

        ElevationProfile expected = new ElevationProfile(9, actualList);

        assertEquals(expected.length(), actual.length());

        for(int i = 0; i < actual.elevationSamples.length; i++){
            assertEquals(expected.elevationSamples[i], actual.elevationSamples[i]);
        }

        for(int i = 0; i < expected.elevationSamples.length; i++){
            assertEquals(expected.elevationSamples[i], actual.elevationSamples[i]);
        }
    }
    */
}
