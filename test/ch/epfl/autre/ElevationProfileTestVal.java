package ch.epfl.autre;

import ch.epfl.javelo.routing.ElevationProfile;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ElevationProfileTestVal {
    @Test
    void ElevationAtWorks() {
        ElevationProfile firstProfile = new ElevationProfile(9, new float[]{384.75f, 384.6875f, 384.5625f, 384.5f, 384.4375f,
                384.375f, 384.3125f, 384.25f, 384.125f, 384.0625f});
        assertEquals(384.6875f, firstProfile.elevationAt(1));
    }

    @Test
    void ConstructorBoundaries(){
        float[] secondProfile=new float[]{384.75f, 384.6875f, 384.5625f, 384.5f, 384.4375f,
                384.375f, 384.3125f, 384.25f, 384.125f, 384.0625f};
        assertThrows(IllegalArgumentException.class, () -> {
            new ElevationProfile(-2,secondProfile);
        });
        float[] thirdProfile=new float[]{384.75f};
        assertThrows(IllegalArgumentException.class, () -> {
            new ElevationProfile(1,thirdProfile);
        });
    }

}