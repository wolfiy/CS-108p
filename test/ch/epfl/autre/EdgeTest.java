package ch.epfl.autre;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.SwissBounds;
import ch.epfl.javelo.routing.Edge;
import org.junit.jupiter.api.Test;

import java.util.function.DoubleUnaryOperator;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EdgeTest {

    @Test
    void testPointAt(){
        double expected = 4 / Math.sqrt(5);

        Edge tt = new Edge(2, 3, new PointCh(SwissBounds.MIN_E + 1,SwissBounds.MIN_N + 1), new PointCh(SwissBounds.MIN_E + 3,SwissBounds.MIN_N + 2), Math.sqrt(5), null);
        double actual = tt.positionClosestTo(new PointCh(SwissBounds.MIN_E + 2,SwissBounds.MIN_N + 3));

        assertEquals(expected, actual);

        expected = 15/Math.sqrt(13);

        Edge tt2 = new Edge(1, 3, new PointCh(SwissBounds.MIN_E + 1,SwissBounds.MIN_N + 4), new PointCh(SwissBounds.MIN_E + 3,SwissBounds.MIN_N + 1), Math.sqrt(13), null);
        double actual2 = tt2.positionClosestTo(new PointCh(SwissBounds.MIN_E + 4,SwissBounds.MIN_N + 1));

        assertEquals(expected, actual2,0.01);

        expected = Math.sqrt(13)-15/Math.sqrt(13);

        Edge tt3 = new Edge(1, 3, new PointCh(SwissBounds.MIN_E + 1,SwissBounds.MIN_N + 4), new PointCh(SwissBounds.MIN_E + 3,SwissBounds.MIN_N + 1), Math.sqrt(13), null);
        double actual3 = tt3.positionClosestTo(new PointCh(SwissBounds.MIN_E + 0,SwissBounds.MIN_N + 4));

        assertEquals(expected, actual3,0.01);

    }

    @Test
    void positionAtTest(){
        PointCh expected = new PointCh(SwissBounds.MIN_E + 2, SwissBounds.MIN_N + 2);

        Edge tt = new Edge(2, 3, new PointCh(SwissBounds.MIN_E + 1,SwissBounds.MIN_N + 1), new PointCh(SwissBounds.MIN_E + 3,SwissBounds.MIN_N + 3), Math.sqrt(2), null);
        PointCh actual = tt.pointAt(Math.sqrt(2)/2);

        assertEquals(expected, actual);

        expected = new PointCh(SwissBounds.MIN_E + 6, SwissBounds.MIN_N + 7);

        Edge tt2 = new Edge(7, 8, new PointCh(SwissBounds.MIN_E + 27,SwissBounds.MIN_N + 7), new PointCh(SwissBounds.MIN_E + 7,SwissBounds.MIN_N + 7), 20, null);
        PointCh actual2 = tt2.pointAt(21);

        assertEquals(expected, actual2);

        expected = new PointCh(SwissBounds.MIN_E + 7, SwissBounds.MIN_N + 5);

        Edge tt3 = new Edge(7, 8, new PointCh(SwissBounds.MIN_E + 7,SwissBounds.MIN_N + 7), new PointCh(SwissBounds.MIN_E + 7,SwissBounds.MIN_N + 11), 4, null);
        PointCh actual3 = tt3.pointAt(-2);

        assertEquals(expected, actual3);
    }

}
