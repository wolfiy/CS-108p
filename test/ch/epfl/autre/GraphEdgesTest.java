package ch.epfl.autre;

import ch.epfl.javelo.data.GraphEdges;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import static org.junit.jupiter.api.Assertions.*;

public class GraphEdgesTest {

    @Test
    public void isInvertedWorking(){
        ByteBuffer edgesBuffer = ByteBuffer.allocate(10);

        edgesBuffer.putInt(0, ~12);
        edgesBuffer.putShort(4, (short) 0x10_b);
        edgesBuffer.putShort(6, (short) 0x10_0);
        edgesBuffer.putShort(8, (short) 2022);

        IntBuffer profileIds = IntBuffer.wrap(new int[]{
                // Type : 3. Index du premier échantillon : 1.
                (3 << 30) | 1
        });

        ShortBuffer elevations = ShortBuffer.wrap(new short[]{
                (short) 0,
                (short) 6156, (short) 0xFEFF,
                (short) 0xFFFE, (short) 0xF000
        });

        GraphEdges edges = new GraphEdges(edgesBuffer, profileIds, elevations);

        assertTrue(edges.isInverted(0));

        ByteBuffer edgesBuffer1 = ByteBuffer.allocate(10);

        edgesBuffer1.putInt(0, 12);
        GraphEdges edges1 = new GraphEdges(edgesBuffer1, profileIds, elevations);
        assertFalse(edges1.isInverted(0));
    }

    @Test
    public void edgesReadingPropertiesWorking(){

        ByteBuffer edgesBuffer = ByteBuffer.allocate(20);

        edgesBuffer.putInt(0, ~12);
        edgesBuffer.putShort(4, (short) 0x10_b);
        edgesBuffer.putShort(6, (short) 0x10_0);
        edgesBuffer.putShort(8, (short) 2022);

        edgesBuffer.putInt(10, 35);
        edgesBuffer.putShort(14, (short) 0b000000000001000);
        edgesBuffer.putShort(16, (short) 0b000000011110000);
        edgesBuffer.putShort(18, (short) (Math.pow(2,16)-1));

        IntBuffer profileIds = IntBuffer.wrap(new int[]{
                // Type : 3. Index du premier échantillon : 1.
                (3 << 30) | 1
        });

        ShortBuffer elevations = ShortBuffer.wrap(new short[]{
                (short) 0,
                (short) 6156, (short) 0xFEFF,
                (short) 0xFFFE, (short) 0xF000
        });

        GraphEdges edges = new GraphEdges(edgesBuffer, profileIds, elevations);

        assertEquals(12, edges.targetNodeId(0));
        assertEquals(16.6875, edges.length(0));
        assertEquals(16.0, edges.elevationGain(0));
        assertEquals(2022, edges.attributesIndex(0));

        assertEquals(35, edges.targetNodeId(1));
        assertEquals(0.5, edges.length(1));
        assertEquals(15.0, edges.elevationGain(1));
        assertEquals((Math.pow(2,16)-1), edges.attributesIndex(1));
    }

    @Test
    public void profileSamplesWorkingCorrectly(){

        ByteBuffer edgesBuffer = ByteBuffer.allocate(50);

        edgesBuffer.putInt(0, ~12);
        edgesBuffer.putShort(4, (short) 0x10_b);
        edgesBuffer.putShort(6, (short) 0x10_0);
        edgesBuffer.putShort(8, (short) 2022);

        edgesBuffer.putInt(10, 35);
        edgesBuffer.putShort(14, (short) 0x10_b);
        edgesBuffer.putShort(16, (short) 0b000000011110000);
        edgesBuffer.putShort(18, (short) (Math.pow(2,16)-1));

        edgesBuffer.putInt(20, ~35);
        edgesBuffer.putShort(24, (short) 0x10_b);
        edgesBuffer.putShort(26, (short) 0b000000011110000);
        edgesBuffer.putShort(28, (short) (Math.pow(2,16)-1));

        edgesBuffer.putInt(30, 35);
        edgesBuffer.putShort(34, (short) 0x10_b);
        edgesBuffer.putShort(36, (short) 0b000000011110000);
        edgesBuffer.putShort(38, (short) (Math.pow(2,16)-1));

        edgesBuffer.putInt(40, 48);
        edgesBuffer.putShort(44, (short) 0x10_b);
        edgesBuffer.putShort(46, (short) 0b000000011110000);
        edgesBuffer.putShort(48, (short) (Math.pow(2,16)-1));

        IntBuffer profileIds = IntBuffer.wrap(new int[]{
                // Type : 3. Index du premier échantillon : 1.
                (3 << 30) | 1,
                0b10000000000000000000000000000101,
                0,
                0b01000000000000000000000000001101,
                0b11000000000000000000000000000101
        });

        ShortBuffer elevations = ShortBuffer.wrap(new short[]{
                (short) 0,

                (short) 6156, (short) 0xFEFF,
                (short) 0xFFFE, (short) 0xF000,

                (short) 6156, (short) 0x0F0F,
                (short) 0x0E01, (short) 0x0F00,
                (short) 0x0E01, (short) 0x0F00,
                (short) 0x0E01, (short) 0x0F00,

                (short) 6156, (short) 0xFEFF,
                (short) 0xFFFE, (short) 0xF000,
                (short) 6156, (short) 0x0F0F,
                (short) 0x0E01, (short) 0x0F00,
                (short) 0x0E01, (short) 0x0F00,
                (short) 0x0E01, (short) 0x0F00,

        });

        GraphEdges edges = new GraphEdges(edgesBuffer, profileIds, elevations);

        float[] expectedSamples = new float[]{
                384.0625f, 384.125f, 384.25f, 384.3125f, 384.375f,
                384.4375f, 384.5f, 384.5625f, 384.6875f, 384.75f
        };

        assertArrayEquals(expectedSamples, edges.profileSamples(0));

        float[] expectedSamples1 = new float[]{
                384.75f, 385.6875f, 386.625f, 387.5f, 387.5625f,
                388.5f, 388.5f, 389.375f, 389.4375f, 390.375f
        };

        assertArrayEquals(expectedSamples1, edges.profileSamples(1));

        assertFalse(edges.hasProfile(2));

        float[] expectedSamples2 = new float[]{
                384.75f, 4079.9375f, 4095.875f, 3840.0f, 384.75f, 240.9375f, 224.0625f, 240.0f, 224.0625f, 240.0f
        };

        assertArrayEquals(expectedSamples2, edges.profileSamples(3));
    }
}
