package ch.epfl.my;

import ch.epfl.javelo.Functions;
import org.junit.jupiter.api.Test;

import java.util.function.DoubleUnaryOperator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FunctionsTest {

    //Elle passe sans le delta. La fonction est trop précise.
    private final static double DELTA = 1e-10;

    @Test
    void constantNormal(){
        int cst = 8;
        int expected = cst;

        DoubleUnaryOperator fonctionConstante = Functions.constant(cst);

        assertEquals(cst, fonctionConstante.applyAsDouble(27.0));

    }


    @Test
    void constantInfinity(){
        int cst = -4;
        int expected = cst;


        DoubleUnaryOperator fonctionConstante = Functions.constant(cst);

        assertEquals(cst, fonctionConstante.applyAsDouble(Double.NEGATIVE_INFINITY));

    }


    @Test
    void constantNegatif(){
        int cst = -4;
        int expected = cst;


        DoubleUnaryOperator fonctionConstante = Functions.constant(cst);

        assertEquals(cst, fonctionConstante.applyAsDouble(-27));

    }


    @Test
    void sampledNormal(){
        float[] sampled = {2, 1.5f, 3, 2.5f, 2};
        double xMax = 8;

        DoubleUnaryOperator fonctionSampled = Functions.sampled(sampled, xMax);

        double x = 5;
        double expected = 2.75;
        assertEquals(expected, fonctionSampled.applyAsDouble(x));
    }

    @Test
    void sampledNormal2(){
        float[] sampled = {2, 1.5f, 3, 2.5f, 2};
        double xMax = 8;

        DoubleUnaryOperator fonctionSampled = Functions.sampled(sampled, xMax);

        double x = 4.1;
        double expected = 2.975;
        assertEquals(expected, fonctionSampled.applyAsDouble(x));
    }


    @Test
    void sampledNormal3(){
        float[] sampled = {2, 1.5f, 3, 2.5f, 2};
        double xMax = 8;

        DoubleUnaryOperator fonctionSampled = Functions.sampled(sampled, xMax);

        double x = 4;
        double expected = 3;
        assertEquals(expected, fonctionSampled.applyAsDouble(x));
    }


    @Test
    void sampledAfterList(){
        float[] sampled = {2, 1.5f};
        double xMax = 2;

        DoubleUnaryOperator fonctionSampled = Functions.sampled(sampled, xMax);

        double x = 4.1;
        double expected = 1.5;
        assertEquals(expected, fonctionSampled.applyAsDouble(x));
    }


    @Test
    void sampledOneValue(){
        float[] sampled = {2};
        double xMax = 0.5;

        assertThrows(IllegalArgumentException.class, ()->{

            DoubleUnaryOperator fonctionSampled = Functions.sampled(sampled, xMax);
            fonctionSampled.applyAsDouble(3);
        });


    }

    @Test
    void sampledNegatifXMax(){
        float[] sampled = {2, 8.0f, 13.2f};
        double xMax = -0.5;

        assertThrows(IllegalArgumentException.class, ()->{

            DoubleUnaryOperator fonctionSampled = Functions.sampled(sampled, xMax);
            fonctionSampled.applyAsDouble(3);
        });

    }

}
