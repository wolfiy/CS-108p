package ch.epfl.my;

import ch.epfl.javelo.gui.MapViewParameters;
import ch.epfl.javelo.projection.PointWebMercator;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static ch.epfl.my.TestUtile.r;
import static org.junit.jupiter.api.Assertions.*;

class MapViewParametersTest {

    @RepeatedTest(10)
    void PoinAtTestInverse(){

        int zoom = r(5,19);
        int x1 = r(0, 300);
        int y1 = r(0, 300);

        int x2 = r(0, 300);
        int y2 = r(0, 300);

        System.out.println("zoom " + zoom + " 1 (" + x1 + ";" + y1 + "), 2 (" + x2 + ";" + y2 + ")");

        MapViewParameters map = new MapViewParameters(zoom,x1,y1);

        PointWebMercator point = map.pointAt(x2,y2);

        assertEquals(x2, map.viewX(point));
        assertEquals(y2, map.viewY(point));
    }


    @Test
    void basicMap(){
        MapViewParameters map = new MapViewParameters(0, 0, 0);

        assertEquals(0, map.x());
        assertEquals(0, map.y());

        assertEquals(0, map.viewX(map.pointAt(0,0)));
        assertEquals(0, map.viewY(map.pointAt(0,0)));

    }

    @Test
    void basicMap2(){
        MapViewParameters map = new MapViewParameters(10, 0, 0);

        assertEquals(0, map.x());
        assertEquals(0, map.y());

        assertEquals(0, map.viewX(map.pointAt(0,0)));
        assertEquals(0, map.viewY(map.pointAt(0,0)));

    }

    @Test
    void basicMap3(){
        MapViewParameters map = new MapViewParameters(0, 0, 0);

        assertEquals(0, map.x());
        assertEquals(0, map.y());

        assertEquals(40, map.viewX(map.pointAt(40,50)));
        assertEquals(50, map.viewY(map.pointAt(40,50)));

    }

    @Test
    void basicMap4(){
        MapViewParameters map = new MapViewParameters(2, 0, 0);

        assertEquals(0, map.x());
        assertEquals(0, map.y());

        assertEquals(40, map.viewX(map.pointAt(40,50)));
        assertEquals(50, map.viewY(map.pointAt(40,50)));

    }

    @Test
    void basicMap5(){
        MapViewParameters map = new MapViewParameters(1, 10, 20);

        assertEquals(10, map.x());
        assertEquals(20, map.y());

        assertEquals(40, map.viewX(map.pointAt(40,50)));
        assertEquals(50, map.viewY(map.pointAt(40,50)));

    }

    @Test
    void basicMap6(){
        MapViewParameters map = new MapViewParameters(0, 10, 10);

        assertEquals(10, map.x());
        assertEquals(10, map.y());

        assertEquals(40, map.viewX(map.pointAt(40,50)));
        assertEquals(50, map.viewY(map.pointAt(40,50)));

    }


    @Test
    void testProf(){
        PointWebMercator p = PointWebMercator.of(10, 135735, 92327);
        assertEquals(6.40366, Math.toDegrees(p.lon()),  1e-5); // ~ 6.40366
        assertEquals(46.88366, Math.toDegrees(p.lat()), 1e-5); // ~46.88366


        MapViewParameters map = new MapViewParameters(10, 135735, 92327);
        PointWebMercator p2 = map.pointAt(0,0);
        assertEquals(6.40366, Math.toDegrees(p2.lon()),  1e-5); // ~ 6.40366
        assertEquals(46.88366, Math.toDegrees(p2.lat()), 1e-5); // ~46.88366

        assertEquals(0, map.viewX(p2));
        assertEquals(0, map.viewY(p2));

    }

    //46.508566280684086, 6.688339977954048
    @Test
    void testPointAt(){
        MapViewParameters map = new MapViewParameters(12, 543200, 370650);

        PointWebMercator point = map.pointAt(600, 300);

        System.out.println(Math.toDegrees(point.lat()) + " " + Math.toDegrees(point.lon()));

        assertEquals(46.508566280684086, Math.toDegrees(point.lat()), 1);
        assertEquals(6.688339977954048, Math.toDegrees(point.lon()), 1);

    }
}