package ch.epfl.my;

import ch.epfl.javelo.data.GraphEdges;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import static org.junit.jupiter.api.Assertions.*;

public class GraphEdgesTest {

    @Test
    void profileSamplesProf(){
        ByteBuffer edgesBuffer = ByteBuffer.allocate(10);
        // Sens : inversé. Nœud destination : 12.
        edgesBuffer.putInt(0, ~12);
        // Longueur : 0x10.b m (= 16.6875 m)
        edgesBuffer.putShort(4, (short) 0x10_b);
        // Dénivelé : 0x10.0 m (= 16.0 m)
        edgesBuffer.putShort(6, (short) 0x10_0);
        // Identité de l'ensemble d'attributs OSM : 2022
        edgesBuffer.putShort(8, (short) 2022);

        IntBuffer profileIds = IntBuffer.wrap(new int[]{
                // Type : 3. Index du premier échantillon : 1.
                (3 << 30) | 1
        });

        ShortBuffer elevations = ShortBuffer.wrap(new short[]{
                (short) 0,
                (short) 0x180C, (short) 0xFEFF,
                (short) 0xFFFE, (short) 0xF000
        });

        GraphEdges edges = new GraphEdges(edgesBuffer, profileIds, elevations);

        assertTrue(edges.isInverted(0));
        assertEquals(12, edges.targetNodeId(0));
        assertEquals(16.6875, edges.length(0));
        assertEquals(16.0, edges.elevationGain(0));
        assertEquals(2022, edges.attributesIndex(0));
        float[] expectedSamples = new float[]{
                384.0625f, 384.125f, 384.25f, 384.3125f, 384.375f,
                384.4375f, 384.5f, 384.5625f, 384.6875f, 384.75f
        };
        assertArrayEquals(expectedSamples, edges.profileSamples(0));
    }

    @Test
    void profileSamplesHard(){
        //10 bytes inutilisé
        ByteBuffer edgesBuffer = ByteBuffer.allocate(20);
        // Sens : pas inversé. Nœud destination : 27.
        edgesBuffer.putInt(10, 27);
        // Longueur : 0x101.b m (= 7 m) => 5 valeurs
        edgesBuffer.putShort(14, (short) (7*16));
        // Dénivelé : 0x10.0 m (= 237.0 m)
        edgesBuffer.putShort(16, (short) 0b1110_1101_0000);
        // Identité de l'ensemble d'attributs OSM : 0
        edgesBuffer.putShort(18, (short) 0);

        IntBuffer profileIds = IntBuffer.wrap(new int[]{
                // Type : 2. Index du premier échantillon : 1.
                0,
                (2 << 30) | 1
        });

        ShortBuffer elevations = ShortBuffer.wrap(new short[]{
                (short) 0b0000_0100_0000_1000,
                (short) 0b0000__1110_1101_0000,
                (short) 0b0011_0100_1100_0000,
                (short) 0b1100_0100_0000_0001,
                (short) 0b0010_0000_0000_0000
        });

        GraphEdges edges = new GraphEdges(edgesBuffer, profileIds, elevations);

        assertFalse(edges.isInverted(0));
        assertEquals(27, edges.targetNodeId(1));
        assertEquals(7, edges.length(1));
        assertEquals(237, edges.elevationGain(1));
        assertEquals(0, edges.attributesIndex(1));
        float[] expectedSamples = new float[]{
                237f,240.25f,236.25f,232.5f,232.5625f
        };
        assertArrayEquals(expectedSamples, edges.profileSamples(1));
    }

    @Test
    void isInvertedTestNormal(){
        //direction 1 (bon sens)
        //id 10
        //reste a 0
        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{0,0,0,10,(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000)});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{123,654,12,1});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0});

        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);
        assertFalse(graphEdges.isInverted(0b0));

    }

    @Test
    void isInvertedTestNormal2(){
        //direction 0 (mauvais sens)
        //id 10
        //reste a 0
        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{-128,0,0,10,0,0,0,0,(byte) r(0,1000)});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{123,654,12,1});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0});

        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);
        assertTrue(graphEdges.isInverted(0b0));

    }


    @Test
    void isInvertedTestNormal3(){
        //direction 0 (mauvais sens)
        //id 10
        //reste a 0
        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{-128,0,0,10,0,0,0,0,(byte) r(0,1000),0,/**/0,0,0,23,0,0,0,0,(byte) r(0,1000),(byte) r(0,100),/**/-128,0,0,10,0,0,0,0,(byte) r(0,1000),0});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{123,654,12,1,123,654,12,1,123,654,12,1});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0,0,0});

        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);
        assertTrue(graphEdges.isInverted(0));
        assertFalse(graphEdges.isInverted(1));
        assertTrue(graphEdges.isInverted(2));

    }

    @Test
    void targetNodeIdTestnormal(){
        byte expected = (byte) r(1,63);
        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{0,0,0, expected,(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000)});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{123,654,12,1});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0});

        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);

       assertEquals(expected,graphEdges.targetNodeId(0));
    }

    @Test
    void targetNodeIdTestnormal2(){
        byte expected = (byte) r(1,100);
        byte a = (byte) ~0;
        byte b = (byte) ~expected;
        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{a,a,a, b,(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000)});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{123,654,12,1});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0});

        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);

        assertEquals(expected,graphEdges.targetNodeId(0));
    }


    @Test
    void lengthTestNormal(){
        byte expected = (byte) r(1,63);
        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{0,0,0, 8,0,expected,(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000)});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{123,654,12,1});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0});

        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);
        assertEquals(expected/16.0, graphEdges.length(0));
    }

    @Test
    void lengthTestNormal2(){
        byte expected = (byte) r(1,63);
        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{0,0,0, 8,0,2,(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000),0/**/,0,0,0, 8,0,expected,(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000)});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{123,654,12,1,123,654,12,1});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0,0});

        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);
        assertEquals(expected/16.0, graphEdges.length(1));
    }


    @Test
    void lengthTestNormal3(){
        byte a = bp();
        double expected = (a+1024)/16.0;
        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{0,0,0, 8,4,a,b(),b(),(byte) r(0,1000),b()/**/,0,0,0, 8,0,23,(byte) r(0,1000),(byte) r(0,1000),(byte) r(0,1000),b()});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{123,654,12,1,123,654,12,1,123,654,12,1});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0,0,0});

        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);
        assertEquals(expected, graphEdges.length(0));
    }


    @Test
    void elevationTestNormal(){
        byte expected = bp();
        byte expected2 = bp();
        byte expected3 = bp();
        byte expected4 = bp();

        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{0,0,0,0,8,0,0,expected,b(),(byte) r(0,1000)/**/,0,0,0,8,0,23,0, expected2,(byte) r(0,1000),b(),/**/0,0,0,8,0,23,0, expected3,0,0,/**/0,0,0,8,0,b(),0,expected4,0,(byte) r(0,1000)});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{123,654,12,1,123,654,12,1,123,654,12,1});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0,0,0});

        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);
        assertEquals(expected/16.0, graphEdges.elevationGain(0));
        assertEquals(expected2/16.0, graphEdges.elevationGain(1));
        assertEquals(expected/16.0, graphEdges.elevationGain(0));
        assertEquals(expected3/16.0, graphEdges.elevationGain(2));
        assertEquals(expected4/16.0, graphEdges.elevationGain(3));

    }

    @Test
    void hasProfileTestNormal(){
        byte expected = bp();
        byte expected2 = bp();

        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{0,0,0,8,0,22,0,27,(byte) r(0,1000),0/**/,0,0,0,8,0,23,0, 56,(byte) r(0,1000),0,/**/0,0,0, 8,0,23,-1, -1,0,0,/**/0,0,0, 8,0,23,-128,0,(byte) r(0,1000),0});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{0,654,2<<30,1,/**/63,2,3,4,/**/-1,2,3,4});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0,0,0});


        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);
        assertFalse(graphEdges.hasProfile(0));
        assertFalse(graphEdges.hasProfile(1));
        assertTrue(graphEdges.hasProfile(2));
    }


    @Test
    void hasProfileTestNormal2(){
        byte expected = (byte) r(1,63);
        byte expected2 = (byte) r(1,63);

        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{0,0,0,8,0,22,0,27,(byte) r(0,1000)/**/,0,0,0,8,0,23,0, 56,(byte) r(0,1000),/**/0,0,0, 8,0,23,-1, -1,(byte) r(0,1000),/**/0,0,0, 8,0,23,-128,0,0});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{0,3<<30,1<<30,2<<30,28});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0,0,0});


        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);
        assertFalse(graphEdges.hasProfile(0));
        assertTrue(graphEdges.hasProfile(1));
        assertTrue(graphEdges.hasProfile(2));
        assertTrue(graphEdges.hasProfile(3));
        assertTrue(graphEdges.hasProfile(2));
        assertFalse(graphEdges.hasProfile(4));
    }


    @Test
    void profileSamplesTestNormal(){
        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{b(),b(),b(),b(),b(),b(),b(),27,(byte) r(0,1000),b()/**/,b(),b(),b(),b(),b(),23,b(), 56,(byte) r(0,1000),b(),/**/b(),b(),b(), b(),b(),23,-1, -1,(byte) r(0,1000),b(),/**/b(),b(),b(), b(),b(),23,-128,b(),(byte) r(0,1000),b()});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{-8,23,12,1/**/,27,2,3,4,/**/0,0,3,4});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{-1,-1,-1});


        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);

        assertArrayEquals(new float[0],graphEdges.profileSamples(2));
    }

    @Test
    void profileSamplesTestNormal2(){
        byte expected = (byte) r(1,63);
        byte expected2 = (byte) r(1,63);

        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{0,0,0,2,0,22,0,27,(byte) r(0,1000)/**/,0,0,0,3,0,23,0, 56,(byte) r(0,1000),/**/0,0,0, 8,0,23,-1, -1,(byte) r(0,1000),/**/0,0,0,1,0,23,0,0,(byte) r(0,1000)});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{0,0,0,expected,27,2,3,4,0,0,1,expected2});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0,0,0,0,0,0});


        GraphEdges graphEdges = new GraphEdges(edgesBuffer, profileIds, elevation);

        assertArrayEquals(new float[0],graphEdges.profileSamples(2));
    }


    @Test
    void attributesIndexTest(){
        ByteBuffer edgesBuffer = ByteBuffer.wrap(new byte[]{0,0,0,8,0,22,0,27,(byte) r(0,1000)/**/,0,0,0,8,0,23,0, 56,(byte) r(0,1000),/**/0,0,0, 8,0,23,-1, -1,(byte) r(0,1000),/**/0,0,0, 8,0,23,-128,0,(byte) r(0,1000)});
        IntBuffer profileIds = IntBuffer.wrap(new int[]{0,654,12,1,27,2,3,4,-45,2,3,4});
        ShortBuffer elevation = ShortBuffer.wrap(new short[]{0,0,0});
    }





    private int r(int minimum, int maximum){
        int random = (int) (Math.random()*(maximum-minimum)+minimum);
        //System.out.println(random + " ");
        return random;
    }
    private byte b(){

        return (byte) r(Byte.MIN_VALUE, Byte.MAX_VALUE);
    }
    private byte bp(){
        return (byte) r(0, Byte.MAX_VALUE);
    }
}
