package ch.epfl.my;

import org.junit.jupiter.api.Test;

import static ch.epfl.javelo.Bits.*;
import static org.junit.jupiter.api.Assertions.*;

class BitsTest {



    @Test
    void testExtracSignedNormal(){
        int value = 0b1001000;
        int start = 3;
        int length = 4;
        int expected = ~0b110;

        assertEquals(expected, extractSigned(value,start,length));

    }


    @Test
    void testExtracSignedNormal2(){
        int value = 0b1001000;
        int start = 3;
        int length = 5;
        int expected = 0b01001;

        assertEquals(expected, extractSigned(value,start,length));

    }
    @Test
    void testExtractUnsignedNormal() {
        int a = 0b0001011100;
        assertEquals(0b111, extractUnsigned(a, 2, 3));
    }

    @Test
    void testExtractUnsignedNormal2() {
        int a = 1;
        assertEquals(1, extractUnsigned(a, 0, 1));
    }


    @Test
    void testExtractUnsignedNormal3() {
        int a = 1;
        assertEquals(0, extractUnsigned(a, 4, 1));
    }


    @Test
    void testExtractUnsignedexceptionNegatif() {
        int a = 0b0001011100;
        assertThrows(IllegalArgumentException.class, ()-> {
            extractUnsigned(a, 20, -4);
        });
    }

    @Test
    void testExtractUnsignedexceptionNegatif2() {
        int a = 0b0001011100;
        assertThrows(IllegalArgumentException.class, ()-> {
            extractUnsigned(a, -4, 20);
        });
    }
}