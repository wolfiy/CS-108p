package ch.epfl.my;

import ch.epfl.javelo.projection.SwissBounds;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SwissBoundsTest {

    @Test
    void SwissBoundsTestNormalCase(){
        assertTrue(SwissBounds.containsEN(2_600_000, 1_100_000));
    }

    @Test
    void SwissBoundsTestNormalCase2(){
        assertTrue(!SwissBounds.containsEN(3_600_000, 1_100_000));
    }

    @Test
    void SwissBoundsTestNormalCase3(){
        assertTrue(!SwissBounds.containsEN(00_000, 100_000));
    }


    @Test
    void SwissBoundsTestNull(){
        assertTrue(!SwissBounds.containsEN(0, 0));
    }

    @Test
    void SwissBoundsTestNormalCaseInfinity(){
        assertTrue(!SwissBounds.containsEN(Double.POSITIVE_INFINITY, 1_100_000));
    }

    @Test
    void SwissBoundsTestNormalCaseInfinity2(){
        assertTrue(!SwissBounds.containsEN(Double.NEGATIVE_INFINITY, 1_100_000));
    }

    @Test
    void SwissBoundsTestLimitCase(){
        assertTrue(SwissBounds.containsEN(SwissBounds.MIN_E,SwissBounds.MIN_N));
    }
}
