package ch.epfl.my;

import ch.epfl.javelo.routing.ElevationProfile;
import org.junit.jupiter.api.Test;

import static ch.epfl.my.TestUtile.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class ElevationProfileTest {


    @Test
    void maxValue(){
        int length = ipp();
        ElevationProfile elevationProfile = new ElevationProfile(length,new float[]{3,28.7f, 42.5f, -17.3f, -Float.MAX_VALUE, Float.MAX_VALUE-1});


        assertEquals(length, elevationProfile.length());
        assertEquals(-Float.MAX_VALUE, elevationProfile.minElevation());
        assertEquals(Float.MAX_VALUE-1, elevationProfile.maxElevation());

        assertTrue(Float.MAX_VALUE < elevationProfile.totalAscent());
        //assertTrue(Float.MAX_VALUE < elevationProfile.totalDescent());

        assertEquals(3, elevationProfile.elevationAt(0));

        assertEquals(Float.MAX_VALUE, elevationProfile.elevationAt(length));

        assertEquals(-Float.MAX_VALUE, elevationProfile.elevationAt(4.0/5.0*length),1e25);

        assertEquals(3*0.1 + 28.7*0.9, elevationProfile.elevationAt(0.9/5*length), 1e-5);

    }



    @Test
    void normal(){
        int length = ipp();
        ElevationProfile elevationProfile = new ElevationProfile(length, new float[]{3,28.7f, 42.5f, -17.3f, 12, -1000.22f});


        assertEquals(length, elevationProfile.length());
        assertEquals(-1000.22f, elevationProfile.minElevation());
        assertEquals(42.5, elevationProfile.maxElevation());


        //68.8

        assertEquals(68.8, elevationProfile.totalAscent(), 1e-1);
        assertEquals(1072.02, elevationProfile.totalDescent(), 1e-1);



        assertEquals(3, elevationProfile.elevationAt(0));

        assertEquals(-1000.22f, elevationProfile.elevationAt(length));

        assertEquals(12f, elevationProfile.elevationAt(4.0/5.0*length), 1e-10);

        assertEquals((float)(3*0.1 + 28.7*0.9), elevationProfile.elevationAt(0.9/5*length), 1e-5);

    }
}
