package ch.epfl.my;

import ch.epfl.javelo.projection.PointWebMercator;
import ch.epfl.javelo.projection.WebMercator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WebMercatorTest {

    final static double delta = 1e-10;

    final static double lon = 6.5790_772;//degres
    final static double lat = 46.521_8976;//degres
    final static double x = 0.518_275_214_444;
    final static double y = 0.353_664_894_749;
    final static double x19 = 69_561_722;
    final static double y19 = 47_468_099;


    @Test
    void x() {

        assertEquals(x, WebMercator.x(Math.toRadians(lon)), delta);
    }

    @Test
    void y() {
        assertEquals(y, WebMercator.y(Math.toRadians(lat)), delta);
    }

    @Test
    void lon() {
        assertEquals(Math.toRadians(lon), WebMercator.lon(x), delta);
    }

    @Test
    void lat() {
        assertEquals(Math.toRadians(lat), WebMercator.lat(y), delta);
    }


}