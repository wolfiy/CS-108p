package ch.epfl.my;

import ch.epfl.javelo.Preconditions;
import ch.epfl.javelo.gui.TileManager;
import javafx.scene.image.Image;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class TileManagerTest {

    @Test
    void testChargerImage(){
        //dosier où sont stocké les tuiles
        Path path = Path.of("tuile");
        String serverName = "tile.openstreetmap.org";

        TileManager tileManager = new TileManager(path, serverName);
        TileManager.TileId tile = new TileManager.TileId(19, 271725, 185422);

        //tileManager.imageForTileAt(tile);

    }

    @Test
    void testChargerImage2(){
        //dosier où sont stocké les tuiles
        Path path = Path.of("tuile");
        String serverName = "tile.openstreetmap.org";

        TileManager tileManager = new TileManager(path, serverName);
        TileManager.TileId tile = new TileManager.TileId(19, 0,0);
        TileManager.TileId tile2 = new TileManager.TileId(19, 0, 1);
        TileManager.TileId tile3 = new TileManager.TileId(19, 0, 2);

        TileManager.TileId tile4 = new TileManager.TileId(19, 271725, 185422);
        Image i;
        i = tileManager.imageForTileAt(tile);
        assertNotNull(i);
        assertFalse(i.isError());
        i = tileManager.imageForTileAt(tile2);
        assertNotNull(i);
        assertFalse(i.isError());
        i = tileManager.imageForTileAt(tile3);
        assertNotNull(i);
        assertFalse(i.isError());
        i = tileManager.imageForTileAt(tile4);
        assertNotNull(i);
        assertFalse(i.isError());
        i = tileManager.imageForTileAt(tile);
        assertNotNull(i);
        assertFalse(i.isError());
    }




    @Test
    void tuilePasPossible(){
    assertThrows(IllegalArgumentException.class, () -> {
            TileManager.TileId tile = new TileManager.TileId(0, 0,10);

        });
        assertThrows(IllegalArgumentException.class, () -> {
            new TileManager.TileId(0, 1,0);

        });
        assertThrows(IllegalArgumentException.class, () -> {
            new TileManager.TileId(-1, 0,1);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            new TileManager.TileId(1, 0,5);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            new TileManager.TileId(1, 2,1);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            new TileManager.TileId(1, 0,2);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            new TileManager.TileId(20, 0,0);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            new TileManager.TileId(20, 10,200);
        });

    }



}