package ch.epfl.my;

import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.gui.*;
import ch.epfl.javelo.routing.CityBikeCF;
import ch.epfl.javelo.routing.RouteComputer;
import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.nio.file.Path;
import java.util.function.Consumer;

public final class Stage11Test extends Application {
    public static void main(String[] args) { launch(args); }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Graph graph = Graph.loadFrom(Path.of("ch_west"));
        Path cacheBasePath = Path.of("osm-cache");
        String tileServerHost = "tile.openstreetmap.org";
        String tileServerHost2 = "cartodb-basemaps-1.global.ssl.fastly.net/light_all";
        String tileServerHost3 = "cartodb-basemaps-1.global.ssl.fastly.net/dark_all";
        TileManager tileManager =
                new TileManager(cacheBasePath, tileServerHost);

        //routeBean
        RouteComputer rc = new RouteComputer(graph, new CityBikeCF(graph));
        RouteBean routeBean = new RouteBean(rc);


        Consumer<String> errorConsumer = new ErrorConsumer();


        routeBean.setHighlightedPosition(1000.0);


        AnnotatedMapManager annotatedMapManager =
                new AnnotatedMapManager(graph, tileManager, routeBean, errorConsumer);


        Pane mainPane = annotatedMapManager.pane();
        primaryStage.setMinWidth(600);
        primaryStage.setMinHeight(300);
        primaryStage.setScene(new Scene(mainPane));
        primaryStage.show();
    }

    private static final class ErrorConsumer
            implements Consumer<String> {
        @Override
        public void accept(String s) { System.out.println(s); }
    }
}