package ch.epfl.my;

import ch.epfl.javelo.Q28_4;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Q28_4Test {

    private final static double delta = 1e-10;
    @Test
    void ofInt() {
        int a = Q28_4.ofInt(27);
        assertEquals(432, a);
    }

    @Test
    void asDouble() {
        int a = 1;
        int b = -15;
        assertEquals(1.0/16.0, Q28_4.asDouble(a), delta);
        assertEquals(-15.0/16.0, Q28_4.asDouble(b), delta);

    }

    @Test
    void asFloat() {
        int a = 322;
        assertEquals(322.0/16.0, Q28_4.asDouble(a), delta);
    }
}