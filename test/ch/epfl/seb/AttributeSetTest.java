package ch.epfl.seb;

import ch.epfl.javelo.data.Attribute;
import ch.epfl.javelo.data.AttributeSet;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Attr;

import static org.junit.jupiter.api.Assertions.*;

class AttributeSetTest {

    public static void main(String[] args) {
        countAttributes();
    }

    long input = 0b100000000000000010;
    long allFalse = 0b00000000000000000000000000000000000000000000000000000000000000L;
    long allTrue = 0b11111111111111111111111111111111111111111111111111111111111111L;

   @Test
   void constructorWorksAsExpected() {
       var a = new AttributeSet(input);
       System.out.println(a.toString());
   }

    @Test
    void ofWorksOnGivenData() {
        var a = AttributeSet.of(Attribute.HIGHWAY_SERVICE);
        System.out.println(a);

        var b = new AttributeSet(input);
        System.out.println(AttributeSet.of(Attribute.HIGHWAY_SERVICE, Attribute.ACCESS_NO, Attribute.ICN_YES));
    }

    @Test
    void containsWorkOnGivenBits() {
        var a = new AttributeSet(input);
        var t = new AttributeSet(allTrue);
        var f = new AttributeSet(allFalse);
        Attribute yes = Attribute.HIGHWAY_TRACK;
        Attribute yes2 = Attribute.TRACKTYPE_GRADE1;
        Attribute no = Attribute.ACCESS_NO;
        Attribute no2 = Attribute.VEHICLE_NO;
        assertTrue(a.contains(yes));
        assertTrue(a.contains(yes2));
        assertFalse(a.contains(no));
        assertFalse(a.contains(no2));
        assertFalse(f.contains(yes));
        assertFalse(f.contains(yes2));
        assertFalse(f.contains(no));
        assertFalse(f.contains(no2));
        assertTrue(t.contains(yes));
        assertTrue(t.contains(yes2));
        assertTrue(t.contains(no));
        assertTrue(t.contains(no2));
    }

    @Test
    void containsWorksOnAttributes() {
        // Uhhh not what i meant to do
        assertEquals("highway", Attribute.HIGHWAY_SERVICE.key());
        assertEquals("opposite_lane", Attribute.CYCLEWAY_OPPOSITE_LANE.value());
        assertTrue((Attribute.ICN_YES.key()).equals("icn"));
    }

    // Counting number of attributes
    public static void countAttributes() {
        System.out.println(Attribute.values().length);
    }

    @Test
    void toStringIsWorkingAsInDocumentation() {
        AttributeSet set = AttributeSet.of(Attribute.TRACKTYPE_GRADE1, Attribute.HIGHWAY_TRACK);
        assertEquals("{highway=track,tracktype=grade1}", set.toString());
    }

    @Test
    void intersectsWorksAsExpected() {
       long flag = 0b100000000000000010;
       var a = new AttributeSet(input);
       var b = new AttributeSet(flag);
       var c = AttributeSet.of(Attribute.TRACKTYPE_GRADE1, Attribute.HIGHWAY_TRACK);
       var d = new AttributeSet(0b100000000011);
       var e = AttributeSet.of(Attribute.ACCESS_NO, Attribute.HIGHWAY_TRACK);
       var f = new AttributeSet(~flag & 0b111111111111111);
       assertTrue(a.intersects(b));
       assertTrue(a.intersects(c));
       assertTrue(a.intersects(d));
       assertTrue(a.intersects(e));
       assertFalse(a.intersects(f));
    }
}