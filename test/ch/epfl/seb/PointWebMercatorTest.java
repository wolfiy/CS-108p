package ch.epfl.seb;

import ch.epfl.javelo.projection.Ch1903;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.PointWebMercator;
import org.junit.jupiter.api.Test;

import static ch.epfl.test.TestRandomizer.newRandom;
import static org.junit.jupiter.api.Assertions.*;

class PointWebMercatorTest {

    final static double DELTA = 1e-10;
    final static double DELTA_2 = 1e-7;
    final static double DELTA_3 = 1e+1;


    final double xNapoleon = 0.518275214444;
    final double yNapoleon = 0.353664894749;
    final double lonNapoleon = 6.5790772;
    final double latNapoleon = 46.5218976;
    final double xNapoleon19 = 69561722;
    final double yNapoleon19 = 47468099;

    @Test
    void pointWebMercatorCantConstructWithInvalidInput() {
        assertThrows(IllegalArgumentException.class, () -> {
            // Negative x
            new PointWebMercator(-1.0, 0.5);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            // Negative x
            new PointWebMercator(newRandom().nextDouble(Double.MIN_VALUE, -1e-11), 0.5);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            // Negative y
            new PointWebMercator(0.5, newRandom().nextDouble(Double.MIN_VALUE, -1e-11));
        });
        assertThrows(IllegalArgumentException.class, () -> {
            // x too big
            new PointWebMercator(newRandom().nextDouble(1.00000000001, Double.MAX_VALUE), 0.5);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            // y to big
            new PointWebMercator(0.5, newRandom().nextDouble(1.00000000001, Double.MAX_VALUE));
        });
    }

    @Test
    void ofWorksOnKnownValues() {
        // kreslotim discord
        var actual1 = PointWebMercator.of(19, 0.518275214444, 0.353664894749).x();
        var expected1 = 3.861451256603003E-9;
        assertEquals(expected1, actual1);
    }

    @Test
    void ofPointChWorksOnKnownData() {
        var e = Ch1903.e(Math.toRadians(lonNapoleon), Math.toRadians(latNapoleon));
        var n = Ch1903.n(Math.toRadians(lonNapoleon), Math.toRadians(latNapoleon));
        PointCh p = new PointCh(e,n);
        System.out.println(e);
        System.out.println(n);
        var expectedX = xNapoleon;
        var expectedY = yNapoleon;
        var actualX = PointWebMercator.ofPointCh(p).x();
        var actualY = PointWebMercator.ofPointCh(p).y();
        System.out.println(expectedX + " " + expectedY + " " + actualX + " " + actualY);
        assertEquals(expectedX, actualX, DELTA_2);
        assertEquals(expectedY, actualY, DELTA_2);
    }

    // les tests sur les valeurs connues.
    @Test
    public void xyAtZoomLevelWorksAsExpected() {
        var actual1 = new PointWebMercator(xNapoleon, yNapoleon).xAtZoomLevel(19);
        var expected1 = 6.956172176138646e7;
        var actual2 = new PointWebMercator(xNapoleon, yNapoleon).yAtZoomLevel(19);
        var expected2 = yNapoleon19;
        assertEquals(expected1, actual1, DELTA_3);
        assertEquals(expected2, actual2, DELTA_3);
    }

    @Test
    void ofWorksOnZoomLevel19() {
        var actual = PointWebMercator.of(19, xNapoleon, yNapoleon);
        var expectedX = 69561722;
        var expectedY = 47468099;
        System.out.println(actual);
    }

    @Test
    void ofPointChWorksOnGivenData() {
        PointWebMercator expectedNapoleon = new PointWebMercator(0.518275214444, 0.353664894749);
        PointCh napoleonCh = new PointCh(Ch1903.e(Math.toRadians(6.5790772), Math.toRadians(46.5218976)), Ch1903.n(Math.toRadians(6.5790772), Math.toRadians(46.5218976)));
        PointWebMercator actualNapoleon = PointWebMercator.ofPointCh(napoleonCh);
        assertEquals(expectedNapoleon.x(), actualNapoleon.x(), DELTA_2);
        assertEquals(expectedNapoleon.y(), actualNapoleon.y(), DELTA_2);
    }

    @Test
    void lonWorksOnKnownData() {
        var expected = lonNapoleon;
        var actual = new PointWebMercator(xNapoleon, yNapoleon).lon();
        assertEquals(Math.toRadians(lonNapoleon), actual, DELTA_2);
    }

    @Test
    void latWorksOnKnownData() {
        var expected = latNapoleon;
        var actual = new PointWebMercator(xNapoleon, yNapoleon).lat();
        assertEquals(Math.toRadians(latNapoleon), actual, DELTA_2);
    }
}