package ch.epfl.seb;

import ch.epfl.javelo.data.GraphSectors;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.SwissBounds;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class GraphSectorsTest {

    ByteBuffer buffer = ByteBuffer.wrap(new byte[16384]);
    GraphSectors gs = new GraphSectors(buffer);

    static PointCh Bern = new PointCh(2600000, 1200000);

    static PointCh pointChPiazza = new PointCh(SwissBounds.MIN_E, SwissBounds.MIN_N);
    final static double distancePiazza = 2730.0;
    static public ByteBuffer bufferPiazza = ByteBuffer.wrap(new byte[16384]);

    @Test
    void sectorInAreaThrowsExceptionOnNullCenter() {
        assertThrows(IllegalArgumentException.class, () -> {
            gs.sectorsInArea(null, 10.0);
        });
    }

    @Test
    void sectorInAreaThrowsExceptionOnNegativeDistance() {
        assertThrows(IllegalArgumentException.class, () -> {
            gs.sectorsInArea(Bern, -10.0);
        });
    }

    @Test
    void sectorsInAreaWorksOnKnownData() {
        int numberOfBytes = 16384 * (4 + 2);
        ByteBuffer gsPiazzaExpected = ByteBuffer.wrap(new byte[numberOfBytes]);
        bufferPiazza.putInt(0, 0);
        bufferPiazza.putShort(4, (short) 1);
        bufferPiazza.putInt(6, 128);
        bufferPiazza.putShort(10, (short) 129);
        GraphSectors gsPiazza = new GraphSectors(bufferPiazza);

        var actual = gsPiazza.sectorsInArea(pointChPiazza, distancePiazza);
        var expected = new ArrayList<>();
        expected.add(0);
        expected.add(1);
        expected.add(128);
        expected.add(129);
        System.out.println("actual size:" + actual.size());
        System.out.println("expected size:" + expected.size());
        for (int i = 0; i < actual.size(); ++i) {

        }
        for (int i = 0; i < actual.size(); ++i) {
            System.out.println(actual.get(i));
            //System.out.println(expected.get(i));
            // System.out.println(expected.get(i));
        }
        // assertArrayEquals(expected, actual);
    }

    @Test
    void sectorsIsAreaWorksOnBern() {
        int numberOfBytes = 16384 * (4 + 2);
        ByteBuffer b = ByteBuffer.wrap(new byte[numberOfBytes]);
        // Width — où est Berne
        double leftToBern = Bern.e() - SwissBounds.MIN_E;
        double oneSectorWidth = (SwissBounds.MAX_E - SwissBounds.MIN_E)/128;
        // 42.17
        double sectorOfBernWidth = leftToBern / oneSectorWidth;
        double bottomToBern = Bern.n() - SwissBounds.MIN_N;
        double oneSectorHeight = (SwissBounds.MAX_N - SwissBounds.MIN_N)/128;
        // 72.39
        double sectorOfBernHeight = bottomToBern / oneSectorHeight;
        //System.out.println((SwissBounds.MAX_E - SwissBounds.MIN_E)/128 * (Bern.e() - SwissBounds.MIN_E));
        //System.out.println(leftToBern);
        System.out.println(sectorOfBernWidth);
        System.out.println(sectorOfBernHeight);
        // bcp bcp ... 40+128
        // 0 1 2 3 ... 40 41 42 43
        b.putInt((43+128*72), 1);
        b.putShort((43+128*72), (short) 8);

        GraphSectors gsb = new GraphSectors(b);
        gsb.sectorsInArea(Bern, 10);
        System.out.println((gsb.sectorsInArea(Bern, 10).size()));
        System.out.println(b.getInt(43+128*72));
        System.out.println(b.getShort(0));
        System.out.println(b.getShort(43+128*73));

        var actual = gsb.sectorsInArea(pointChPiazza, distancePiazza);
        var expected = new ArrayList<>();
        expected.add(1);

        System.out.println("actual size:" + actual.size());
        System.out.println("expected size:" + expected.size());
    }

}