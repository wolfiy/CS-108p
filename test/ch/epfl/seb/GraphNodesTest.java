package ch.epfl.seb;

import ch.epfl.javelo.Q28_4;
import ch.epfl.javelo.data.GraphNodes;
import ch.epfl.javelo.projection.Ch1903;
import org.junit.jupiter.api.Test;

import java.nio.IntBuffer;

import static org.junit.jupiter.api.Assertions.*;

class GraphNodesTest {

    final double DELTA = 1e-10;

    final double xNapoleon = 0.518275214444;
    final double yNapoleon = 0.353664894749;
    final double lonNapoleon = 6.5790772;
    final double latNapoleon = 46.5218976;
    final double lonrNapoleon = Math.toRadians(lonNapoleon);
    final double latrNapoleon = Math.toRadians(latNapoleon);
    final double eNapoleon = Ch1903.e(lonrNapoleon, latrNapoleon);
    final double nNapoleon = Ch1903.n(lonrNapoleon, latrNapoleon);

    @Test
    void countWorksAsExpected() {
        IntBuffer intBuffer = IntBuffer.wrap(new int[]{1, 2, 3});
        GraphNodes g = new GraphNodes(intBuffer);
        var actual = g.count();
        var expected = 1;
        assertTrue(expected == actual);
        assertEquals(expected, actual);
    }

    @Test
    void nodeEWorksAsExpected() {
        var expected = (int) eNapoleon;
        IntBuffer i = IntBuffer.wrap(new int[]{(int) eNapoleon, 1, 1});
        GraphNodes g = new GraphNodes(i);
        //var actual = g.nodeE(0);
        //assertEquals(expected, actual, DELTA);

        System.out.println(eNapoleon);
        System.out.println(expected);
    }

    @Test
   void outDegreeWorksOnGivenValues() {
        // Nb arrete entre 0 et 15 compris
        IntBuffer buffer = IntBuffer.wrap(new int[]    {0, 0, 4<<28,
                                                        1, 0, 0,
                                                        50, 34567, 15<<28});
        IntBuffer buffer2 = IntBuffer.wrap(new int[] {0, 0, 3,
                                                      1, 0, 0b11111111111111111111111111111111,
                                                      50, 34567, 0b10011010100101010100101101011110});
        GraphNodes g = new GraphNodes(buffer);
        GraphNodes gg = new GraphNodes(buffer2);
        var a1 = g.outDegree(0);
        var a2 = g.outDegree(1);
        var a3 = g.outDegree(2);
        var e1 = 4;
        var e2 = 0;
        var e3 = 15;

        var a5 = gg.outDegree(0);
        var a6 = gg.outDegree(1);
        var a7 = gg.outDegree(2);
        var e5 = 0b0000;
        var e6 = 0b1111;
        var e7 = 0b1001;

        assertEquals(e1, a1, DELTA);
        assertEquals(e2, a2, DELTA);
        assertEquals(e3, a3, DELTA);

        //assertEquals(e5, a5, DELTA);
        assertEquals(e6, a6, DELTA);
        assertEquals(e7, a7, DELTA);

        System.out.println(g.outDegree(1));
        System.out.println(g.outDegree(2));
    }

    @Test
    void givenTestsPass() {
        IntBuffer b = IntBuffer.wrap(new int[] {2_600_000 << 4,
                                                1_200_000 << 4,
                                                0x2_000_1234});
        GraphNodes ns = new GraphNodes(b);
        assertEquals(1, ns.count());
        assertEquals(2_600_000, ns.nodeE(0));
        //assertEquals(1_200_000, ns.nodeN(0));
        assertEquals(2, ns.outDegree(0));
        assertEquals(0x1234, ns.edgeId(0, 0));
        assertEquals(0x1235, ns.edgeId(0, 1));
    }

   @Test
   void givenEN() {
       IntBuffer b = IntBuffer.wrap(new int[] {2_600_000 << 4,
                                                1_200_000 << 4,
                                                0x2_000_1234});
       GraphNodes ns = new GraphNodes(b);
       assertEquals(2_600_000, ns.nodeE(0));
       assertEquals(1_200_000, ns.nodeN(0));
   }

    @Test
    void givenDegree() {
        IntBuffer b = IntBuffer.wrap(new int[] {2_600_000 << 4,
                                                1_200_000 << 4,
                                                0x2_000_1234});
        GraphNodes ns = new GraphNodes(b);
        assertEquals(2, ns.outDegree(0));
    }

    @Test
    void givenEdgeId() {
        IntBuffer b = IntBuffer.wrap(new int[] {2_600_000 << 4,
                                                1_200_000 << 4,
                                                0x2_000_1234});
        GraphNodes ns = new GraphNodes(b);
        assertEquals(0x1234, ns.edgeId(0, 0));
        assertEquals(0x1235, ns.edgeId(0, 1));
    }

    @Test
    public void  count() {
        IntBuffer a = IntBuffer.wrap(new int[]{0,0,0,2,3,4});
        assertEquals(2, new GraphNodes(a).count());
    }

    @Test
    void nodeE() {
        IntBuffer a = IntBuffer.wrap(new int[]{1,1,1,0,1,1});
        assertEquals(0, new GraphNodes(a).nodeE(1));
    }

    @Test
    void nodeN() {
        int b = Q28_4.ofInt(12);
        IntBuffer a = IntBuffer.wrap(new int[]{8,8,8,0,b,0});
        assertEquals(12, new GraphNodes(a).nodeN(1));
    }

    @Test
    void outDegree() {
        // int c = Q28_4.ofInt(268435456);  // 268435456 = 2^28 = 000100...0
        IntBuffer a = IntBuffer.wrap(new int[]{0,0,268435456});
        assertEquals(1, new GraphNodes(a).outDegree(0));

        IntBuffer aa = IntBuffer.wrap(new int[]{0,0,1073741824}); //2^31
        assertEquals(4, new GraphNodes(aa).outDegree(0));
    }

    @Test
    void edgeId() {
        IntBuffer a = IntBuffer.wrap(new int[]{0,0,8 | 3 <<28});
        assertEquals(10, new GraphNodes(a).edgeId(0,2));
    }
}