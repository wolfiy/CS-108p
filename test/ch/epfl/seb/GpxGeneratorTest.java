package ch.epfl.seb;

import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.routing.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class GpxGeneratorTest {

    @Test
    void gpxMaker() throws IOException {
        Graph graph = Graph.loadFrom(Path.of("lausanne"));
        CostFunction cost = new CityBikeCF(graph);
        RouteComputer computer = new RouteComputer(graph, cost);
        Route route = computer.bestRouteBetween(159049, 117669);
        ElevationProfile ele = ElevationProfileComputer.elevationProfile(route, 1);

        GpxGenerator.writeGpx("leGPX.gpx", route, ele);
    }


    public static void main(String[] args) throws IOException {
        Graph g = Graph.loadFrom(Path.of("lausanne"));
        CostFunction cf = new CityBikeCF(g);
        RouteComputer rc = new RouteComputer(g, cf);
        Route r = rc.bestRouteBetween(159049, 117669);
        ElevationProfile e = ElevationProfileComputer.elevationProfile(r, 1);

        GpxGenerator.writeGpx("test.gpx", r, e);
    }
}