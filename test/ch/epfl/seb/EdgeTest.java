package ch.epfl.seb;

import ch.epfl.javelo.Functions;
import ch.epfl.javelo.Math2;
import ch.epfl.javelo.data.*;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.routing.Edge;
import ch.epfl.my.TestUtile;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.nio.file.Path;
import java.util.List;
import java.util.function.DoubleUnaryOperator;

import static ch.epfl.my.TestUtile.i;
import static org.junit.jupiter.api.Assertions.*;

class EdgeTest {

    Path basePath = Path.of(TestUtile.LAUSANNE_PATH);

    // Handmade buffers, used for testing Edge of()
    IntBuffer graphNodesBuffer = IntBuffer.allocate(10);
    ByteBuffer graphSectorsBuffer = ByteBuffer.allocate(320);
    ByteBuffer graphEdgeEdgesBuffer = ByteBuffer.allocate(320);
    IntBuffer graphEdgeProfileIds = IntBuffer.allocate(10);
    ShortBuffer graphEdgeElevation = ShortBuffer.allocate(160);
    List<AttributeSet> graphEdgeAttributeSetList;

    @Test
    void positionClosestToWorks() {
        try {
            DoubleUnaryOperator profile = Functions.constant(9);
            Graph graphLsn = Graph.loadFrom(basePath);

            PointCh inf = new PointCh(2532864.1, 1152298.5);
            PointCh lutry = new PointCh(2542202.6, 1150383.9);
            PointCh chuv = new PointCh(2538922.3, 1152937.6);
            Edge edge = new Edge(0, 900000, lutry, inf, 10000, profile);

            // 9338.5,-1914.6
            double AB_x = inf.e() - lutry.e();
            double AB_y = inf.n() - lutry.n();
            double sqr = AB_x*AB_x + AB_y*AB_y;
            double AB_length = Math.sqrt(sqr);
            // 3280.3, -2553.7
            double AP_x = chuv.e() - lutry.e();
            double AP_y = chuv.n() - lutry.n();

            double projx = - 1047786572243912700.0 / 155397769689793.0 + lutry.e();
            double projy = 407848759189593550.0 / 776988848448965.0 + lutry.n();
            var expected = new PointCh(projx, projy);
            var actual = edge.positionClosestTo(chuv);
            boolean print = true;

            if (print) {
                System.out.println("Vector AB, lutry (1) to inf (2) | AB = (" + AB_x + "," + AB_y + ")");
                System.out.println("Vector AP: lutry (1) to chuv (3) | AB = (" + AP_x + "," + AP_y + ")");
                System.out.println();
                System.out.println("Projection manuelle: " + projx + " " + projy);
                System.out.println("Point associe: " + new PointCh(projx, projy));
                System.out.println("Longueur projection manuelle: " + Math2.norm(projx - lutry.e(), projy - lutry.n()));
                System.out.println("Longueur projection Edge: " + edge.positionClosestTo(chuv));
                System.out.println("Longueur projection Math2: " + Math2.projectionLength(lutry.e(), lutry.n(), inf.e(), inf.n(), chuv.e(), chuv.n()));
                System.out.println();
                System.out.println("Point at actual position: " + edge.pointAt(actual));
                System.out.println("Expected point:           " + expected);
                //System.out.println(edge.elevationAt(4)); // Expected: 9.0
            }

        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Disabled
    @Test
    void ofReturnsCorrectEdge() {
        try {
            Graph graphLsn = Graph.loadFrom(basePath);

            //Edge expected = new Edge(0, 4, );
            Edge actual = Edge.of(graphLsn, 2, 0, 4);
            boolean print = true;

            Edge expected = new Edge(0, 4, graphLsn.nodePoint(0), graphLsn.nodePoint(4), graphLsn.edgeLength(2), graphLsn.edgeProfile(2));

            // Data printing
            if (print) {
                System.out.println(graphLsn.nodePoint(2));
                System.out.println("expected: " + expected);
                System.out.println("actual:   " + actual);
            }

            // Le test ne passe pas pourtant, c'est exactement la même chose ?
            assertEquals(expected, actual);
            //assertSame(expected, actual);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    void pointAtWorksOnGivenData() {
        try {
            DoubleUnaryOperator profile = Functions.constant(9);
            Graph graphLsn = Graph.loadFrom(basePath);

            PointCh inf = new PointCh(2532864.1, 1152298.5);
            PointCh lutry = new PointCh(2542202.6, 1150383.9);
            PointCh chuv = new PointCh(2538922.3, 1152937.6);
            Edge edge = new Edge(0, 900000, inf, lutry, 10000, profile);

            assertEquals(9, edge.elevationAt(i()));

            // Pour actual, la valeur vient du test sur position closest to.
            var expected = chuv;
            var actual = edge.pointAt(3726);
            boolean print = true;

            if (print) {
                System.out.println(edge);
                System.out.println("expected: " + expected);
                System.out.println("actual:   " + actual);
            }

            //assertEquals(expected, actual);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    void elevationAtWorksOnGivenData() {

    }

}