package ch.epfl.seb;

import ch.epfl.javelo.routing.ElevationProfile;
import org.junit.jupiter.api.Test;

import static ch.epfl.test.TestRandomizer.RANDOM_ITERATIONS;
import static ch.epfl.test.TestRandomizer.newRandom;
import static org.junit.jupiter.api.Assertions.*;

class ElevationProfileTest {

    final double DELTA = 1e-5;

    @Test
    void minWorksOnGivenData() {

        float[] f1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        ElevationProfile ele1 = new ElevationProfile(40, f1);
        var expected1 = 1;
        var actual1 = ele1.minElevation();
        assertEquals(expected1, actual1);

        float[] f2 = {80, 95, (float) -9.5, (float) 3.789, 4, 5, 6, 78, (float) -0.00002, 1};
        ElevationProfile ele2 = new ElevationProfile(40, f2);
        var expected2 = (float) -9.5;
        var actual2 = ele2.minElevation();
        assertEquals(expected2, actual2);

        float[] f3 = {(float) 0b111111111111, 0, 3, (float) 5.5555, (float) Math.PI*100000, 6098 };
        ElevationProfile ele3 = new ElevationProfile(40, f3);
        var expected3 = 0;
        var actual3 = ele3.minElevation();
        assertEquals(expected3, actual3);

        float[] f4 = {(float) -666.8761, (float) -821.82434, (float) 149.81348, (float) 308.79626};
        ElevationProfile ele4 = new ElevationProfile(40, f4);
        var expected4 = (float) -821.82434;
        var actual4 = ele4.minElevation();
        assertEquals(expected4, actual4);

        float[] f5 = {(float) 123456789/987654321, (float) 123456789/987654321+1, (float) 123456789/987654321+2};
        ElevationProfile ele5 = new ElevationProfile(40, f5);
        float expected5 = (float) 123456789/987654321;
        var actual5 = ele5.minElevation();
        assertEquals(expected5, actual5);

        float[] f6 = {(float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5};
        ElevationProfile ele6 = new ElevationProfile(40, f6);
        var expected6 = (float) -0.5;
        var actual6 = ele6.minElevation();
        assertEquals(expected6, actual6);
    }

    @Test
    void maxWorksOnGivenData() {

        float[] f1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        ElevationProfile ele1 = new ElevationProfile(40, f1);
        var expected1 = 10;
        var actual1 = ele1.maxElevation();
        assertEquals(expected1, actual1);

        float[] f2 = {80, 95, (float) -9.5, (float) 3.789, 4, 5, 6, 78, (float) -0.00002, 1};
        ElevationProfile ele2 = new ElevationProfile(40, f2);
        var expected2 = (float) 95;
        var actual2 = ele2.maxElevation();
        assertEquals(expected2, actual2);

        float[] f3 = {(float) 0b111111111111, 0, 3, (float) 5.5555, (float) Math.PI*100000, 6098 };
        ElevationProfile ele3 = new ElevationProfile(40, f3);
        var expected3 = (float) Math.PI*100000;
        var actual3 = ele3.maxElevation();
        assertEquals(expected3, actual3);

        float[] f4 = {(float) -666.8761, (float) -821.82434, (float) 149.81348, (float) 308.79626};
        ElevationProfile ele4 = new ElevationProfile(40, f4);
        var expected4 = (float) 308.79626;
        var actual4 = ele4.maxElevation();
        assertEquals(expected4, actual4);

        float[] f5 = {(float) 123456789/987654321, (float) 123456789/987654321+1, (float) 123456789/987654321+2};
        ElevationProfile ele5 = new ElevationProfile(40, f5);
        float expected5 = (float) 123456789/987654321+2;
        var actual5 = ele5.maxElevation();
        assertEquals(expected5, actual5);

        float[] f6 = {(float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5};
        ElevationProfile ele6 = new ElevationProfile(40, f6);
        var expected6 = (float) 0.5;
        var actual6 = ele6.maxElevation();
        assertEquals(expected6, actual6);
    }

    @Test
    void minMaxWorksAsExpectedRandomized() {
        float[] sortThis = new float[10];
        ElevationProfile ele = new ElevationProfile(40, sortThis);

        var rng = newRandom();
        for (int i = 0; i < sortThis.length; ++i) {
            sortThis[i] = rng.nextFloat(-1000, 1000);
            System.out.println(sortThis[i]);
        }
        System.out.println("min: " + ele.minElevation());
        System.out.println("max: " + ele.maxElevation());
    }

    @Test
    void ascentWorksOnGivenData() {

        // /!\ Regarder l'augmentation à partir du premier point !

        float[] f1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        ElevationProfile ele1 = new ElevationProfile(40, f1);
        var expected1 = 9;
        var actual1 = ele1.totalAscent();
        assertEquals(expected1, actual1);

        float[] f2 = {80, 95, (float) -9.5, (float) 3.789, 4, 5, 6, 78, (float) -0.00002, 1};
        ElevationProfile ele2 = new ElevationProfile(40, f2);
        var expected2 = 15 + 13.289 + 0.211 + 1 + 1 + 72 + 1.00002;
        var actual2 = ele2.totalAscent();
        assertEquals(expected2, actual2, DELTA);

        // First element is 4095
        float[] f3 = {(float) 0b111111111111, 0, 3, (float) 5.5555, (float) Math.PI*100000, 6098};
        ElevationProfile ele3 = new ElevationProfile(40, f3);
        var expected3 = 3 + 2.5555 + (Math.PI*100000);
        var actual3 = ele3.totalAscent();
        //assertEquals(expected3, actual3);

        float[] f4 = {(float) -666.8761, (float) -821.82434, (float) 149.81348, (float) 308.79626};
        ElevationProfile ele4 = new ElevationProfile(40, f4);
        var expected4 = 971.63782 + 158.98278;
        var actual4 = ele4.totalAscent();
        assertEquals(expected4, actual4, 1e4);

        float[] f5 = {(float) 123456789/987654321, (float) 123456789/987654321+1, (float) 123456789/987654321+2};
        ElevationProfile ele5 = new ElevationProfile(40, f5);
        float expected5 = 2;
        var actual5 = ele5.totalAscent();
        assertEquals(expected5, actual5);

        float[] f6 = {(float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5};
        ElevationProfile ele6 = new ElevationProfile(40, f6);
        var expected6 = (float) 4;
        var actual6 = ele6.totalAscent();
        assertEquals(expected6, actual6);
    }

    @Test
    void descentWorksOnGivenData() {

        // /!\ Regarder la diminution à partir du premier point !

        float[] f1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        ElevationProfile ele1 = new ElevationProfile(40, f1);
        var expected1 = 0;
        var actual1 = ele1.totalDescent();
        assertEquals(expected1, actual1);

        float[] f2 = {80, 95, (float) -9.5, (float) 3.789, 4, 5, 6, 78, (float) -0.00002, 1};
        ElevationProfile ele2 = new ElevationProfile(40, f2);
        var expected2 = 182.50002;
        var actual2 = ele2.totalDescent();
        assertEquals(expected2, actual2, DELTA);

        // First element is 4095
        float[] f3 = {(float) 0b111111111111, 0, 3, (float) 5.5555, (float) Math.PI*100000, 6098};
        ElevationProfile ele3 = new ElevationProfile(40, f3);
        var expected3 = 3 + 2.5555 + (Math.PI*100000);
        var actual3 = ele3.totalDescent();
        //assertEquals(expected3, actual3);

        float[] f4 = {(float) -666.8761, (float) -821.82434, (float) 149.81348, (float) 308.79626};
        ElevationProfile ele4 = new ElevationProfile(40, f4);
        var expected4 = 154.94824;
        var actual4 = ele4.totalDescent();
        assertEquals(expected4, actual4, 1e4);

        float[] f5 = {(float) 123456789/987654321, (float) 123456789/987654321+1, (float) 123456789/987654321+2};
        ElevationProfile ele5 = new ElevationProfile(40, f5);
        float expected5 = 0;
        var actual5 = ele5.totalDescent();
        assertEquals(expected5, actual5);

        float[] f6 = {(float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5, (float) 0.5, (float) -0.5};
        ElevationProfile ele6 = new ElevationProfile(40, f6);
        var expected6 = (float) 5;
        var actual6 = ele6.totalDescent();
        assertEquals(expected6, actual6);
    }
}