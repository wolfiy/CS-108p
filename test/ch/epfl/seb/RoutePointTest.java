package ch.epfl.seb;

import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.routing.RoutePoint;
import ch.epfl.my.TestUtile;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;

import static ch.epfl.test.TestRandomizer.RANDOM_ITERATIONS;
import static ch.epfl.test.TestRandomizer.newRandom;
import static org.junit.jupiter.api.Assertions.*;

class RoutePointTest {

    Path basePath = Path.of(TestUtile.LAUSANNE_PATH);

    PointCh Bern = new PointCh(2600000, 1200000);
    PointCh Basel = new PointCh(2608395, 1267465);
    PointCh inf = new PointCh(2532864.1, 1152298.5);

    RoutePoint fromInf = new RoutePoint(inf, 12.3, 50);
    RoutePoint fromBern = new RoutePoint(Bern, 567, 98);
    RoutePoint fromBasel = new RoutePoint(Basel, 789, 100);

    @Test
    void testOnLausanneTemplate() {
        try {
            Graph graphLsn = Graph.loadFrom(basePath);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    /*@Test
    void testOnLausanneTemplateRandomized() {
        try {
            Graph graphLsn = Graph.loadFrom(basePath);
            var rng = newRandom();

            for (int i = 0; i < RANDOM_ITERATIONS; ++i) {
                var e = rng.nextDouble(2485000, 2834000);
                var n = rng.nextDouble(1075000, 1296000);

                var expected;
                var actual;
                boolean print = false;

                // Data printing
                if (print) {

                }

                assertEquals(expected, actual);
            }
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }*/

    @Test
    void withPositionShiftedByWorks() {
        try {
            Graph graphLsn = Graph.loadFrom(basePath);
            // https://map.geo.admin.ch/?lang=fr&topic=ech&bgLayer=ch.swisstopo.pixelkarte-farbe&E=2542203&N=1150384&zoom=11.604726226433158&crosshair=marker&layers=KML%7C%7Chttps:%2F%2Fcs108.epfl.ch%2Fp%2Ff%2Fbbox-lausanne.kml
            PointCh lutry = new PointCh(2542202.6, 1150383.9);
            RoutePoint rte = new RoutePoint(lutry, 10, 20);
            var expected = new RoutePoint(lutry, 15, 20);
            var actual = rte.withPositionShiftedBy(5);
            System.out.println(rte);
            System.out.println(actual);
            assertEquals(expected, actual);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    void withPositionShiftedByWorksRandomized() {
        try {
            Graph graphLsn = Graph.loadFrom(basePath);
            var rng = newRandom();

            for (int i = 0; i < RANDOM_ITERATIONS; ++i) {
                var e = rng.nextDouble(2485000, 2834000);
                var n = rng.nextDouble(1075000, 1296000);
                var pos = rng.nextDouble(0, 500);
                var shift = rng.nextDouble(-567, 567);

                PointCh point = new PointCh(e, n);
                RoutePoint rte = new RoutePoint(point, pos, 200);

                var expected = new RoutePoint(point, pos+shift, 200);
                var actual = rte.withPositionShiftedBy(shift);
                boolean print = false;

                // Data printing
                if (print) {
                    System.out.println("base:     " + rte);
                    System.out.println("expected: " + expected);
                    System.out.println("actual:   " + actual);
                    System.out.println("position: " + pos);
                    System.out.println("shift:    " + shift);
                }

                assertEquals(expected, actual);
            }
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    void minWorksInLausanne() {
        try {
            Graph graphLsn = Graph.loadFrom(basePath);
            // https://map.geo.admin.ch/?lang=fr&topic=ech&bgLayer=ch.swisstopo.pixelkarte-farbe&E=2542203&N=1150384&zoom=11.604726226433158&crosshair=marker&layers=KML%7C%7Chttps:%2F%2Fcs108.epfl.ch%2Fp%2Ff%2Fbbox-lausanne.kml
            PointCh lutry = new PointCh(2542202.6, 1150383.9);
            // https://map.geo.admin.ch/?lang=fr&topic=ech&bgLayer=ch.swisstopo.pixelkarte-farbe&E=2541004.83&N=1150624.33&zoom=11.776392893099823&layers=KML%7C%7Chttps:%2F%2Fcs108.epfl.ch%2Fp%2Ff%2Fbbox-lausanne.kml
            PointCh paudex = new PointCh(2540968.9, 1150597.9);

            RoutePoint fromLutry = new RoutePoint(lutry, 50, 69);
            RoutePoint fromPaudex = new RoutePoint(paudex, 70, 40);

            var actual = fromLutry.min(fromPaudex);
            var expected = fromPaudex;
            boolean print = true;

            if (print) {
                System.out.println("actual:     " + actual);
                System.out.println("fromLutry:  " + fromLutry);
                System.out.println("fromPaudex: " + fromPaudex);
            }

            assertEquals(expected, actual);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    void minWorksRandomized() {
        try {
            Graph graphLsn = Graph.loadFrom(basePath);
            var rng = newRandom();

            for (int i = 0; i < RANDOM_ITERATIONS; ++i) {
                var e = rng.nextDouble(2485000, 2834000);
                var n = rng.nextDouble(1075000, 1296000);
                var pos = rng.nextDouble(0, 1000);
                var dist = rng.nextDouble(-500, 500);
                var dist2 = rng.nextDouble(-70, 900);

                PointCh point = new PointCh(e, n);
                PointCh lutry = new PointCh(2542202.6, 1150383.9);
                RoutePoint rte = new RoutePoint(point, pos, dist);
                RoutePoint fromLutry = new RoutePoint(lutry, pos-dist, dist2);

                var actual = rte.min(fromLutry);
                RoutePoint expected;
                if (dist < dist2) expected = rte;
                else expected = fromLutry;
                boolean print = false;

                // Data printing
                if (print) {
                    System.out.println("rte:      " + rte);
                    System.out.println("lutry:    " + fromLutry);
                    System.out.println("actual:   " + actual);
                    System.out.println("expected: " + expected);
                }

                assertEquals(expected, actual);
            }
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    void minWorksWithSmallDataRandomized() {
        try {
            Graph graphLsn = Graph.loadFrom(basePath);
            var rng = newRandom();

            for (int i = 0; i < RANDOM_ITERATIONS; ++i) {
                var e = rng.nextDouble(2485000, 2834000);
                var n = rng.nextDouble(1075000, 1296000);
                var pos = rng.nextDouble(0, 1000);
                var dist = rng.nextDouble(-0.0005, 0.0009);
                var dist2 = rng.nextDouble(-0.0000000006, 0.1);

                PointCh point = new PointCh(e, n);
                PointCh lutry = new PointCh(2542202.6, 1150383.9);
                RoutePoint rte = new RoutePoint(point, pos, dist);
                RoutePoint fromLutry = new RoutePoint(lutry, pos-dist, dist2);

                var actual = rte.min(fromLutry);
                RoutePoint expected;
                if (dist < dist2) expected = rte;
                else expected = fromLutry;
                boolean print = false;

                // Data printing
                if (print) {
                    System.out.println("rte:      " + rte);
                    System.out.println("lutry:    " + fromLutry);
                    System.out.println("actual:   " + actual);
                    System.out.println("expected: " + expected);
                    System.out.println("pos:      " + pos);
                    System.out.println("dist:     " + dist);
                    System.out.println("dist2:    " + dist2);
                }

                assertEquals(expected, actual);
            }
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    void minWorksWithHugeDataRandomized() {
        try {
            Graph graphLsn = Graph.loadFrom(basePath);
            var rng = newRandom();

            for (int i = 0; i < RANDOM_ITERATIONS; ++i) {
                var e = rng.nextDouble(2485000, 2834000);
                var n = rng.nextDouble(1075000, 1296000);
                var pos = rng.nextDouble(0, 1000);
                var dist = rng.nextDouble(-5000000, 5000000);
                var dist2 = rng.nextDouble(-7000000, 9000000);

                PointCh point = new PointCh(e, n);
                PointCh lutry = new PointCh(2542202.6, 1150383.9);
                RoutePoint rte = new RoutePoint(point, pos, dist);
                RoutePoint fromLutry = new RoutePoint(lutry, pos-dist, dist2);

                var actual = rte.min(fromLutry);
                RoutePoint expected;
                if (dist < dist2) expected = rte;
                else expected = fromLutry;
                boolean print = true;

                // Data printing
                if (print) {
                    System.out.println("rte:      " + rte);
                    System.out.println("lutry:    " + fromLutry);
                    System.out.println("actual:   " + actual);
                    System.out.println("expected: " + expected);
                    System.out.println("pos:      " + pos);
                    System.out.println("dist:     " + dist);
                    System.out.println("dist2:    " + dist2);
                }

                assertEquals(expected, actual);
            }
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    void min2WorksRandomized() {
        try {
            Graph graphLsn = Graph.loadFrom(basePath);
            var rng = newRandom();

            for (int i = 0; i < RANDOM_ITERATIONS; ++i) {
                var e = rng.nextDouble(2485000, 2834000);
                var n = rng.nextDouble(1075000, 1296000);
                var e2 = rng.nextDouble(2485000, 2834000);
                var n2 = rng.nextDouble(1075000, 1296000);
                var pos = rng.nextDouble(0, 10000);
                var dist = rng.nextDouble(-50000, 50000);
                var pos2 = rng.nextDouble(0, 10000);
                var dist2 = rng.nextDouble(-50000, 50000);

                PointCh point = new PointCh(e, n);
                PointCh point2 = new PointCh(e2, n2);

                RoutePoint rte = new RoutePoint(point, pos, dist);
                RoutePoint rte2 = new RoutePoint(point2, pos2, dist2);

                var actual = rte.min(rte2);
                RoutePoint expected;
                if (dist < dist2) expected = rte;
                else expected = rte2;
                boolean print = false;

                // Data printing
                if (print) {

                }

                assertEquals(expected, actual);
            }
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    void noneConstructorWorks() {
        var actual = RoutePoint.NONE;
        var expected = new RoutePoint(null, Double.NaN, Double.POSITIVE_INFINITY);
        assertEquals(actual, expected);
    }

    @Test
    void minWorksOnGivenData() {
        var actual = fromInf.min(fromBern);
        var actual2 = fromInf.min(Basel, 10, 1000000);
        var actual3 = fromInf.min(Basel, 10, 0);
        System.out.println(actual);
        System.out.println(actual2);
        System.out.println(actual3);

        var actual4 = fromBern.min(fromBern);
        var a5 = fromBern.min(fromInf);
        System.out.println(actual4);
        System.out.println(a5);

        var a6 = fromBern.min(fromBasel);
        var a7 = fromBasel.min(fromBern);
        var a8 = fromBasel.min(fromInf);
        System.out.println(a6);
        System.out.println(a7);
        System.out.println(a8);

        // TRY TO WORK
        // Inf has the smallest distanceToReference, it should always be inf
        // Did I test the right thing?
        var ac1 = fromInf.min(fromBasel);
        var ac2 = fromInf.min(fromBern);
        var ac3 = fromInf.min(fromInf);

        var exInf = fromInf;

        assertEquals(ac1, exInf);
        assertEquals(ac2, exInf);
        assertEquals(ac3, exInf);

        System.out.println(ac1);
        System.out.println(ac2);
        System.out.println(ac3);
    }

}