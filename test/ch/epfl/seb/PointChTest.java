package ch.epfl.seb;

import ch.epfl.javelo.projection.PointCh;
import org.junit.jupiter.api.Test;
import ch.epfl.test.TestRandomizer;

import static ch.epfl.test.TestRandomizer.RANDOM_ITERATIONS;
import static ch.epfl.test.TestRandomizer.newRandom;
import static org.junit.jupiter.api.Assertions.*;

class PointChTest {

    @Test
    void pointChCantConstructWithWrongInput() {
        // E too small
        assertThrows(IllegalArgumentException.class, () -> {
            new PointCh(newRandom().nextInt(Integer.MIN_VALUE, 2485000-1), newRandom().nextInt(2485000, 2834000));
        });

        // N too small
        assertThrows(IllegalArgumentException.class, () -> {
            new PointCh(newRandom().nextInt(2485000, 2834000), newRandom().nextInt(Integer.MIN_VALUE, 1075000-1));
        });

        // E too big
        assertThrows(IllegalArgumentException.class, () -> {
            new PointCh(newRandom().nextInt(2834001, Integer.MAX_VALUE), newRandom().nextInt(2485000, 2834000));
        });

        // N too big
        assertThrows(IllegalArgumentException.class, () -> {
            new PointCh(newRandom().nextInt(2485000, 2834000), newRandom().nextInt(1296001, Integer.MAX_VALUE));
        });
    }

    @Test
    void pointChCanConstructWithValidInput() {
        for (int i = 0; i < 1000; i ++) {
            var e = newRandom().nextDouble(2485000, 2834000);
            var n = newRandom().nextDouble(1075000, 1296000);
            new PointCh(e, n);
        }
    }
}