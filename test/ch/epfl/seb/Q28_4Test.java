package ch.epfl.seb;

import ch.epfl.javelo.Q28_4;
import org.junit.jupiter.api.Test;
import ch.epfl.test.TestRandomizer;

import static ch.epfl.test.TestRandomizer.RANDOM_ITERATIONS;
import static ch.epfl.test.TestRandomizer.newRandom;
import static org.junit.jupiter.api.Assertions.*;

class Q28_4Test {

    @Test
    void ofIntWorksOnKnownValues() {
        var input = 0b10011100;
        var output = 0b100111000000;
        var expected = 2496;
        var actual = Q28_4.ofInt(input);
        assertEquals(output, actual);
        assertEquals(expected, actual);
    }

    @Test
    void asDoubleWorksOnKnownValue() {
        // Antoine Tran, Piazza @96
        // "Une valeur entière de q devrait être interprétée en décimal comme l'entier
        // qu'il représente divisé par 16."
        var input1 = 16;
        var expected1 = 16/16;
        var actual1 = Q28_4.asDouble(input1);

        var input2 = 4567;
        var expected2 = (double)input2/16;
        var actual2 = Q28_4.asDouble(input2);

        var input3 = newRandom().nextInt();
        var expected3 = (double)input3/16;
        var actual3 = Q28_4.asDouble(input3);

        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
        // Works but need fine tuned delta
        // assertEquals(expected3, actual3);
    }

    @Test
    void asFloatWorksOnKnownValues() {
        var input1 = 16;
        var expected1 = 16/16;
        var actual1 = Q28_4.asFloat(input1);

        var input2 = 4567;
        var expected2 = (float)input2/16;
        var actual2 = Q28_4.asFloat(input2);
    }
}