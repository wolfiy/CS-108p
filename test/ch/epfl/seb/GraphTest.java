package ch.epfl.seb;

import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.projection.Ch1903;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.my.TestUtile;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.LongBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.util.function.DoubleUnaryOperator;

import static org.junit.jupiter.api.Assertions.*;

class GraphTest {

    private int LAUSANNE_E = 2_533_312;
    private int LAUSANNE_N = 1_152_173;


    //point particulier
    private final static int DISTANCE_MAX_CHERCHE = 20;
    private final static double LAT = Math.toRadians(46.51695);
    private final static double LON = Math.toRadians(6.56602);

    //point 2 rond point
    private final static double LAT2 = Math.toRadians(46.51730);
    private final static double LON2 = Math.toRadians(6.56976);

    @RepeatedTest(30)
    void TestGeneralAvecAffichage() {
        Path basePath = Path.of(TestUtile.LAUSANNE_PATH);
        try {
            Graph graph = Graph.loadFrom(basePath);

            // Lausanne : E=2_520_000 N=1_145_000 à E=2_550_000 N=1_165_000
            PointCh random = new PointCh(TestUtile.r(2_500_000, 2_550_000), TestUtile.r(1_145_000,1_165_000));
            PointCh voulu = new PointCh(2_500_000, 1_145_503);

            //pour choisir une arrete suffit de changer le random en voulu ligne de dessous.
            PointCh point = random;
            int noeud = graph.nodeClosestTo(point, 60);
            if(noeud!=-1) {

                System.out.println("\nposition initiale (" + point.e() + ";" + point.n() + ")");

                PointCh pointProche = graph.nodePoint(noeud);
                System.out.println(noeud + " point proche (" + pointProche.e() + ";" + pointProche.n() + ")");
                System.out.println("length " + graph.edgeLength(noeud));
                System.out.println("edgeProfil " + graph.edgeProfile(noeud) );
                System.out.println("nodeCount nombre de noeud (change pas) " + graph.nodeCount());
                int nombreAreteSortante = graph.nodeOutDegree(noeud);
                System.out.println("node out degree, nombre arête sortante " + nombreAreteSortante);
                for(int i=0;i<nombreAreteSortante;i++){
                    System.out.println(i + " " + graph.nodeOutEdgeId(noeud, i));
                    System.out.println("next arrete " + graph.edgeTargetNodeId(noeud));
                }
                System.out.println("arete inverse " + (graph.edgeIsInverted(noeud) ? "oui": "non"));
                System.out.println("attributeSet " + graph.edgeAttributes(noeud));
                System.out.println("dénivelé " + graph.edgeElevationGain(noeud));
                System.out.println("OSM: " + getOSM(noeud));
            }else{
                System.out.println("\npas de noeud");
            }



        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }


    @Disabled
    @Test
    void altitude() {
        double expected = 397.3;
        Path basePath = Path.of(TestUtile.LAUSANNE_PATH);
        try {
            Graph graph = Graph.loadFrom(basePath);

            PointCh point = new PointCh(LAUSANNE_E, LAUSANNE_N);
            int node = graph.nodeClosestTo(point, 5);

            if (node == -1) {
                fail();
            } else {
                System.out.println(graph.nodePoint(node));
                DoubleUnaryOperator fonction = graph.edgeProfile(node);
                for(int i=-10;i<500;i++){
                    System.out.println(i/10.0 + " " + fonction.applyAsDouble(i/10.0));
                }
                assertEquals(expected, fonction.applyAsDouble(0));
            }


        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }


    @Test
    void pointOpenstreetMap1(){
        Graph lausanne = lausanne();


        double e  = Ch1903.e(LON, LAT);
        double n = Ch1903.n(LON,LAT);
        System.out.println(e);
        System.out.println(n);
        PointCh point = new PointCh(e,n) ;
        int noeud = lausanne.nodeClosestTo(point, DISTANCE_MAX_CHERCHE);

        assertEquals(1952301583, getOSM(noeud));
        assertEquals(Math.toRadians(6.56609), lausanne.nodePoint(noeud).lon(), 1e-5);
        assertEquals(Math.toRadians(46.51699), lausanne.nodePoint(noeud).lat(), 1e-5);

        int edge1 = lausanne.nodeOutEdgeId(noeud,1);

        int b = lausanne.edgeTargetNodeId(edge1);

        assertEquals(2, lausanne.nodeOutDegree(2));

        assertEquals(2, lausanne.nodeOutDegree(noeud));

        assertEquals(7057012465L, getOSM(b));

        assertEquals(2, lausanne.nodeOutDegree(b));
    }

    //fonctionne
    @Test
    void pointOSM2(){
        Graph graph = lausanne();
        double lon = Math.toRadians(6.6013034);
        double lat = Math.toRadians(46.6326106);
        PointCh point = new PointCh(Ch1903.e(lon, lat), Ch1903.n(lon, lat));
        int n = graph.nodeClosestTo(point, 6);

        assertEquals(310876657, getOSM(n));

    }

    @Disabled
    @Test
    public void afficherOSM(){
        Path filePath = Path.of("lausanne/nodes_osmid.bin");
        LongBuffer osmIdBuffer = null;
        try (FileChannel channel = FileChannel.open(filePath)) {
            osmIdBuffer = channel
                    .map(FileChannel.MapMode.READ_ONLY, 0, channel.size())
                    .asLongBuffer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(" a " + osmIdBuffer.get(155671));
        System.out.println(" b " + osmIdBuffer.get(155664));
        System.out.println(" c " + osmIdBuffer.get(155662));
    }

    public long getOSM(int numeroJavelo){
        Path filePath = Path.of("lausanne/nodes_osmid.bin");
        LongBuffer osmIdBuffer = null;
        try (FileChannel channel = FileChannel.open(filePath)) {
            osmIdBuffer = channel
                    .map(FileChannel.MapMode.READ_ONLY, 0, channel.size())
                    .asLongBuffer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return osmIdBuffer.get(numeroJavelo);
    }

    public static Graph lausanne(){
        Graph graph=null;
        Path basePath = Path.of("lausanne");
        try {
            graph = Graph.loadFrom(basePath);
        }catch (IOException e) {
            e.printStackTrace();
            fail();
        }

        return graph;
    }

    @Test
    void test() throws IOException {
        Path filePath = Path.of("lausanne/nodes_osmid.bin");
        LongBuffer osmIdBuffer;
        try (FileChannel channel = FileChannel.open(filePath)) {
            osmIdBuffer = channel
                    .map(FileChannel.MapMode.READ_ONLY, 0, channel.size())
                    .asLongBuffer();
        }
        System.out.println(osmIdBuffer.get(194878));
    }
}