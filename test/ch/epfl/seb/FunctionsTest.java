package ch.epfl.seb;

import ch.epfl.javelo.Functions;
import org.junit.jupiter.api.Test;
import ch.epfl.test.TestRandomizer;

import static ch.epfl.test.TestRandomizer.RANDOM_ITERATIONS;
import static ch.epfl.test.TestRandomizer.newRandom;
import java.util.function.DoubleUnaryOperator;

import static org.junit.jupiter.api.Assertions.*;

class FunctionsTest {

    @Test
    void constantReturnsAConstant() {
        var cst = Functions.constant(10.0);
        var expected = 10.0;
        var actual = cst.applyAsDouble(newRandom().nextDouble());
        assertEquals(expected, actual);
    }

    @Test
    void constantWorksOnRandomValue() {
        for (int i = 0; i < 1000; i++) {
            double rng = newRandom().nextDouble(-1000, 1000);
            var cst = Functions.constant(rng);
            var expected = rng;
            var actual = cst.applyAsDouble(newRandom().nextDouble(-2500, 2500));
            assertEquals(expected, actual);
        }
    }

    @Test
    void sampledWorksWithTwoPoints() {
        // Over two steps, 1.5 is like 0.75 if it were over 1.
        float[] samples = {0,1};
        var n = 1.5;
        var sampled = Functions.sampled(samples, 2);
        var result = sampled.applyAsDouble(n);
        assertEquals(0.75, result);
        System.out.println(result);
    }

    @Test
    void sampledWorksWithGivenData() {
        // iPad example-ish. xMax = 10. operand = x = n. samples contient
        // Matches with Roi Myw
        float[] samples = {0, 2, 4, 6, 8};
        var n = 3.5;
        var sampled = Functions.sampled(samples, 10);
        var result = sampled.applyAsDouble(3.5);
        assertEquals(2.8, result);
        System.out.println(result);
    }
}