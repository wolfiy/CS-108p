package ch.epfl.seb;

import ch.epfl.javelo.Math2;
import org.junit.jupiter.api.Test;
import ch.epfl.test.TestRandomizer;

import static ch.epfl.test.TestRandomizer.RANDOM_ITERATIONS;
import static ch.epfl.test.TestRandomizer.newRandom;
import static org.junit.jupiter.api.Assertions.*;

class Math2Test {

    @Test
    void ceilDivXCantBeNegative() {
        assertThrows(IllegalArgumentException.class, () -> {
            Math2.ceilDiv(-1,1);
        });
    }

    @Test
    void ceilDivYCantBeZero() {
        assertThrows(IllegalArgumentException.class, () -> {
            Math2.ceilDiv(1, 0);
        });
    }

    @Test
    void ceilDivWorksOnMin() {
        assertEquals(0, Math2.ceilDiv(0, 1));
    }

    @Test
    void interpolateWorksOnLoop() {
        for (int i = 0; i < 1000; i++) {
            double y0 = newRandom().nextDouble();
            double y1 = newRandom().nextDouble();
            assertEquals(y0, Math2.interpolate(y0,y1, i), 1e-10);
        }
    }

    @Test
    void clampNotWorkingIfMinBiggerMax() {
        assertThrows(IllegalArgumentException.class, () -> {
            for (int i = 0; i < 100; i++) {
                int a = newRandom().nextInt();
                int b = a + Math.abs(newRandom().nextInt());
                Math2.clamp(b, 10, a);
            }
        });
    }
}