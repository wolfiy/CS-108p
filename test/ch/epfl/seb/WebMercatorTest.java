package ch.epfl.seb;

import ch.epfl.javelo.projection.WebMercator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WebMercatorTest {

    final static double DELTA = 1e-10;
    final static double DELTA_2 = 1e-7;
    // Given in class.
    final double xNapoleon = 0.518275214444;
    final double yNapoleon = 0.353664894749;
    final double lonNapoleon = 6.5790772;
    final double latNapoleon = 46.5218976;

    // Data from OSM.
    final double xVisnitzbahn = 7.97846395;
    final double yVisnitzbahn = 0.31965459;
    // Computed with Mathematica; radians.
    final double lonVisnitzbahn = 46.9885748;
    final double latVisnitzbahn = 10.3725070;

    @Test
    void xWorksOnKnownValue() {
        assertEquals(xNapoleon, WebMercator.x(Math.toRadians(lonNapoleon)), DELTA);
    }

    @Test
    void xNotWorkingOnWrongData() {
        assertNotEquals((xNapoleon+0.001), WebMercator.x(Math.toRadians(lonNapoleon)), DELTA);
    }

    @Test
    void yWorksOnKnownValue() {
        assertEquals(yNapoleon, WebMercator.y(Math.toRadians(latNapoleon)), DELTA);
    }

    @Test
    void yNotWorkingOnWrongData() {
        assertNotEquals((yNapoleon-000000.1), WebMercator.y(Math.toRadians(latNapoleon)), DELTA);
    }

    @Test
    void lonWorksOnKnownValue() {
        assertEquals(Math.toRadians(lonNapoleon), WebMercator.lon(xNapoleon), DELTA);
        assertEquals(lonVisnitzbahn, WebMercator.lon(xVisnitzbahn), DELTA_2);
    }

}