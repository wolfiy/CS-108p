package ch.epfl.seb;

import ch.epfl.javelo.gui.MapViewParameters;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MapViewParametersTest {

    final double xNap19 = 69561722;
    final double yNap19 = 47468099;

    @Test
    void mapViewParameterWorks() {
        MapViewParameters mvp = new MapViewParameters(19, 0, 0);

        var expectedx19 = xNap19;
        var expectedy19 = yNap19;
    }
}