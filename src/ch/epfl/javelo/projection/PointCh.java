package ch.epfl.javelo.projection;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.Preconditions;

/**
 * L'enregistrement représente un point dans le système de coordonnées suisse.
 *
 *                          E [2_485_000 ; 2_834_000]
 *                          N [1_075_000 ; 1_296_000]
 *
 * @author Antoine Cornaz (339526)
 */

public record PointCh (double e, double n) {

    /**
     * Vérifie que le point est en Suisse.
     *
     * @param e coordonnée est du point. Appartient à [2_485_000 ; 2_834_000].
     * @param n coordonnée nord du point. Appartient à [1_075_000 ; 1_296_000].
     * @throws IllegalArgumentException si le point n'est pas contenu dans le territoire suisse.
     */
    public PointCh {
        Preconditions.checkArgument(SwissBounds.containsEN(e,n));
    }

    /**
     * Calcul la distance au carré.
     *
     * @param that seconde coordonnée.
     * @return le carré de la distance en mètres séparant le récepteur (this) de l'argument that.
     */
    public double squaredDistanceTo(PointCh that){
        return Math2.squaredNorm(that.e - this.e, that.n - this.n);
    }

    /**
     * Donne la distance en mètres entre 2 points.
     *
     * @param that seconde coordonnée.
     * @return la distance en mètres entre 2 points.
     */
    public double distanceTo(PointCh that){
        return Math2.norm(that.e - this.e, that.n - this.n);
    }

    /**
     * Donne la longitude du point.
     *
     * @return la longitude.
     */
    public double lon(){
        return Ch1903.lon(this.e, this.n);
    }

    /**
     * Donne la latitude du point.
     *
     * @return la latitude.
     */
    public double lat(){
        return Ch1903.lat(this.e, this.n);
    }
}
