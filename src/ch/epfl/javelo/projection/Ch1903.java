package ch.epfl.javelo.projection;

/**
 * Conversion des coordonnées entre WSG 84 et le système suisse CH1903.
 *
 * @author Sébastien Tille (313220)
 */
public final class Ch1903 {

    private Ch1903() {}

    /**
     * Calcule la coordonnée est du point dans le système suisse.
     *
     * @param lon longitude du point en radians.
     * @param lat latitude du point en radians.
     * @return la coordonnée E du point dans le système CH1903.
     */
    public static double e(double lon, double lat) {
        double l = 1e-4 * (3_600*Math.toDegrees(lon) - 26_782.5);
        double p = 1e-4 * (3_600*Math.toDegrees(lat) - 169_028.66);

        return (2_600_072.37 + 211_455.93*l)
                 - (10_938.51*l*p) - (0.36*l*p*p)
                 - (44.54*l*l*l);
    }

    /**
     * Calcule la coordonnée nord du point.
     *
     * @param lon la longitude du point en radiants.
     * @param lat la latitude du point en radiants.
     * @return la coordonnée N du point dans le système CH1903.
     */
    public static double n(double lon, double lat) {
        double l = 1e-4 * (3_600*Math.toDegrees(lon) - 26_782.5);
        double p = 1e-4 * (3_600*Math.toDegrees(lat) - 169_028.66);

        return (1_200_147.07 + 308_807.95*p)
                 + (3_745.25*l*l) + (76.63*p*p)
                 - (194.56*l*l*p) + (119.79*p*p*p);
    }

    /**
     * Calcule la longitude du point.
     *
     * @param e la coordonnée CH1903 est (E).
     * @param n la coordonnée CH1903 nord (N).
     * @return la longitude du point dans le système WGS84.
     */
    public static double lon(double e, double n) {
        double x = 1e-6 * (e-2_600_000);
        double y = 1e-6 * (n-1_200_000);

        double l = (2.6779094 + 4.728982*x)
                 + (0.791484*x*y) + (0.1306*x*y*y)
                 - (0.0436*x*x*x); // degré

        double lon = l * (100./36.);
        return Math.toRadians(lon);
    }

    /**
     * Calcule la latitude du point.
     *
     * @param e la coordonnée CH1903 est (E).
     * @param n la coordonnée CH1903 nord (N).
     * @return la latitude du point dans le système WGS84.
     */
    public static double lat(double e, double n) {
        double x = 1e-6 * (e-2_600_000);
        double y = 1e-6 * (n-1_200_000);

        double p = (16.9023892 + 3.238272*y)
                 - (0.270978*x*x) - (0.002528*y*y)
                 - (0.0447*x*x*y) - (0.0140*y*y*y);

        double lat = p * (100./36.);
        return Math.toRadians(lat);
    }
}
