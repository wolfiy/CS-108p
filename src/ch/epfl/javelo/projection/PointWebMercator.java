package ch.epfl.javelo.projection;

import ch.epfl.javelo.Preconditions;

/**
 * Représente un point dans le système Web Mercator.
 *
 * @author Antoine Cornaz (339526)
 */
public record PointWebMercator(double x, double y) {

    private final static int IMAGE_FACTOR = 8;

    /**
     * Constructeur d'un point dans le système de coordonnées Web Mercator.
     *
     * @param x coordonnée "de gauche à droite". Argument de [0,1].
     * @param y coordonnée "de haut en bas". Argument de [0,1].
     * @throws IllegalArgumentException si x et y ne sont pas compris entre 0 et 1 inclus.
     */
    public PointWebMercator{
        Preconditions.checkArgument(x >= 0);
        Preconditions.checkArgument(x <= 1);
        Preconditions.checkArgument(y >= 0);
        Preconditions.checkArgument(y <= 1);
    }

    /**
     * Crée un objet PointWebMercator à partir des coordonées Web Mercator et du niveau de zoom.
     *
     * @param zoomLevel indique le niveau de zoom entre 0 et 19 (20 suivant les cas).
     * @param x coordonnée x. Argument de [0,1].
     * @param y coordonnée y. Argument de [0,1].
     * @return un point dans le système Web Mercator.
     */
    public static PointWebMercator of(int zoomLevel, double x, double y){
        double newX = dezoom(zoomLevel, x);
        double newY = dezoom(zoomLevel, y);
        return new PointWebMercator(newX, newY);
    }

    /**
     * Convertit un point du système suisse au système Web Mercator.
     *
     * @param pointCh point dans le système suisse.
     * @return un point dans le référentiel WebMercator.
     */
    public static PointWebMercator ofPointCh(PointCh pointCh){
        double newX = WebMercator.x(pointCh.lon());
        double newY = WebMercator.y(pointCh.lat());
        return new PointWebMercator(newX, newY);
    }

    /**
     * Permet d'obtenir la valeur de x dans un zoom donnée.
     *
     * @param zoomLevel niveau d'agrandissement. Argument de [0,19] (20 suivant les cas).
     * @return la valeur de x, par rapport à un certain level de zoom
     */
    public double xAtZoomLevel(int zoomLevel){
        return zoom(zoomLevel, this.x);
    }

    /**
     * Permet d'obtenir la valeur de x dans un zoom donnée.
     *
     * @param zoomLevel niveau d'agrandissement. Argument de [0,19] (20 suivant les cas).
     * @param r postion initiale de x ou y.
     * @return la valeur de x, par rapport à un certain level de zoom.
     */
    private static double zoom(int zoomLevel, double r){
        return Math.scalb(r, IMAGE_FACTOR+zoomLevel);
    }

    /**
     * Permet d'obtenir la valeur de y dans un niveau de zoom donné.
     *
     * @param zoomLevel niveau d'agrandissement. Argument de [0,19] (20 suivant les cas).
     * @return la valeur de x, par rapport à un certain level de zoom.
     */
    public double yAtZoomLevel(int zoomLevel){
        return zoom(zoomLevel, y);
    }

    /**
     * Permet d'obtenir la valeur de x ou y dans un niveau de zoom donné.
     *
     * @param zoomLevel niveau d'agrandissement. Argument de [0,19] (20 suivant les cas).
     * @param r postion initiale de x ou y.
     * @return la valeur de x ou y, par rapport à un certain level de zoom.
     */
    private static double dezoom(int zoomLevel, double r){
        return Math.scalb(r, -(IMAGE_FACTOR+zoomLevel));
    }

    /**
     * Permet d'obtenir la longitude d'un point.
     *
     * @return la longitude en radians
     */
    public double lon(){
        return WebMercator.lon(x);
    }

    /**
     * Permet d'obtenir la latitude d'un point.
     *
     * @return la latitude en radians
     */
    public double lat(){
        return WebMercator.lat(y);
    }

    /**
     * Transforme un point Web Mercator au format suisse.
     *
     * @return un point au format suisse.
     */
    public PointCh toPointCh(){

        // x,y -> lon, lat PointWebMercator.
        double lon = lon();
        double lat = lat();

        // lon lat -> e, n Ch1903.
        double e = Ch1903.e(lon,lat);
        double n = Ch1903.n(lon,lat);

        // Vérifie que le point est en Suisse.
        if (!SwissBounds.containsEN(e,n)) return null;

        //e,n -> PointCh PointCh
        return new PointCh(e, n);
    }
}
