package ch.epfl.javelo.projection;

/**
 * Bordures de la Suisse
 *
 * @author Sébastien Tille (313220)
 */
public final class SwissBounds {
    public static final double MIN_E = 2_485_000;
    public static final double MAX_E = 2_834_000;
    public static final double MIN_N = 1_075_000;
    public static final double MAX_N = 1_296_000;
    public static final double WIDTH = MAX_E - MIN_E;
    public static final double HEIGHT = MAX_N - MIN_N;

    private SwissBounds() {}

    /**
     * Vérifie si les coordonnées entrées sont belles-et-bien dans le territoire Suisse. Ces
     * coordonnées sont au format Ch1903.
     *
     * @param e coordonnée est.
     * @param n coordonnée nord.
     * @return true si le point appartient au territoire Suisse, false sinon.
     */
    public static boolean containsEN(double e, double n) {
        return e >= MIN_E && e <= MAX_E && n >= MIN_N && n <= MAX_N;
    }
}
