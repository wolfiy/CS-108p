package ch.epfl.javelo.projection;

import ch.epfl.javelo.Math2;

/**
 * Convertit les coordonnées WGS-84 et Web Mercator.
 *
 * @author Antoine Cornaz (339526)
 */
public final class WebMercator {
    private final static double OVER_TWO_PI = 1.0/2.0/Math.PI;
    private WebMercator(){}

    /**
     * Permet d'obtenir la coordonnée 'y' dans le système Web Mercator à partir de la longitude
     * WSG-84.
     *
     * @param lon longitude du point en radians.
     * @return la valeur de x dans le système Web Mercator.
     */
    public static double x(double lon){
        return OVER_TWO_PI *(lon+Math.PI);
    }

    /**
     * Permet d'obtenir la coordonnée 'x' dans le système Web Mercator à partir de la longitude
     * WSG-84.
     *
     * @param lat latitude du point en radians.
     * @return la valeur de y dans le système Web Mercator.
     */
    public static double y(double lat){
        double asinArg = Math.tan(lat);
        return OVER_TWO_PI * (Math.PI - Math2.asinh(asinArg));
    }

    /**
     * Permet d'obtenir la longitude dans le système WSG-84 à partir de la coordonnée Web Mercator
     * 'x'.
     *
     * @param x coordonnée 'x' dans Web Mercator.
     * @return longitude en radians.
     */
    public static double lon(double x){
        return 2 * Math.PI * x - Math.PI;
    }

    /**
     * Permet d'obtenir la longitude dans le système WSG-84 à partir de la coordonnée Web Mercator
     * 'y'.
     *
     * @param y coordonnée 'y' dans Web Mercator.
     * @return longitude en radians.
     */
    public static double lat(double y){
        double asinArg = Math.PI - 2 * Math.PI * y;
        return Math.atan(Math.sinh(asinArg));
    }
}
