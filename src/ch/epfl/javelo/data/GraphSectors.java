package ch.epfl.javelo.data;

import ch.epfl.javelo.Bits;
import ch.epfl.javelo.Math2;
import ch.epfl.javelo.Preconditions;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.SwissBounds;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant les différents secteurs de la carte de la Suisse. Elle contient
 * un tableau contenant les 16'384 secteurs nécessaires pour couvrir l'ensemble du
 * territoire.
 *
 * @author Antoine Cornaz (339526)
 */

public record GraphSectors(ByteBuffer buffer) {
    // id premier noeud 32 u32 unsigned
    // nombre de noeuds 16 u16 unsigned
    // ________________________________  ________________ est un secteur
    //          id du noeud                nombre noeuds
    // https://piazza.com/class/kzifjghz6po4se?cid=281

    private final static int BYTE = 8;

    private final static int OFFSET_ID = 0 / BYTE;
    private final static int ID_SIZE = Integer.BYTES;

    private final static int OFFSET_NODE = OFFSET_ID + ID_SIZE;
    private final static int NODE_SIZE = Short.BYTES;

    // Nombre d'octets d'un élément du buffer
    private final static int GRAPH_BUFFER_SIZE = ID_SIZE + NODE_SIZE;

    // Largeur, hauteur
    private static Sector[][] sectors;

    // Nombre de secteurs
    private final static int SECTORS_HEIGHT = 128;
    private final static int SECTORS_WIDTH = 128;

    public GraphSectors {

        int numberOfSectors = buffer.capacity() / GRAPH_BUFFER_SIZE;
        sectors = new Sector[SECTORS_WIDTH][SECTORS_HEIGHT];

        for (int i = 0; i < numberOfSectors;i++){

            //Récupère l'id du premier nœud. Pas besoin verifier si positif.
            int firstNodeId = buffer.getInt(OFFSET_ID + i* GRAPH_BUFFER_SIZE);
            //Récupère la quantité de nœuds. Besoin de l'interpréter comme non-signé.
            int numberOfNodes = Bits.extractUnsigned(buffer
                                    .getShort(OFFSET_NODE + i * GRAPH_BUFFER_SIZE),
                              0,16);
            int lastNodeId = firstNodeId + numberOfNodes;

            int width = i % SECTORS_WIDTH;
            int height = i / SECTORS_WIDTH; // Arrondi en dessous.
            sectors[width][height] = new Sector(firstNodeId, lastNodeId);
        }
    }

    /**
     * Permet d'obtenir les secteurs ayant une intersection avec le carré centré en 'center', de
     * côté égal au double de 'distance'.
     *
     * @param center Point au format suisse.
     * @param distance en mètres.
     * @throws IllegalArgumentException si le centre est nul ou si la distance est négative.
     * @return qui retourne la liste de tous les secteurs ayant une intersection avec le carré
     * centré au point donné et de côté égal au double (!) de la distance donnée.
     */
    public List<Sector> sectorsInArea(PointCh center, double distance){
        // Retourne la liste de tous les secteurs ayant une intersection avec
        // le carré centré au point donné et de côté égal au double
        // https://piazza.com/class/kzifjghz6po4se?cid=285

        Preconditions.checkArgument(center != null);
        Preconditions.checkArgument(distance >= 0);

        // Notes :
        // La distance est en mètres;
        // Le secteur en bas à gauche a les coordonnées (x,y) = (0,0);
        // Le secteur en haut à droite (127, 127);
        // Le secteur en bas à droite (127, 0);
        // car on commence à 0, et la Suisse est recouverte d'un pavage de 128x128.

        double e = center.e();
        double n = center.n();

        int minSectorHeight = heightSectorIndex(n - distance);
        int maxSectorHeight = heightSectorIndex(n + distance);
        int minSectorWidth = widthSectorIndex(e - distance);
        int maxSectorWidth = widthSectorIndex(e + distance);

        List<Sector> sectorsList = new ArrayList<>();

        // i la hauteur et j la largeur.
        for (int i = minSectorHeight; i <= maxSectorHeight; i++) {
            for (int j = minSectorWidth; j <= maxSectorWidth; j++) {
                sectorsList.add(sectors[j][i]);
            }
        }

        return sectorsList;
    }

    /**
     * Prend la valeur est et retourne la largeur du secteur.
     *
     * @param e valeur de l'est au format Suisse.
     * @return l'index du secteur, selon le nord.
     */
    private static int widthSectorIndex(double e){

        // Exemple: affecte le point 2_485_000 au secteur numéro 0.
        // Map de 2_485_000, 2_834_000 → 0, 127.

        e -= SwissBounds.MIN_E;
        e /= (SwissBounds.WIDTH / SECTORS_WIDTH);

        // Arrondi en dessous.
        int eInteger = (int) e;

        eInteger = Math2.clamp(0, eInteger, SECTORS_WIDTH - 1);

        return eInteger;
    }

    /**
     * Prend la valeur du sud et retourne la hauteur du secteur.
     *
     * @param n valeur du nord au format Suisse.
     * @return l'index du secteur selon l'est.
     */
    private static int heightSectorIndex(double n){

        // Exemple: affecte le point 1_075_000 au secteur numéro 0.
        // Map de 1_075_000, 1_296_000 → 0, 127.
        n -= SwissBounds.MIN_N;
        n /= ((SwissBounds.HEIGHT)/(SECTORS_HEIGHT));

        // Arrondi en dessous.
        int nInteger = (int) n;

        nInteger = Math2.clamp(0, nInteger, SECTORS_HEIGHT - 1);

        return nInteger;
    }

    /**
     * Représente un secteur.
     *
     * @param startNodeId l'identité (index) du premier nœud du secteur.
     * @param endNodeId l'identité (index) du nœud situé juste après le dernier nœud.
     */
    public record Sector(int startNodeId, int endNodeId){}
}
