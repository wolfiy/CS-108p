package ch.epfl.javelo.data;


import ch.epfl.javelo.Preconditions;

import java.util.StringJoiner;

/**
 * Représente l'ensemble des attributs d'un noeud.
 *
 * @author Antoine cornaz (339526)
 */
public record AttributeSet(long bits) {

    private final static int FLAG_SIZE = Long.SIZE;

    /**
     * Créé un drapeau qui stocke les attributs.
     *
     * @param bits drapeau contenant quel Attribute il y a.
     * @throws IllegalArgumentException si le 32e bit est 1.
     */
    public AttributeSet{
        long copyBits = bits;
        int count = Attribute.COUNT;
        copyBits = copyBits >>> count;
        // Vérifie qu'il n'y ait pas de 1 après le 31e bit (le 'count').
        Preconditions.checkArgument(copyBits == 0);
    }

    /**
     * Crée un drapeau qui stocke les attributs.
     *
     * @param attributes à mettre dans le drapeau.
     * @return le drapeau contenant les attributs.
     */
    public static AttributeSet of(Attribute... attributes){
        long flag = 0;
        for (Attribute attribute : attributes){
            // Crée le 0000100000 à la bonne place.
            long mask = flag(attribute);
            // Ajoute un 0010000 (drapeau) à la bonne case.
            flag = flag | mask;
        }
        return new AttributeSet(flag);
    }

    /**
     * Vérifie si l'attribueSet contient un certain attribut.
     *
     * @param attribute attribut à vérifier.
     * @return vrai si l'attribut est contenu, faux sinon.
     */
    public boolean contains(Attribute attribute){
        long mask = flag(attribute);
        return (bits & mask) != 0;
    }


    /**
     * Compare s'il y a un attribut en commun entre les 2 sets d'attributs.
     *
     * @param that autre attribut à comparer.
     * @return vrai si un attribut est en commun, faux sinon.
     */
    public boolean intersects(AttributeSet that){
        return (this.bits & that.bits) != 0;
    }

    /**
     * Affiche les attributs du set d'attribut
     *
     * @return un texte contenant l'ensemble des attributs.
     */
    @Override
    public String toString(){
        StringJoiner j = new StringJoiner(",", "{", "}");

        for(int i = 0; i < Attribute.COUNT; i++){
            Attribute attribute = Attribute.ALL.get(i);
            if (contains(attribute)){
                j.add(attribute.toString());
            }
        }
        return j.toString();
    }

    /**
     * Transforme un attribut en le masque correspondant.
     *
     * @param attribute l'attribut à transformer.
     * @throws IllegalArgumentException si l'attribut est null.
     * @return le masque de l'attribut.
     */
    private static long flag(Attribute attribute){
        Preconditions.checkArgument(attribute!=null);
        // Regarde quel flag correspond
        int bitWeight = attribute.ordinal();
        // Crée le drapeau décalé de "bitWeight" cases.
        return flag(bitWeight);
    }

    /**
     * Crée un masque, un long de type 0000100. Ex: flag(2) → 00000100.
     *
     * @param shift décalage depuis la droite vers la gauche. Valeur entre 0 et 63.
     * @throws IllegalArgumentException si le décalage est inférieur à 0 ou
     *                                  si le décalage est plus grand que 64 bits.
     * @return un drapeau contenant 1 seul bit allumé.
     */
    private static long flag(int shift){
        Preconditions.checkArgument(shift >= 0);
        Preconditions.checkArgument(shift < FLAG_SIZE);
        return 1L << shift;
    }
}
