package ch.epfl.javelo.data;

import ch.epfl.javelo.Bits;
import ch.epfl.javelo.Math2;
import ch.epfl.javelo.Q28_4;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;


/**
 * Enregistrement représentant le tableau de toutes les arêtes du graphe de JaVelo.
 * Une arête est représentée par 10 octets.
 *
 * @author Antoine Cornaz (339526)
 * @author Sébastien Tille (313220)
 */
public record GraphEdges(ByteBuffer edgesBuffer, IntBuffer profileIds, ShortBuffer elevations) {

    // edgeBuffer
    private final static int OFFSET_EDGE = 0;
    private final static int OFFSET_NODEID = 0;
    private final static int OFFSET_LENGTH = OFFSET_NODEID + 4;
    private final static int OFFSET_ELEVATION = OFFSET_LENGTH + 2;
    private final static int OFFSET_OSM = OFFSET_ELEVATION + 2;
    private final static int OFFSET_TOTAL_SIZE = OFFSET_OSM + 2;

    // profileId
    private final static int OFFSET_TYPE = 0;
    private final static int OFFSET_PROFILE_ID = 0;
    private final static int OFFSET_PROFILE_TOTAL = 4;
    private final static int OFFSET_PROFILE_TOTAL_INT = OFFSET_PROFILE_TOTAL/4;

    private final static int OFFSET_PROFILE = 30;
    private final static int PROFILE_LENGTH = 2;

    private final static int SIZE_COMPRESSED_PROFILE_0_4 = 4;
    private final static int SIZE_COMPRESSED_PROFILE_4_4 = 8;

    /**
     * Vérifie si l'arête d'identité 'edgeId' va dans le sens inversé de la voie
     * OpenStreetMap dont elle provient. Si le premier bit est 0, alors l'arête est positive
     * (même sens). S'il est 1, elle est négative (sens inversé).
     * @param edgeId l'arête d'identité.
     *
     * @return true si l'arête donnée va dans le sens inverse de la voie OSM.
     */
    public boolean isInverted(int edgeId) {
        return (edgesBuffer.getInt(OFFSET_TOTAL_SIZE*edgeId + OFFSET_EDGE) < 0);
    }

    /**
     * Permet d'obtenir l'identité du noeud destination de l'arête donnée. Si celle-ci est inversée,
     * on retourne le complément de la totalité des bits de la valeur de 32 bits.
     *
     * @param edgeId une arête.
     * @return l'identité du noeud destination de l'arête donnée.
     */
    public int targetNodeId(int edgeId) {
        int targetNode = edgesBuffer.getInt(OFFSET_TOTAL_SIZE*edgeId + OFFSET_EDGE);
        if (!isInverted(edgeId)) return targetNode;
        return ~targetNode;
    }

    /**
     * Permet d'obtenir la longueur de l'arête donnée en mètre.
     *
     * @param edgeId une arête identité.
     * @return la longueur en mètres.
     */
    public double length(int edgeId) {
        return Q28_4.asDouble(Short.toUnsignedInt(edgesBuffer.getShort(
                OFFSET_TOTAL_SIZE*edgeId + OFFSET_LENGTH)));
    }

    /**
     * Permet d'obtenir le dénivelé
     *
     * @param edgeId une arête identité.
     * @return le dénivelé positif sur cette arête, en mètres.
     */
    public double elevationGain(int edgeId) {
        return Q28_4.asDouble(Short.toUnsignedInt(edgesBuffer
                .getShort(OFFSET_TOTAL_SIZE*edgeId + OFFSET_ELEVATION)));
    }

    /**
     * Indique si une arête utilise un profil. S'il n'y a pas de profil, celui ci
     * "vaut" 00.
     *
     * @param edgeId une arête identité.
     * @return true si un profil est utilisé, faux sinon.
     */
    public boolean hasProfile(int edgeId) {
        int currentEdge = profileIds.get(OFFSET_PROFILE_TOTAL_INT*edgeId + OFFSET_TYPE);
        int usedProfile = currentEdge >>> OFFSET_PROFILE;
        return usedProfile != 0;
    }

    /**
     * Permet de créer un tableau contenant les échantillons du profil de l'arête donnée.
     * Celui-ci est vide si le profil est inexistant (00).
     *
     * @param edgeId une arête identité.
     * @return un tableau contenant les échantillons.
     */
    public float[] profileSamples(int edgeId) {

        // 1) Regarder le type de profil de l'edge (hint : 0 indique qu'elle a pas de profil)
        int useThisInt = profileIds.get(OFFSET_PROFILE_TOTAL_INT*edgeId + OFFSET_TYPE);
        int usedProfile = Bits.extractSigned(useThisInt, OFFSET_PROFILE, PROFILE_LENGTH);
        // Retirer les 1 au debut du poids fort à cause du décalage arithmétique
        usedProfile &= 0b11;

        // 2) Regarder l'index du premier sample (qui est contenu dans les 30bits restants
        // du même int que le type de profil)
        int sampleProfileId = Bits.extractUnsigned(useThisInt, 0, OFFSET_PROFILE);

        // Distance en mètres.
        short distance = edgesBuffer.getShort(OFFSET_TOTAL_SIZE * edgeId + OFFSET_LENGTH);

        // Nombre d'arrêtes.
        int sampleCount = 1 + Math2.ceilDiv(Short.toUnsignedInt(distance), Q28_4.ofInt(2));

        /*
         * 0 : rien
         * 1 : non compressé
         * 2 : compressé au format Q4.4
         * 3 : compressé au format Q0.4
         *
         * Nombre d'échantillons: 1 + ceil(l/2)
         *
         * A noter que:
         * 1er échantillon: UQ12.4 (non compressé)
         * Suivant: Q4.4 ou Q0.4
         */
        float[] samples;
        samples = profileType(usedProfile, sampleProfileId, sampleCount);

        if (isInverted(edgeId) && usedProfile!=0) {
            float[] tmp = new float[sampleCount];
            for (int i = 0; i < sampleCount; ++i) {
                float a = samples[i];
                tmp[sampleCount-i-1]=a;
            }
            samples = tmp;
        }

        return samples;
    }

    private float[] profileType(int usedProfile, int sampleProfileId, int sampleCount) {
        float[] samples;
        switch (usedProfile) {
            case 0 -> samples = new float[0];
            case 1 -> samples = uncompressedProfile(sampleProfileId, sampleCount);
            case 2 -> samples = profileCompressedQ4_4(sampleProfileId, sampleCount);
            case 3 -> samples = profileCompressedQ0_4(sampleProfileId, sampleCount);
            default -> {
                samples = new float[0];
                assert false;
            }
        }
        return samples;
    }

    private float[] profileCompressedQ0_4(int sampleProfileId, int sampleCount) {
        float[] samples;
        assert sampleCount >= 1;

        samples = new float[sampleCount];
        // Le premier élément n'est pas compressé; on fait comme pour le profil 1.
        float unsigned = Q28_4.asFloat(elevations.get(sampleProfileId));
        if(unsigned < 0){
            unsigned += 4095;
        }
        samples[0] = unsigned;

        // Pour le reste, on ne stocke que la différence de hauteur au format Q.4.4.
        for (int i = 1; 4*i < sampleCount +3; ++i) {

            short elevationShort = elevations.get(sampleProfileId + i);
            float signedBit = Q28_4.asFloat(Bits.extractSigned(Short.toUnsignedInt(elevationShort),
                                                         12, SIZE_COMPRESSED_PROFILE_0_4));

            samples[4*i - 3] = signedBit + samples[4*i-4]; // Les 8 bits de poids forts
            signedBit = Q28_4.asFloat(Bits.extractSigned(Short.toUnsignedInt(elevationShort),
                                                          8, SIZE_COMPRESSED_PROFILE_0_4));

            if (4*i - 2 < sampleCount){
                // Ajoute si elle existe, la deuxième partie du buffer.
                samples[4*i - 2] = signedBit + samples[4*i-3];
            }

            signedBit = Q28_4.asFloat(Bits.extractSigned(Short.toUnsignedInt(elevationShort), 4, SIZE_COMPRESSED_PROFILE_0_4));

            if (4*i -1 < sampleCount){
                // Ajoute, si elle existe, la troisième partie du buffer.
                samples[4*i - 1] = signedBit + samples[4*i-2];
            }

            signedBit = Q28_4.asFloat(Bits.extractSigned(Short.toUnsignedInt(elevationShort), 0, SIZE_COMPRESSED_PROFILE_0_4));

            if (SIZE_COMPRESSED_PROFILE_0_4 *i < sampleCount){
                // Ajoute si elle existe, la quatrième partie du buffer.
                samples[SIZE_COMPRESSED_PROFILE_0_4 *i] = signedBit + samples[4*i-1];
            }
        }
        return samples;
    }

    private float[] profileCompressedQ4_4(int sampleProfileId, int sampleCount) {
        float[] samples;
        // Stocker les differences d'altitude, sauf pour le premier.
        // On stocke deux différences d'élévation par élément du tableau ; on a besoin d'un
        // tableau moitié-moins grand.

        samples = new float[sampleCount];
        // Le premier élément n'est pas compressé ; on fait comme pour le profil 1.
        short hauteur = elevations.get(sampleProfileId);

        samples[0] = Q28_4.asFloat(Short.toUnsignedInt(hauteur));

        // Pour le reste, on ne stocke que la différence de hauteur au format Q.4.4.
        for (int i = 1; 2*i < sampleCount + 1; ++i) {
            short compressed = elevations.get(sampleProfileId +i);

            int signedBitNormal = Bits.extractSigned(Short.toUnsignedInt(compressed),8,
                    SIZE_COMPRESSED_PROFILE_4_4);
            float signedQ28 = Q28_4.asFloat(signedBitNormal);

            samples[2*i - 1] = samples[2*i-2] + signedQ28;

            signedBitNormal = Bits.extractSigned(Short.toUnsignedInt(compressed), 0, SIZE_COMPRESSED_PROFILE_4_4);
            signedQ28 =  Q28_4.asFloat(signedBitNormal); // Les 8 MSB.

            if (2*i < sampleCount) {
                // Ajoute, si elle existe, la deuxième partie du buffer.
                samples[2*i] = samples[2*i-1] + signedQ28;
            }
        }
        return samples;
    }

    private float[] uncompressedProfile(int sampleProfileId, int sampleCount) {
        float[] samples;
        samples = new float[sampleCount];

        for (int i = 0; i < sampleCount; ++i) {
            int a = Short.toUnsignedInt(elevations.get(sampleProfileId +i));
            float elevation = Q28_4.asFloat(a);
            samples[i] = elevation;
        }

        return samples;
    }

    /**
     * Permet de donner l'index de l'identité de l'ensemble d'attributs attachés à
     * l'arête donnée.
     *
     * @param edgeId une arête identité.
     * @return l'identité de l'ensemble d'attributs attachés à l'arête.
     */
    public int attributesIndex(int edgeId) {
        short toConvert = edgesBuffer.getShort((OFFSET_TOTAL_SIZE * edgeId + OFFSET_OSM));
        return Short.toUnsignedInt(toConvert);
    }
}
