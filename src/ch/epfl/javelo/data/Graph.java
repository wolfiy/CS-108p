package ch.epfl.javelo.data;

import ch.epfl.javelo.Functions;
import ch.epfl.javelo.projection.PointCh;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.DoubleUnaryOperator;

/**
 * Représente le graphe JaVelo.
 * Classe utile à la représentation de graph.
 * Cette classe n'affiche pas de graphique.
 *
 * @author Antoine Cornaz (339526)
 */
public final class Graph {

    private final static String NODES_PATH = "nodes.bin";
    private final static String SECTORS_PATH = "sectors.bin";
    private final static String EDGES_PATH = "edges.bin";
    private final static String PROFILE_IDS_PATH = "profile_ids.bin";
    private final static String ELEVATIONS_PATH = "elevations.bin";
    private final static String ATTRIBUTES_PATH = "attributes.bin";

    private final GraphNodes nodes;
    private final GraphSectors sectors;
    private final GraphEdges edges;
    private final List<AttributeSet> attributeSetList;

    /**
     * Constructeur
     * Mappage de tous les fichiers d'un répertoire ou autre.
     * Permet d'obtenir des informations sur des arêtes ou des noeuds ou des edges.
     *
     * @param nodes Ensemble des noeuds d'une zone. (Possiblement plusieurs secteurs)
     * @param sectors Ensemble des secteurs avec les id des arêtes associé. (Possiblement
     *                plusieurs secteurs)
     * @param edges Ensemble des arêtes d'une zone. (Possiblement plusieurs secteurs)
     * @param attributeSetList Liste des attributs OSM.
     */
    public Graph(GraphNodes nodes, GraphSectors sectors, GraphEdges edges,
                 List<AttributeSet> attributeSetList){
        this.nodes = nodes;
        this.sectors = sectors;
        this.edges = edges;
        this.attributeSetList = new ArrayList<>(attributeSetList);
    }


    /**
     * Charge un graphe depuis un répertoire.
     *
     * @param basePath dossier contenant les données OpenStreetMap.
     * @throws IOException Si le dossier ne contient pas attributes.bin, edges.bin, ... ou sectors.bin.
     *                     Tout les "PATHS" au-dessus.
     * @return un graph contenant les arêtes, noeuds, secteurs et attribut de la suisse.
     */
    public static Graph loadFrom(Path basePath)throws IOException{

        // Donne le chemin
        Path nodesPath = basePath.resolve(NODES_PATH);
        Path sectorPath = basePath.resolve(SECTORS_PATH);
        Path edgePath = basePath.resolve(EDGES_PATH);
        Path profilePath = basePath.resolve(PROFILE_IDS_PATH);
        Path elevationPath = basePath.resolve(ELEVATIONS_PATH);
        Path attributesPath = basePath.resolve(ATTRIBUTES_PATH);

        // Crée le buffer
        IntBuffer nodeBuffer;
        ByteBuffer sectorBuffer;
        ByteBuffer edgeBuffer;
        IntBuffer profileBuffer;
        ShortBuffer elevationBuffer;
        LongBuffer attributeBuffer;

        // "try" permettant de fermer les buffers même s'il y a une exception.
        try (FileChannel nodeChannel = FileChannel.open(nodesPath);
             FileChannel sectorChanel = FileChannel.open(sectorPath);
             FileChannel edgeChannel = FileChannel.open(edgePath);
             FileChannel profileChannel = FileChannel.open(profilePath);
             FileChannel elevationChannel = FileChannel.open(elevationPath);
             FileChannel attributeChannel = FileChannel.open(attributesPath)
        ) {
            // Mappe les buffers.
            nodeBuffer = nodeChannel.map(FileChannel.MapMode.READ_ONLY, 0,
                                         nodeChannel.size()).asIntBuffer();
            sectorBuffer = sectorChanel.map(FileChannel.MapMode.READ_ONLY, 0,
                                            sectorChanel.size());
            edgeBuffer = edgeChannel.map(FileChannel.MapMode.READ_ONLY, 0,
                                         edgeChannel.size());
            profileBuffer = profileChannel.map(FileChannel.MapMode.READ_ONLY, 0,
                                               profileChannel.size()).asIntBuffer();
            elevationBuffer = elevationChannel.map(FileChannel.MapMode.READ_ONLY, 0,
                                                   elevationChannel.size()).asShortBuffer();
            attributeBuffer = attributeChannel.map(FileChannel.MapMode.READ_ONLY, 0,
                                                   attributeChannel.size()).asLongBuffer();
        }

        //Créer une liste de set vide et la remplir.
        List<AttributeSet> attributeSetList = new ArrayList<>();
        for (int i = 0; i < attributeBuffer.capacity(); i++){
            attributeSetList.add(new AttributeSet(attributeBuffer.get(i)));
        }

        // Créer les 3 Graphs.
        GraphNodes nodes = new GraphNodes(nodeBuffer);
        GraphSectors sectors = new GraphSectors(sectorBuffer);
        GraphEdges edges = new GraphEdges(edgeBuffer, profileBuffer, elevationBuffer);

        return new Graph(nodes, sectors, edges, Collections.unmodifiableList(attributeSetList));
    }

    /**
     * Compte les noeuds du graph.
     *
     * @return le nombre total de noeuds.
     */
    public int nodeCount(){
        return nodes.count();
    }

    /**
     * Permet d'obtenir la position d'une arête.
     *
     * @param nodeId noeuds dans le système suisse.
     * @return la position du noeud donné.
     */
    public PointCh nodePoint(int nodeId){
        double e = nodes.nodeE(nodeId);
        double n = nodes.nodeN(nodeId);
        return new PointCh(e,n);
    }

    /**
     * Permet d'obtenir le nombre d'arêtes sortantes.
     *
     * @param nodeId noeuds dans le système suisse.
     * @return Le nombre d'arêtes sortantes.
     */
    public int nodeOutDegree(int nodeId){
        return nodes.outDegree(nodeId);
    }

    /**
     * Permet d'obtenir la edgeIndex-ième arête sortant du noeud d'identité nodeId.
     *
     * @param nodeId noeuds dans le système Suisse.
     * @param edgeIndex Numéro de l'arête à sortir. Ex: la première.
     * @return L'identité de la edgeIndex-ième arête sortant du nœud d'identité nodeId
     */
    public int nodeOutEdgeId(int nodeId, int edgeIndex){
        return nodes.edgeId(nodeId, edgeIndex);
    }

    /**
     * Cherche le noeud le plus proche du point donné dans la limite de la distance.
     *
     * @param point Centre depuis lequel un rond de rayon "searchDistance" mètre.
     * @param searchDistance rayon de distance en mètre autour de point
     * @return Le noeud le plus proche dans la limite donnée. Retourne -1 si pas trouvé.
     */
    public int nodeClosestTo(PointCh point, double searchDistance){

        // La classe regarde toujours par rapport au carré de la distance.
        // x^2 < y^2 => x < y.  (x et y sont positifs ou neutre)

        List<GraphSectors.Sector> sectorList = sectors.sectorsInArea(point, searchDistance);

        // Regarde quelle arête est la plus proche du point. Maximum searchDistance
        // Le carré de la distance minimale est stocké.
        double squaredDistanceMin = Double.MAX_VALUE;
        int closestNodeId = -1;

        // Parcours tous les secteurs puis tous les nœuds des secteurs
        for (GraphSectors.Sector sector : sectorList) {

            int startNodeId = sector.startNodeId();
            int endNodeId = sector.endNodeId();

            for (int j = startNodeId; j < endNodeId; j++) {
                //regarde le carré de la distance du nœud j.
                double squaredDistance = point.squaredDistanceTo(nodePoint(j));

                //Si le noeud est le plus proche (des noeuds parcourus) et qu'il est plus proche
                //que la distance max, alors garder son id et sa distance.
                if (squaredDistance < squaredDistanceMin &&
                        squaredDistance <= searchDistance*searchDistance) {
                    squaredDistanceMin = squaredDistance;
                    closestNodeId = j;
                }
            }
        }

        return closestNodeId;
    }

    /**
     * Permet d'obtenir l'identité du noeud de destination de l'arête indiquée.
     *
     * @param edgeId noeuds dans le système Suisse.
     * @return l'identité du nœud destination de l'arête d'identité donnée
     */
    public int edgeTargetNodeId(int edgeId){
        return edges.targetNodeId(edgeId);
    }

    /**
     * Vérifie si l'arête d'identité 'edgeId' va dans le sens inversé de la voie
     * OpenStreetMap dont elle provient. Si le premier bit est 0, alors l'arête est positive
     * (même sens). S'il est 1, elle est négative (sens inversé).
     *
     * @param edgeId l'arête d'identité. Noeuds dans le système Suisse.
     * @return vrai si l'arête donnée va dans le sens inverse de la voie OSM.
     */
    public boolean edgeIsInverted(int edgeId){
        return edges.isInverted(edgeId);
    }

    /**
     * Permet d'obtenir le set d'attribut d'une arête.
     *
     * @param edgeId noeuds dans le système Suisse.
     * @return le set d'attribut
     */
    public AttributeSet edgeAttributes(int edgeId){
        int indexOSM = edges.attributesIndex(edgeId);
        return attributeSetList.get(indexOSM);
    }

    /**
     * Permet d'obtenir la taille d'une arête.
     *
     * @param edgeId noeuds dans le système Suisse.
     * @return la taille de l'arête.
     */
    public double edgeLength(int edgeId){
        return edges.length(edgeId);
    }

    /**
     * Permet d'obtenir le dénivelé
     *
     * @param edgeId noeuds dans le système Suisse.
     * @return le dénivelé positif sur cette arête, en mètres.
     */
    public double edgeElevationGain(int edgeId){
        return edges.elevationGain(edgeId);
    }

    /**
     * Calcul le profil en long de l'arête d'identité donnée, sous la forme d'une fonction ;
     * si l'arête ne possède pas de profil,
     * alors cette fonction retourne "Double.NaN" pour n'importe quel argument.
     *
     * @param edgeId noeuds dans le système Suisse.
     * @return Une fonction de la hauteur de l'arête ou une fonction retournant uniquement NaN.
     */
    public DoubleUnaryOperator edgeProfile(int edgeId){
        DoubleUnaryOperator function;
        if (edges.hasProfile(edgeId)){
            // Taille de l'arête.
            double length = edges.length(edgeId);
            // Hauteur en mètres de chaque morceau de l'arête.
            float[] profile = edges.profileSamples(edgeId);
            function = Functions.sampled(profile, length);
        } else {
            function = Functions.constant(Double.NaN);
        }

        return function;
    }
}
