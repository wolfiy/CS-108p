package ch.epfl.javelo.data;

import ch.epfl.javelo.Bits;
import ch.epfl.javelo.Q28_4;

import java.nio.IntBuffer;


/**
 * Classe qui permet de gérer certaines données sous forme de nœuds pour
 * ensuite permettre à une autre classe de les afficher.
 *
 * @author Antoine Cornaz (339526)
 * @author Sébastien Tille (313220)
 */

public record GraphNodes(IntBuffer buffer) {

    // Buffer 3x int : Est, Nord, nb. d'arêtes sortantes du nœud et l'index de la première.
    // 0: Est, 1: Nord, 2:nbr arrête et identité première
    // 3: Est du nœud id1, 4 Nord du nœud id1, id du noeud id1, ...
    private final static int OFFSET_E = 0; //EST
    private final static int OFFSET_N = OFFSET_E + 1; //NORD
    private final static int OFFSET_OUT_EDGES = OFFSET_N + 1; // nbr d'arête et identité premiere.
    private final static int NODE_INTS = OFFSET_OUT_EDGES + 1; //BUFFERTAILLEBOUCLE


    private final static int FIRST_EDGE_SIZE = 28;
    private final static int EDGE_INDEX = FIRST_EDGE_SIZE;
    private final static int EDGE_COUNT = 4;

    /**
     * Compte les noeuds du buffer.
     *
     * @return le nombre total de noeuds.
     */
    public int count(){
        // Devrait être un multiple de 3
        return (buffer.capacity())/NODE_INTS;
    }

    /**
     * Calcul la valeur de la coordonnée Est dans le système suisse.
     *
     * @param nodeId nœud dont on veut connaitre la coordonnée Est.
     * @return la coordonnée Est du nœud d'identité donnée.
     */
    public double nodeE(int nodeId){
        return Q28_4.asDouble(buffer.get(nodeId * NODE_INTS + OFFSET_E));
    }

    /**
     * Calcul la valeur de la coordonnée Nord dans le système suisse.
     *
     * @param nodeId nœud dont on veut connaitre la coordonnée Nord.
     * @return la coordonnée Nord du nœud d'identité donnée.
     */
    public double nodeN(int nodeId){
        return Q28_4.asDouble(buffer.get(nodeId * NODE_INTS + OFFSET_N));
    }

    /**
     * Retourne le nombre d'arêtes sortant du nœud identité.
     *
     * @param nodeId identité du nœud.
     * @return le nombre d'arêtes sortant du nœud d'identité donné.
     */
    public int outDegree(int nodeId){
        int byte3 = buffer.get(nodeId * NODE_INTS + OFFSET_OUT_EDGES);
        return Bits.extractUnsigned(byte3, EDGE_INDEX,EDGE_COUNT);
    }

    /**
     * Retourne l'identité de l'edgeIndex i-ème valeur.
     *
     * @param nodeId nœud de départ.
     * @param edgeIndex edgeIndex-i-ème arête.
     * @return l'identité de la i-ème arête.
     */
    public int edgeId(int nodeId, int edgeIndex){
        assert 0 <= edgeIndex;
        assert edgeIndex < outDegree(nodeId);
        int byteFixed = buffer.get(nodeId * NODE_INTS + OFFSET_OUT_EDGES);
        int idFixed = Bits.extractUnsigned(byteFixed, 0, FIRST_EDGE_SIZE);
        return idFixed + edgeIndex;
    }
}
