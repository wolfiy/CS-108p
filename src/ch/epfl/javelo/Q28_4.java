package ch.epfl.javelo;

/**
 * Classe permettant la conversion des nombres entre la représentation "Q28_4" et les autres.
 *
 * @author Sébastien Tille (313220)
 * @author Antoine Cornaz (339526)
 */
public final class Q28_4 {

    private final static int SHIFT = 4;
    private Q28_4() {}

    /**
     * Convertit un entier 'i' dans la représentation Q28.4.
     *
     * @param i l'entier à convertir.
     * @return l'entier dans la représentation Q28.4.
     */
    public static int ofInt(int i) {
        return i << SHIFT;
    }

    /**
     * Convertit un entier donné dans sa représentation Q28.4 en 'double'.
     *
     * @param q28_4 l'entier à convertir.
     * @return le double égal à la valeur donnée.
     */
    public static double asDouble(int q28_4) {
        return Math.scalb((double)q28_4, -SHIFT);
    }

    /**
     * Convertit en 'float' la valeur de l'entier en représentation Q28.4.
     *
     * @param q28_4 l'entier à convertir.
     * @return le float égal à la valeur donnée.
     */
    public static float asFloat(int q28_4) {
        return Math.scalb((float) q28_4, -SHIFT);
    }
}