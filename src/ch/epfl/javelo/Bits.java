package ch.epfl.javelo;

/**
 * Classe permettant d'extraire des séquences de bits à partir de vecteurs de 32 bits.
 *
 * @author Sébastien Tille (313220)
 */
public final class Bits {

    private Bits(){}

    private final static int VECTOR_SIZE = Integer.SIZE;

    /**
     * Extrait d'un vecteur de 32 bits la plage de bits commençant à la position 'start'
     * et de longueur 'length'. Celle-ci sera ici signée (décalage arithmétique).
     *
     * @param value un vecteur de 32 bits.
     * @param start l'index du premier bit de la plage à extraire.
     * @param length la longueur de la plage voulue.
     * @throws IllegalArgumentException si la plage comprend moins de 0 bits ou plus de 32.
     * @return la plage de bits signée en complément deux-à-deux.
     */
    public static int extractSigned(int value, int start, int length)
                                    throws IllegalArgumentException {

        Preconditions.checkArgument(length >= 0);
        Preconditions.checkArgument(start >= 0);
        Preconditions.checkArgument(length + start <= VECTOR_SIZE);

        // 1. Décalage à gauche (virer ce qu'il y a devant l'index start).
        int span = value << (VECTOR_SIZE - (start + length));
        // 2. Décalage arithmétique à droite (virer ce qu'il y a après la fin de la plage).
        span = span >> (VECTOR_SIZE - length);

        return span;
    }

    /**
     * Extrait d'un vecteur de 32 bits la plage de bits commençant à la position 'start'
     * et de longueur 'length'. Celle-ci sera ici non signée (décalage logique).
     *
     * @param value un vecteur de 32 bits.
     * @param start l'index du premier bit de la plage à extraire.
     * @param length la longueur de la plage voulue.
     * @throws IllegalArgumentException si la plage n'est pas comprise entre 0 et 32 bits ou
     *                                  si length est plus grand que 32.
     * @return la plage de bits non signée en complément deux-à-deux.
     */
    public static int extractUnsigned(int value, int start, int length)
                                      throws IllegalArgumentException {

        Preconditions.checkArgument(length >= 0);
        Preconditions.checkArgument(start >= 0);
        Preconditions.checkArgument(length < VECTOR_SIZE);
        Preconditions.checkArgument(length+start <= VECTOR_SIZE);

        // 1. Décalage à gauche (virer ce qu'il y a devant l'index start)
        int span = value << (VECTOR_SIZE - (start + length));
        // 2. Décalage logique à droite (virer ce qu'il y a après la fin de la plage)
        span = span >>> (VECTOR_SIZE - length);

        return span;
    }
}
