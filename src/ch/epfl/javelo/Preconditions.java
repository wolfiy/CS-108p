package ch.epfl.javelo;

/**
 * Vérifie si les arguments des méthodes sont valides.
 *
 * @author Antoine Cornaz (339526)
 */

public final class Preconditions{

    private Preconditions(){}

    /**
     * Permet de vérifier si un argument est vrai.
     * @param shouldBeTrue la condition à vérifier.
     * @throws IllegalArgumentException si l'argument est invalide.
     */
    public static void checkArgument(boolean shouldBeTrue) throws IllegalArgumentException{
        if (!shouldBeTrue){
            throw new IllegalArgumentException();
        }
    }
}
