package ch.epfl.javelo;

/**
 * Classe qui possède des fonctions mathématiques. Cette librairie a été faite pour ce
 * projet spécifiquement.
 *
 * @author Antoine Cornaz (339526).
 */
public final class Math2 {

    /**
     * Retourne la division de 2 nombres entiers, arrondie vers le haut.
     *
     * @param x un nombre positif ou nul.
     * @param y un nombre positif non-nul.
     * @throws IllegalArgumentException si x est négatif ou
     *                                  si y est négatif ou nul.
     * @return le résultat de la division.
     */
    public static int ceilDiv(int x, int y) throws IllegalArgumentException{
        Preconditions.checkArgument(x >= 0);
        Preconditions.checkArgument(y > 0);

        return (x+y-1)/y;
    }

    /**
     * Permet de faire une interpolation linéaire de 'x' sur la droite entre y_0 et y_1.
     *
     * @param y0 valeur de f(x) = y en x=0.
     * @param y1 valeur de f(x) = y en x=1.
     * @param x la valeur de x, comprise entre 0 et 1.
     * @return la valeur de f(x) = y interpolée sur la droite.
     */
    public static double interpolate(double y0, double y1, double x){
        double a = y1 - y0;
        return Math.fma(a, x, y0);
    }

    /**
     * Vérifie que 'v' est compris entre un minimum et un maximum.
     *
     * @param min la borne inférieure.
     * @param v la valeur à vérifier.
     * @param max la borne supérieure.
     * @throws IllegalArgumentException si le maximum est plus petit que le minimum.
     * @return la valeur la plus proche comprise dans l'intervalle.
     */
    public static int clamp(int min, int v, int max){
        Preconditions.checkArgument(min <= max);
        if (v < min) return min;
        else if (v > max) return max;
        return v;
    }

    /**
     * Vérifie que 'v' est compris entre un minimum et un maximum.
     *
     * @param min la borne inférieure.
     * @param v la valeur à vérifier.
     * @param max la borne supérieure.
     * @throws IllegalArgumentException si le minimum est plus grand que le maximum.
     * @return la valeur la plus proche comprise dans l'intervalle.
     */
    public static double clamp(double min, double v, double max){
        Preconditions.checkArgument(min <= max);
        if (v < min) return min;
        else if (v > max) return max;
        return v;
    }

    /**
     * Représente la fonction inverse du sinus hyperbolique (argsinh(x)).
     *
     * @param x la variable en radians.
     * @return argsinh(x).
     */
    public static double asinh(double x){
        double root = Math.sqrt(1 + x*x);
        return Math.log(x + root);
    }

    /**
     * Produit scalaire entre un vecteur u et un autre vecteur v.
     *
     * @param uX coordonné x de u.
     * @param uY coordonné y de u.
     * @param vX coordonné x de v.
     * @param vY coordonné y de v.
     * @return le produit scalaire de u et v.
     */
    public static double dotProduct(double uX, double uY, double vX, double vY){
        return Math.fma(uX, vX, uY * vY);
    }

    /**
     * Permet d'obtenir le carré de la norme d'un vecteur u. Plus rapide que la norme, car il
     * n'y a pas de racine à calculer.
     *
     * @param uX coordonnée x.
     * @param uY coordonnée y.
     * @return le carré de la norme.
     */
    public static double squaredNorm(double uX, double uY){
        return dotProduct(uX, uY, uX, uY);
    }

    /**
     * Calcule la norme du vecteur u.
     *
     * @param uX coordonnée x de u.
     * @param uY coordonnée Y de u.
     * @return longueur du vecteur.
     */
    public static double norm(double uX, double uY){
        return Math.sqrt(squaredNorm(uX, uY));
    }

    /**
     * Retourne la taille de la projection du vecteur AP sur le vecteur AB
     *
     * @param aX coordonnée x du point A.
     * @param aY coordonnée y du point A.
     * @param bX coordonnée x du point B.
     * @param bY coordonnée y du point B.
     * @param pX coordonnée x du point P.
     * @param pY coordonnée y du point P.
     * @return la longueur de la projection.
     */
    public static double projectionLength(double aX, double aY,
                                          double bX, double bY,
                                          double pX, double pY){
        double uX = pX - aX;
        double uY = pY - aY;

        double vX = bX - aX;
        double vY = bY - aY;

        double dotPro = dotProduct(uX, uY, vX, vY);
        double norme = norm(vX, vY);

        return dotPro/norme;
    }
}