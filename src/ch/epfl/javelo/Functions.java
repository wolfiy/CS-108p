package ch.epfl.javelo;

import java.util.function.DoubleUnaryOperator;

/**
 * Classe permettant la création de fonctions mathématiques, des réels vers les réels.
 *
 * @author Sébastien Tille (313220)
 */
public final record Functions() {

    /**
     * Créé une fonction constante de valeur y.
     *
     * @param y la constante.
     * @return une fonction constante de valeur y.
     */
    public static DoubleUnaryOperator constant(double y) {
        return new Constant(y);
    }

    /**
     * Créé une fonction permettant d'interpoler linéairement entre chaque échantillon du tableau
     * sample de points. Chacun de ces points est espacé de deux mètres du suivant.
     *
     * @param samples tableau contenant l'ensemble des points à interpoler.
     * @param xMax limite de la plage à interpoler.
     * @throws IllegalArgumentException si le tableau de sample contient moins de 2 éléments,
     *                                  si 'xMax' est inférieur à 0.
     * @return la fonction d'interpolation linéaire.
     */
    public static DoubleUnaryOperator sampled(float[] samples, double xMax)
                                      throws IllegalArgumentException {
        Preconditions.checkArgument(samples.length >= 2);
        Preconditions.checkArgument(xMax > 0);
        return new Sampled(samples, xMax);
    }

    /**
     * Classe propre à 'Functions' permettant la création de fonctions constantes.
     */
    private static final record Constant(double constante) implements DoubleUnaryOperator {
        @Override
        public double applyAsDouble(double operand) {
            return constante;
        }
    }

    /**
     * Classe propre à 'Functions', permettant la création d'une fonction de type 'sampled',
     * laquelle permet d'interpoler linéairement les espaces entre chaque point de donnée (séparés
     * de 2 m).
     *
     * @param samples tableau de données.
     * @param xMax f(x) maximum, le tableau sera agrandi en conséquence.
     */
    private static final record Sampled(float[] samples, double xMax)
                                implements DoubleUnaryOperator {

        /**
         * Implémentation de l'interpolation linéaire.
         *
         * Ici, l'opérant représente notre x, et la méthode applyAsDouble joue le rôle de f(x). Le
         * tableau contient des f(x) = y, et l'indice auquel se trouve chaque élément représente
         * le f^-1(y) = x.
         *
         * @param operand x.
         * @return f(x) interpolé.
         */
        @Override
        public double applyAsDouble(double operand) {

            double y;

            if (operand <= 0) y = samples[0];
            else if (operand >= xMax) {
                // Si x est après le dernier élément de la plage de donnée, donne la dernière valeur.
                y = samples[samples.length-1];
            } else {
                // Nombre d'intervalle.
                int interval = samples.length - 1;

                /*
                 * Par exemple, chaque valeur est décalée de 3.5:
                 *                    0 -> 1.7
                 *                    2 -> 3.8
                 *                    4 -> 5.7
                 * Alors tailleInterval vaudra 2.
                 */
                double intervalSize = (xMax/(double)interval);

                // Intervalle dans lequel x se trouve.
                int operandInterval = (int) Math.ceil(operand / intervalSize)-1;

                // Dit si c'est à la moitié de l'intervalle, à 90% de intervalle, etc. (ratio).
                double ratio = (operand - operandInterval*intervalSize) / intervalSize;

                // Moyenne pondérée entre la mesure précédente et la suivante. Pondérée par rapport
                // à la distance.
                y = Math2.interpolate(samples[operandInterval],
                                      samples[operandInterval+1],
                                      ratio);
            }

            return y;
        }
    }
}
