package ch.epfl.javelo.gui;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.PointWebMercator;
import ch.epfl.javelo.routing.Route;
import ch.epfl.javelo.routing.RoutePoint;
import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import java.util.function.Consumer;

/**
 * S'occupe d'instancier la carte, l'ensemble des points de passage et la route.
 * Elle gère aussi la detection du curseur sur la carte lorsqu'il est proche de la route.
 *
 * @author Antoine Cornaz (339526)
 */
public final class AnnotatedMapManager {

    private final StackPane mainPane;
    private final Property<Point2D> mousePositionOnRoutePropertyPoint;
    private final DoubleProperty mousePositionOnRoutePropertyDouble;
    private final ObjectProperty<MapViewParameters> mapViewParametersP;
    private final RouteBean routeBean;

    private final static int INITIAL_ZOOM_LEVEL = 12;
    private final static int INITIAL_X = 543200;
    private final static int INITIAL_Y = 370650;

    private final static int MAX_DISTANCE_CURSOR = 15;

    private final static String MAP_STYLE = "map.css";

    /**
     * Constructeur d'une carte annotée.
     *
     * @param graph Graphique des routes et pistes cyclables
     * @param tileManager Tuiles pour le fond de la carte
     * @param routeBean conteneur de la route et de la position à mettre en évidence.
     * @param errorConsumer Consommateur d'erreur.
     */
    public AnnotatedMapManager(Graph graph, TileManager tileManager,
                               RouteBean routeBean, Consumer<String> errorConsumer){

        MapViewParameters mapViewParameters
                = new MapViewParameters(INITIAL_ZOOM_LEVEL, INITIAL_X, INITIAL_Y);

        ObservableList<Waypoint> waypoints = routeBean.getWaypoints();

        mapViewParametersP =
                new SimpleObjectProperty<>(mapViewParameters);

        WaypointsManager waypointsManager =
                new WaypointsManager(graph,
                        mapViewParametersP,
                        waypoints,
                        errorConsumer);

        BaseMapManager baseMapManager =
                new BaseMapManager(tileManager,
                        waypointsManager,
                        mapViewParametersP);

        RouteManager routeManager = new RouteManager(routeBean, mapViewParametersP);
        Pane baseMapPane = baseMapManager.pane();
        Pane waypointPane = waypointsManager.pane();
        Pane routePane = routeManager.pane();

        this.routeBean = routeBean;
        this.mainPane = new StackPane(baseMapPane, routePane, waypointPane);
        this.mousePositionOnRoutePropertyPoint = new SimpleObjectProperty<>(null);
        this.mousePositionOnRoutePropertyDouble = new SimpleDoubleProperty(Double.NaN);

        addListeners(mainPane, routeBean);
        mainPane.getStylesheets().add(MAP_STYLE);
    }

    /**
     * Permet d'obtenir le panneau contenant le fond de carte, les points de passages et la route.
     *
     * @return le panneau.
     */
    public Pane pane(){
        return this.mainPane;
    }

    /**
     * Position mise en évidence.
     *
     * @return Position mise en évidence ou null si la souris est trop loin de l'itinéraire.
     */
    public DoubleProperty mousePositionOnRouteProperty(){
        return mousePositionOnRoutePropertyDouble;
    }

    /**
     * Permet d'ajouter les auditeurs.
     *
     * @param mainPane le panneau principal.
     * @param routeBean le 'bean'.
     */
    private void addListeners(StackPane mainPane, RouteBean routeBean){
        mainPane.setOnMouseExited(e -> {
            this.mousePositionOnRoutePropertyDouble.setValue(Double.NaN);
            updateMouseDoubleProperty();
        });
        mainPane.setOnMouseMoved(e -> {
            this.mousePositionOnRoutePropertyPoint.setValue(new Point2D(e.getX(), e.getY()));
            updateMouseDoubleProperty();
        });

        routeBean.getRoute().addListener(e -> updateMouseDoubleProperty());
    }

    /**
     * Permet de mettre à jour la propriété
     */
    private void updateMouseDoubleProperty(){
        Point2D position2D = mousePositionOnRoutePropertyPoint.getValue();
        Route route = routeBean.getRoute().get();
        if (position2D == null || route == null) {
            mousePositionOnRoutePropertyDouble.setValue(Double.NaN);
        } else {
            PointWebMercator pointWebMercatorCursor = mapViewParametersP.get()
                    .pointAt(position2D.getX(), position2D.getY());

            // Pour obtenir le pointWebMercator le plus proche, sur la route.
            PointCh pointRoute = pointWebMercatorCursor.toPointCh();
            if (pointRoute != null) {
                RoutePoint routePoint = route.pointClosestTo(pointRoute);
                PointCh pointChRoute = routePoint.point();
                PointWebMercator pointWebMercatorRoute = PointWebMercator.ofPointCh(pointChRoute);

                int zoom = mapViewParametersP.get().zoom();

                double x1 = pointWebMercatorRoute.xAtZoomLevel(zoom);
                double y1 = pointWebMercatorRoute.yAtZoomLevel(zoom);

                double x2 = pointWebMercatorCursor.xAtZoomLevel(zoom);
                double y2 = pointWebMercatorCursor.yAtZoomLevel(zoom);

                double distance = Math2.norm(x1 - x2, y1 - y2);

                if (distance <= MAX_DISTANCE_CURSOR) {
                    mousePositionOnRoutePropertyDouble.setValue(routePoint.position());
                } else {
                    mousePositionOnRoutePropertyDouble.setValue(Double.NaN);
                }
            }
        }
    }
}
