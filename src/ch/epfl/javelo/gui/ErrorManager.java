package ch.epfl.javelo.gui;

import javafx.animation.FadeTransition;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * Classe permettant la gestion des erreurs en créant un panneau sur lequel placer celles-ci.
 *
 * @author Antoine Cornaz (339526)
 */
public final class ErrorManager {

    private final Text text;
    private final SequentialTransition transition;

    // Transition
    private final static String STYLE_ERROR = "error.css";
    private final static double[] TIME_INTERVAL = {0.2, 2, 0.5}; // Temps exprimé en secondes.
    private final static double[] OPACITY = {0, 0.8, 0};
    private final VBox vBox;

    /**
     * Constructeur du gestionnaire d'erreurs.
     */
    public ErrorManager() {
        this.text = new Text();
        this.vBox = new VBox(text);
        this.vBox.getStylesheets().add(STYLE_ERROR);
        this.transition = transition(vBox);

        // Pour ne pas bloquer les clicks de souris sur les panneaux en dessous et pouvoir
        // changer la taille de la fenêtre.
        vBox.setMouseTransparent(true);
        vBox.setPickOnBounds(false);
    }

    /**
     * Permet d'obtenir le panneau d'erreur.
     *
     * @return le panneau d'erreur.
     */
    public Pane pane(){
        return vBox;
    }

    /**
     * Demande au panneau d'afficher une erreur et fait un <i>beep</i>.
     *
     * @param displayedText erreur à afficher.
     */
    public void displayError(String displayedText){
        java.awt.Toolkit.getDefaultToolkit().beep();
        text.setText(displayedText);
        transition.stop();
        transition.play();
    }

    /**
     * Création de la transition du message d'erreur.
     *
     * @param node noeud sur lequel la transition se passe.
     * @return l'ensemble de la transition.
     */
    private SequentialTransition transition(Node node){

        // Fondu entrant.
        FadeTransition fadeBegin = new FadeTransition(Duration.seconds(TIME_INTERVAL[0]), node);
        fadeBegin.setFromValue(OPACITY[0]);
        fadeBegin.setToValue(OPACITY[1]);

        // Pause.
        PauseTransition pauseTransition = new PauseTransition(Duration.seconds(TIME_INTERVAL[1]));

        // Fondu sortant.
        FadeTransition fadeEnd = new FadeTransition(Duration.seconds(TIME_INTERVAL[2]), node);
        fadeEnd.setFromValue(OPACITY[1]);
        fadeEnd.setToValue(OPACITY[2]);

        return new SequentialTransition(node, fadeBegin, pauseTransition, fadeEnd);
    }
}
