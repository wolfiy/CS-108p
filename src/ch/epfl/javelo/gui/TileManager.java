package ch.epfl.javelo.gui;

import ch.epfl.javelo.Preconditions;
import javafx.scene.image.Image;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashMap;

import static java.lang.Math.max;

/**
 * Gestionnaire de tuiles OpenStreetMap. Permet de télécharger et stocker dans la mémoire et le
 * cache les images de la carte.
 *
 * @author Antoine Cornaz (339526)
 */
public final class TileManager {

    private final static int CACHE_SIZE = 100;

    private final Path basePath;
    private final String serverName;
    private final LinkedHashMap<TileId, Image> tileCache;

    /**
     * Constructeur du gestionnaire de tuiles.
     *
     * @param path Chemin d'accès au répertoire contenant le cache disque.
     * @param serverName Nom du serveur de Tuile.
     *                   Exemples: tile.openstreetmap.org
     *                             cartodb-basemaps-1.global.ssl.fastly.net/dark_all
     */
    public TileManager(Path path, String serverName){
        this.basePath = path;
        this.serverName = serverName;
        tileCache = new LinkedHashMap<>(CACHE_SIZE);
    }

    /**
     * Permet de récupérer l'image correspondante à la tuile d'identité 'tileId'. Celle-ci
     * est d'abord cherchée dans le cache mémoire, ensuite dans le cache disque, et si elle
     * n'est pas trouvée, elle est téléchargée depuis le serveur puis gardée en mémoire.
     *
     * @param tileId identité de la tuile recherchée.
     * @return l'image correspondante, ou null si l'image n'est ni en mémoire ni sur le serveur.
     */
    public Image imageForTileAt(TileId tileId) {
        try {
            return getImageCache(tileId);
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
            return null;
        }
    }

    /**
     * Permet d'effectivement récupérer l'image correspondant à la tuile d'identité 'tileId'.
     * Permet de récupérer l'image correspondante à la tuile d'identité 'tileId'. Celle-ci
     * est d'abord cherchée dans le cache mémoire, ensuite dans le cache disque, et si elle
     * n'est pas trouvée, elle est téléchargée depuis le serveur puis gardée en mémoire. Si
     * le cache est plein, l'élément le moins utilisé est jeté.
     *
     * @param tileId l'identité de la tuile.
     * @return l'image correspondante.
     * @throws IOException en cas de problème d'écriture ou de lecture.
     */
    private Image getImageCache(TileId tileId) throws IOException {
        Image tile = tileCache.getOrDefault(new TileId(tileId.zoomLevel, tileId.x, tileId.y),
                                 null);
        if (tile == null) {
            InputStream stream = getImageMemory(tileId);
            tile = new Image(stream);
            setImageCache(tile, tileId);
        }
        return tile;
    }

    /**
     * Permet de récupérer l'image correspondant à la tuile d'identité 'tileId' dans la mémoire. Si
     * elle ne s'y trouve pas, elle est recherchée dans le serveur puis stockée dans la mémoire.
     *
     * @param tileId l'identité de la tuile.
     * @return l'image de la tuile.
     * @throws IOException en cas de problèmes de lecture ou d'écriture.
     */
    private InputStream getImageMemory(TileId tileId) throws IOException {

        Path memoryPath = getPathMemory(tileId);
        String saveFile = memoryPath.toString();

        if (!Files.exists(memoryPath)) setImageMemory(getImageServer(tileId), tileId);
        assert Files.exists(memoryPath);

        return new FileInputStream(saveFile);
    }

    /**
     * Récupère l'image de la tuile d'identité 'tildeId' sur le serveur.
     *
     * @param tileId l'identité de la tuile.
     * @return l'image correspondante.
     * @throws IOException en cas de problèmes de lecture ou d'écriture.
     */
    private InputStream getImageServer(TileId tileId) throws IOException {
        // Préparation du lien vers le serveur.
        String serverUrl = "https://" + serverName + "/" +
                            tileId.zoomLevel + "/" + tileId.x + "/" + tileId.y + ".png";

        URL url = new URL(serverUrl);
        URLConnection connection = url.openConnection();
        connection.setRequestProperty("User-Agent", "JaVelo");

        return connection.getInputStream();
    }

    /**
     * Stocke l'image dans le cache.
     *
     * @param image l'image à stocker.
     * @param tileId meta-donnée de l'image.
     */
    private void setImageCache(Image image, TileId tileId){
        tileCache.put(tileId, image);
    }

    /**
     * Stocke l'image dans la mémoire.
     *
     * @param stream lien de l'image à stocker.
     * @param tileId identité de la tuile.
     */
    private void setImageMemory(InputStream stream, TileId tileId) throws IOException {
        // Préparation du stream de stockage.
        // Texte du fichier où stocker et création des dossiers nécessaires.
        Path memoryPath = getPathMemory(tileId);
        String saveFile = memoryPath.toString();
        Files.createDirectories(memoryPath.getParent());

        try (FileOutputStream o = new FileOutputStream(saveFile)) {
            stream.transferTo(o);
            stream.close();
        }
    }

    /**
     * Chemin d'accès à une tuile.
     *
     * Exemple pour une tuile:
     * x = 4, y = 7, zoom = 19.
     *
     * *NIX
     * Le <i>path</i> serait: base/19/4/7.png
     *
     * Windows
     * Le <i>path</i> serait: base\\19\\4\\7.png
     *
     * @param tileId identité de la tuile.
     * @return le chemin d'accès vers l'image.
     */
    private Path getPathMemory(TileId tileId) {
        Path savePath = basePath;
        savePath = savePath.resolve(String.valueOf(tileId.zoomLevel))
                           .resolve(String.valueOf(tileId.x))
                           .resolve(tileId.y + ".png");
        return savePath;
    }

    /**
     * Enregistrement imbriqué représentant l'identité d'une tuile (petite image faisant partie de
     * la carte complète d'OpenStreetMap).
     *
     * @param zoomLevel le niveau de zoom de la tuile.
     * @param x l'index x de la tuile.
     * @param y l'index y de la tuile.
     */
    public record TileId(int zoomLevel, int x, int y) {
        public TileId {
            Preconditions.checkArgument(isValid(zoomLevel, x, y));
        }

        /**
         * Vérifie la validité d'une tuile.
         *
         * Une tuile est dite valide si ses coordonnées 'x' et 'y' sont positives, et si son
         * niveau de zoom est compris entre 0 (compris) et 19 (non compris).
         *
         * @param zoomLevel le niveau de zoom de la tuile.
         * @param x l'index x de la tuile.
         * @param y l'index y de la tuile.
         * @return vrai si la tuile est valide.
         */
        public static boolean isValid(int zoomLevel, int x, int y) {
            if (x < 0 || y < 0 || zoomLevel < 0 || zoomLevel > 19) return false;

            // Vérification des valeurs de 'x' et 'y' en fonction du niveau de zoom.
            // Maximum entre 'x' et 'y'.
            int actualMax = max(x,y);

            // Formule 4^z.
            // 0 -> 0
            // 1 -> 3
            // 2 -> 15
            int theoreticalMax = maxValue(zoomLevel);

            // Maximum inclus.
            return (actualMax <= theoreticalMax);
        }

        /**
         * Permet d'obtenir la valeur maximale que peuvent prendre les coordonnées 'x' et 'y' des
         * tuiles en fonction d'un certain zoom.
         *
         * @param zoomLevel le niveau de zoom.
         * @return la valeur maximale.
         */
        public static int maxValue(int zoomLevel) {
            Preconditions.checkArgument(zoomLevel >= 0);
            Preconditions.checkArgument(zoomLevel <= 19);

            return (int) Math.scalb(1, zoomLevel) - 1;
        }
    }
}
