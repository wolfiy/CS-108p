package ch.epfl.javelo.gui;

import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.PointWebMercator;
import ch.epfl.javelo.routing.Edge;
import ch.epfl.javelo.routing.Route;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polyline;

import java.util.ArrayList;
import java.util.List;

/**
 * Gestionnaire d'itinéraire. Permet d'afficher le chemin sur la carte.
 *
 * @author Antoine Cornaz (339526)
 */
public final class RouteManager {

    private Polyline polyline;
    private final Circle circle;
    private final Pane pane;
    private final RouteBean routeBean;
    private final ReadOnlyObjectProperty<MapViewParameters> parameters;

    private final static double RADIUS = 5.0;
    private final static String ID_ROUTE = "route";
    private final static String ID_CIRCLE = "highlight";

    // Nombre de coordonnées (ici 'x' et 'y').
    private final static int DIMENSION = 2;
    private final static int MINIMUM_POINTS_LINE = 2;

    private double[] relativeCoordinates;


    /**
     * Constructeur d'un gestionnaire d'itinéraire.
     *
     * @param routeBean conteneur pour le trajet.
     * @param parameters paramètres de la carte.
     */
    public RouteManager(RouteBean routeBean,
                        ReadOnlyObjectProperty<MapViewParameters> parameters){

        this.pane = new Pane();
        this.parameters = parameters;
        this.routeBean = routeBean;
        this.pane.setPickOnBounds(false);
        this.relativeCoordinates = pointsCoordinates(routeBean, this.parameters.get());
        this.circle = new Circle(RADIUS);
        this.polyline = new Polyline();

        addListeners(parameters, routeBean);
        update();
    }

    /**
     * Panneau contenant le trajet et le cercle
     *
     * @return le panneau du trajet et du cercle.
     */
    public Pane pane(){
        return this.pane;
    }

    /**
     * Permet de mettre à jour le panneau, la ligne et le cercle.
     */
    private void update(){

        if (polyline.getPoints().size() < MINIMUM_POINTS_LINE) {
            circle.setVisible(false);
            polyline.setVisible(false);
        } else {
            if (Double.isNaN(routeBean.highlightedPositionProperty().get())){
                circle.setVisible(false);
            } else {
                circle.setVisible(true);
                updateCircle(circle, routeBean, this.parameters.get());
                addCircleListener(circle);
            }
            polyline.setVisible(true);
        }

        ObservableList<Node> paneChildren = pane.getChildren();
        paneChildren.clear();
        paneChildren.add(polyline);
        paneChildren.add(circle);
    }

    /**
     * Permet d'ajouter des auditeurs.
     *
     * @param mapObject la propriété contenant la carte.
     * @param routeBean la propriété 'bean'.
     */
    private void addListeners(ReadOnlyObjectProperty<MapViewParameters> mapObject,
                              RouteBean routeBean){

        mapObject.addListener((changingProperty, oldValue, newValue) -> {
            int deltaZoom = newValue.zoom() - oldValue.zoom();
            int deltaX = newValue.x() - oldValue.x(), deltaY = newValue.y() - oldValue.y();

            if (deltaZoom != 0) zoom(oldValue, newValue);
            else if (deltaX != 0 || deltaY != 0) move(deltaX, deltaY);
        });

        routeBean.getRoute().addListener((changing, oldValue, newValue) -> {
            this.polyline = updatePolyline(routeBean, parameters.get());
            update();
        });

        routeBean.highlightedPositionProperty().addListener((changing, oldValue, newValue) -> {
            update();

            if (oldValue == null && newValue != null) circle.setVisible(true);
            if (oldValue != null && newValue == null) circle.setVisible(false);
        });

    }

    /**
     * Permet d'ajouter les auditeurs sur le cercle.
     *
     * @param circle le cercle.
     */
    private void addCircleListener(Circle circle){
        circle.setOnMouseClicked(e -> {
            Route route = routeBean.getRoute().get();
            double highlightedPosition = routeBean.highlightedPosition();
            int node = route.nodeClosestTo(highlightedPosition);
            int index = 1 + routeBean.indexOfNonEmptySegmentAt(highlightedPosition);

            Point2D point2D = circle.localToParent(e.getX(), e.getY());
            MapViewParameters map = parameters.get();
            PointCh pointCh = map.pointAt(point2D.getX(), point2D.getY()).toPointCh();

            Waypoint waypoint = new Waypoint(pointCh, node);
            ObservableList<Waypoint> waypoints = routeBean.getWaypoints();

            waypoints.add(index, waypoint);
        });
    }

    /**
     * Permet d'agrandir la ligne en fonction du zoom.
     *
     * @param oldMap l'ancienne carte.
     * @param newMap la nouvelle carte.
     */
    private void zoom(MapViewParameters oldMap, MapViewParameters newMap){
        ObservableList<Double> pointsPoly = polyline.getPoints();
        double[] layout = {polyline.getLayoutX(), polyline.getLayoutY()};

        int i = 0;
        List<Double> layoutPoint = new ArrayList<>();
        for (Double point: pointsPoly) {
            layoutPoint.add(layout[i % 2] + point);
            i++;
        }

        PointWebMercator[] pointWM = getPointsWMOfPolyline(layoutPoint, oldMap);
        this.relativeCoordinates = getPointsWMOfPolylineDouble(pointWM, newMap);
        this.polyline = updatePolyline(this.relativeCoordinates);

        update();
    }


    /**
     * Permet d'obtenir un tableau contenant les points de la ligne au format WebMercator.
     *
     * @param pointsPoly la liste de points de la ligne.
     * @param map la carte.
     * @return le tableau contenant les points au format Web Mercator.
     */
    private PointWebMercator[] getPointsWMOfPolyline(List<Double> pointsPoly,
                                                     MapViewParameters map) {

        PointWebMercator[] pointWM = new PointWebMercator[pointsPoly.size()/ DIMENSION];

        for (int i = 0; i < pointsPoly.size()/ DIMENSION; i++) {
            double x = pointsPoly.get(i * DIMENSION);
            double y = pointsPoly.get(i * DIMENSION + 1);

            pointWM[i] =  map.pointAt(x, y);
        }
        return pointWM;
    }

    /**
     * Permet d'obtenir les points de la ligne sous la forme d'un tableau de doubles.
     *
     * @param pointsWM le tableau de points Web Mercator.
     * @param map la carte.
     * @return le tableau de double correspondant.
     */
    private double[] getPointsWMOfPolylineDouble(PointWebMercator[] pointsWM,
                                                 MapViewParameters map){

        double [] pointPoly = new double[pointsWM.length * DIMENSION];

        int i = 0;
        for (PointWebMercator point : pointsWM) {

            pointPoly [i] = map.viewX(point);
            i++;

            pointPoly[i] = map.viewY(point);
            i++;
        }

        return pointPoly;
    }


    /**
     * Permet de recalculer les données de la ligne lors d'un déplacement de la carte.
     *
     * @param deltaX la différence de 'x'.
     * @param deltaY la différence de 'y'.
     */
    private void move(int deltaX, int deltaY) {
        if (relativeCoordinates != null) {
            for (int i = 0; i < relativeCoordinates.length / 2; i++) {
                relativeCoordinates[i * DIMENSION] -= deltaX;
                relativeCoordinates[i * DIMENSION + 1] -= deltaY;
            }
        }

        circle.setCenterX(circle.getCenterX() - deltaX);
        circle.setCenterY(circle.getCenterY() - deltaY);

        polyline.setLayoutX(polyline.getLayoutX() - deltaX);
        polyline.setLayoutY(polyline.getLayoutY() - deltaY);
    }

    /**
     * Permet de mettre à jour le cercle.
     *
     * @param circle le cercle.
     * @param routeBean le 'bean'.
     * @param map la carte.
     */
    private void updateCircle(Circle circle, RouteBean routeBean, MapViewParameters map) {
        Route route = routeBean.getRoute().get();
        if (route == null) return;
        double roundDistance = routeBean.highlightedPosition();
        PointCh roundPointCh = route.pointAt(roundDistance);
        PointWebMercator roundPointWebMercator = PointWebMercator.ofPointCh(roundPointCh);

        double roundX = map.viewX(roundPointWebMercator), roundY = map.viewY(roundPointWebMercator);
        circle.setCenterX(roundX);
        circle.setCenterY(roundY);
        circle.setId(RouteManager.ID_CIRCLE);
    }

    /**
     * Permet de préparer la mise à jour de la ligne.
     *
     * @param routeBean le 'bean'.
     * @param map la carte.
     * @return la ligne mise à jour.
     */
    private Polyline updatePolyline(RouteBean routeBean, MapViewParameters map){
        return updatePolyline(pointsCoordinates(routeBean, map));
    }

    /**
     * Permet de mettre à jour la ligne.
     *
     * @param pointsWM un tableau contenant les points au format Web Mercator.
     * @return la 'polyline' mise à jour.
     */
    private Polyline updatePolyline(double[] pointsWM){
        Polyline polyline = new Polyline(pointsWM);
        polyline.setId(RouteManager.ID_ROUTE);
        return polyline;
    }

    /**
     * Permet d'obtenir la liste des points au format double.
     *
     * @param routeBean conteneur pour la route.
     * @param map carte sur laquelle placer les points.
     * @return la liste des points au formats webMercator, x1, y1, x2, y2, ... xn, yn.
     */
    private double[] pointsCoordinates(RouteBean routeBean, MapViewParameters map){
        Route route = routeBean.getRoute().get();
        if (route == null) return null;
        List<Edge> edges = route.edges();
        PointWebMercator[] pointsWM = new PointWebMercator[edges.size() + 1];
        pointsWM[0] = PointWebMercator.ofPointCh(edges.get(0).fromPoint());

        int i = 1;
        for (Edge edge : edges) {
            pointsWM[i] = PointWebMercator.ofPointCh(edge.toPoint());
            i++;
        }

        return getPointsWMOfPolylineDouble(pointsWM, map);
    }
}
