package ch.epfl.javelo.gui;

import ch.epfl.javelo.projection.PointWebMercator;
import javafx.geometry.Point2D;


/**
 * Enregistrement représentant les paramètres du fond de carte présenté dans l'interface
 * graphique. Il est constitué d'un niveau de zoom et des coordonnées du coin haut-gauche
 * dans le système WebMercator, au niveau de zoom donné.
 *
 * @author Sébastien Tille (313220)
 */
public record MapViewParameters(int zoom, int x, int y) {

    /**
     * Permet d'obtenir les coordonnées du coin en haut à gauche d'une tuile sous la forme d'un
     * objet 'Point2D'.
     *
     * @return les coordonnées du point haut-gauche.
     */
    public Point2D topLeft() {
        return new Point2D(x, y);
    }

    /**
     * Permet de créer une instance de 'MapViewParameters' identique au récepteur, avec les
     * coordonnées données (dans le système WebMercator) pour le point en haut à gauche.
     *
     * @param x la coordonnée 'x' du point.
     * @param y la coordonnée 'y' du point.
     * @return la MapViewParameters correspondante.
     */
    public MapViewParameters withMinXY(int x, int y) {
        return new MapViewParameters(zoom, x, y);
    }

    /**
     * Permet d'obtenir le point, dans le système Web Mercator, correspondant aux coordonnées
     * données et au niveau de zoom actuel.
     *
     * @param x coordonnée selon 'x' par rapport au coin haut-gauche.
     * @param y coordonnée selon 'y' par rapport au coin haut-gauche.
     * @return le point Web Mercator correspondant.
     */
    public PointWebMercator pointAt(double x, double y) {
        return PointWebMercator.of(zoom, this.x + x, this.y + y);
    }

    /**
     * Permet de connaitre la coordonnée 'x' correspondante au point donnée par rapport au
     * coin haut-gauche.
     *
     * @param point un point au format Web Mercator.
     * @return coordonnée 'x'.
     */
    public double viewX(PointWebMercator point) {
        return point.xAtZoomLevel(zoom) - topLeft().getX();
    }

    /**
     * Permet de connaitre la coordonnée 'x' correspondante au point donnée par rapport au
     * coin haut-gauche.
     *
     * @param point point au format Web Mercator.
     * @return coordonnée 'y'.
     */
    public double viewY(PointWebMercator point) {
        return point.yAtZoomLevel(zoom) - topLeft().getY();
    }
}