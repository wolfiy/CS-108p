package ch.epfl.javelo.gui;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.projection.PointWebMercator;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

/**
 * Classe permettant la gestion du fond de carte de l'application. Elle s'occupe de son affichage
 * ainsi que de l'interaction avec l'utilisateur.
 *
 * @author Antoine Cornaz (339526)
 */
public final class BaseMapManager {

    private final TileManager tileManager;
    private final WaypointsManager wayPointsManager;
    private final ObjectProperty<MapViewParameters> parameters;
    private final Canvas canvas;
    private final Pane pane;

    private boolean redrawNeeded = true;

    private final static int ZOOM_MIN = 8;
    private final static int ZOOM_MAX = 19;

    // Point utilisé pour calculer les différences entre le curseur et l'endroit cliqué.
    private Point2D oldPoint = null;

    /**
     * Constructeur d'un 'BaseMapManager' (gestionnaire de fond de carte).
     *
     * @param tileManager le gestionnaire de tuiles pour afficher les tuiles de la carte.
     * @param wayPointsManager le gestionnaire des points de passage.
     * @param parameters une propriété JavaFX contenant les paramètres de la carte affichée.
     */
    public BaseMapManager(TileManager tileManager, WaypointsManager wayPointsManager,
                          ObjectProperty<MapViewParameters> parameters){
        this.tileManager = tileManager;
        this.wayPointsManager = wayPointsManager;
        this.parameters = parameters;

        canvas = new Canvas();
        pane = new Pane(canvas);
        canvas.widthProperty().bind(pane.widthProperty());
        canvas.heightProperty().bind(pane.heightProperty());

        // Ajout des gestionnaires d'événements.
        addObservers(canvas, pane);
        addListeners(parameters, canvas, pane);
    }

    /**
     * Panneau contenant le fond de carte.
     *
     * @return un panneau avec le fond de la carte.
     */
    public Pane pane() {
        redrawIfNeeded();
        return pane;
    }

    /**
     * Permet d'ajouter des observateurs afin de détecter les changements de la carte.
     * En cas de changements, on demande de redessiner la carte au prochain affichage.
     *
     * @param canvas feuille à observer.
     * @param pane panneau à observer.
     */
    private void addObservers(Canvas canvas, Pane pane) {
        // Changements de la taille du 'canvas' (feuille) ou de la fenêtre de l'application.
        canvas.sceneProperty().addListener((p, oldS, newS) -> {
            assert oldS == null;
            if (newS != null) newS.addPreLayoutPulseListener(this::redrawIfNeeded);
        });

        canvas.widthProperty().addListener(e -> redrawOnNextPulse());
        canvas.heightProperty().addListener(e -> redrawOnNextPulse());

        // Changements de niveau de zoom lors du 'scroll' (molette) de la souris.
        SimpleLongProperty minScrollTime = new SimpleLongProperty();
        pane.setOnScroll(e -> {

            if (e.getDeltaY() == 0d) return;
            long currentTime = System.currentTimeMillis();
            if (currentTime < minScrollTime.get()) return;
            minScrollTime.set(currentTime + 200);
            int zoomDelta = (int) Math.signum(e.getDeltaY());

            Point2D point = new Point2D(e.getX(), e.getY());
            MapViewParameters map = zoom(zoomDelta, point, this.parameters.get());

            // Redessiner la carte si elle a changé.
            if (!map.equals(this.parameters.get())) {
                this.parameters.set(map);
                redrawOnNextPulse();
            }
        });

        // Création d'un 'Waypoint' (point de passage) lors d'un clic sur la carte.
        pane.setOnMouseClicked(e -> {
            // Coordonnées du curseur.
            double mouseX = e.getX(), mouseY = e.getY();

            if (e.isStillSincePress()) wayPointsManager.addWaypoint(mouseX, mouseY);
            redrawOnNextPulse();
        });

        // Déplacement de la carte lors d'un glissement de la souris (bouton principal enfoncé).
        pane.setOnMouseDragged(e -> {
            if (e.isPrimaryButtonDown()) {
                Point2D newPoint = new Point2D(e.getX(), e.getY());

                // On récupère la position avant le déplacement pour calculer le vecteur.
                if (oldPoint == null) oldPoint = newPoint;
                MapViewParameters newMap = moveMap(oldPoint.subtract(newPoint),
                                                   this.parameters.get());

                if (newMap != this.parameters.get()) {
                    oldPoint = newPoint;
                    this.parameters.set(newMap);
                    redrawOnNextPulse();
                }
            }
        });

        // Les déplacements sont terminés une fois le bouton principal de la souris relâché;
        // le point est remis à 'null' pour permettre le prochain calcul du vecteur.
        pane.setOnMouseReleased(e -> oldPoint = null);
    }

    /**
     * Permet d'ajouter des auditeurs.
     *
     * @param mapProperty propriété contenant les paramètres de la carte.
     * @param canvas feuille à observer.
     * @param pane panneau à observer.
     */
    private void addListeners(ObjectProperty<MapViewParameters> mapProperty,
                              Canvas canvas, Pane pane) {
        pane.widthProperty().addListener((p, o, n) -> redrawOnNextPulse());
        canvas.widthProperty().addListener((p, o, n) -> redrawOnNextPulse());
        mapProperty.addListener((o, oV, nV) -> redrawOnNextPulse());
    }


    /**
     * Dessine la carte si besoin, sinon laisse l'ancienne carte.
     */
    private void redrawIfNeeded(){
        if (!redrawNeeded) return;
        redrawNeeded = false;
        paneMap(canvas, tileManager, pane, parameters.get());
    }

    /**
     * Demande a ce que la carte soit redessinée.
     */
    private void redrawOnNextPulse() {
        redrawNeeded = true;
        Platform.requestNextPulse();
    }

    /**
     * Affecte le panneau avec les nouvelles valeurs.
     */
    private void paneMap(Canvas canvas, TileManager tileManager, Pane pane, MapViewParameters map){
        pane.getChildren().clear();
        pane.getChildren().add(canvas);

        GraphicsContext context = canvas.getGraphicsContext2D();
        drawMap(context, tileManager, map, canvas);
    }

    /**
     * Dessine le fond de la carte sur le contexte graphique.
     *
     * @param context canvas ou le dessin sera fait.
     * @param tileManager type est gestion des tuiles.
     * @param map carte du lieu qu'il faut afficher.
     */
    private void drawMap(GraphicsContext context, TileManager tileManager,
                         MapViewParameters map, Canvas canvas){
        // Note: "pwm" ici réfère à "PointWebMercator".
        int zoom = map.zoom();
        double width = canvas.getWidth(), height = canvas.getHeight();
        final PointWebMercator pwmTopLeft = map.pointAt(0,0);
        final PointWebMercator pwmBottomRight = map.pointAt(width, height);

        // Zone de recherche.
        int minCanvasX = (int) Math.floor(Math.scalb(pwmTopLeft.x(), zoom));
        int minCanvasY = (int) Math.floor(Math.scalb(pwmTopLeft.y(), zoom));
        int maxCanvasX = (int) Math.ceil(Math.scalb(pwmBottomRight.x(), zoom));
        int maxCanvasY = (int) Math.ceil(Math.scalb(pwmBottomRight.y(), zoom));

        minCanvasX = clampPosition(minCanvasX, zoom);
        maxCanvasX = clampPosition(maxCanvasX, zoom);
        minCanvasY = clampPosition(minCanvasY, zoom);
        maxCanvasY = clampPosition(maxCanvasY, zoom);

        // Largeur: x.
        // Hauteur: y.
        for (int canvasX = minCanvasX; canvasX <= maxCanvasX; ++canvasX) {
            for (int canvasY = minCanvasY; canvasY <= maxCanvasY; ++canvasY) {
                TileManager.TileId tileId = new TileManager.TileId(zoom, canvasX, canvasY);
                Image image = tileManager.imageForTileAt(tileId);
                assert image != null;

                double x = Math.scalb(canvasX, -zoom);
                double y = Math.scalb(canvasY, -zoom);
                PointWebMercator point = new PointWebMercator(x, y);

                context.drawImage(image, map.viewX(point), map.viewY(point));
            }
        }
    }

    /**
     * Permet de borner la position en fonction du niveau de zoom.
     *
     * @param r argument principal, généralement x ou y.
     * @param zoom niveau de zoom, entre 0 et 19 compris.
     * @return la valeur de r borné.
     */
    private int clampPosition(int r, int zoom){
        return Math2.clamp(0, r, TileManager.TileId.maxValue(zoom));
    }

    /**
     * Permet de borner le niveau de zoom entre le minimum ZOOM_MIN et le maximum ZOOM_MAX.
     *
     * @param zoom niveau de zoom.
     * @return la valeur de r bornée.
     */
    private int clampZoom(int zoom){
        return Math2.clamp(ZOOM_MIN, zoom, ZOOM_MAX);
    }

    /**
     * Zoom dans la carte.
     *
     * @param scroll le changement de zoom.
     * @param pointMousse la position ou zoomer par rapport à la carte.
     * @param map l'ancienne carte.
     * @return une nouvelle carte s'il y a eu un changement et l'ancienne sinon.
     */
    private MapViewParameters zoom(double scroll, Point2D pointMousse, MapViewParameters map){
        int zoom = map.zoom();
        int newZoom = clampZoom((int) Math.round(zoom + scroll));
        final int zoomDifference = newZoom - zoom;

        // 1. Il n'est pas possible de zoomer plus ou moins.
        // 2. L'utilisateur n'a pas assez fait defiler pour changer de niveau de zoom.
        if (zoomDifference == 0) return map;

        PointWebMercator pointWM = map.pointAt(pointMousse.getX(), pointMousse.getY());
        int newX = (int) (pointWM.xAtZoomLevel(newZoom) - pointMousse.getX());
        int newY = (int) (pointWM.yAtZoomLevel(newZoom) - pointMousse.getY());

        return new MapViewParameters(newZoom, newX, newY);
    }

    /**
     * Déplace la carte.
     *
     * @param move distance à parcourir.
     * @param map ancienne carte.
     * @return nouvelle carte déplacée ou l'ancienne s'il n'y a pas eu de changement.
     */
    private MapViewParameters moveMap(Point2D move, MapViewParameters map){
        if (move.getX() == 0 && move.getY() == 0) return map;

        Point2D topLeft = map.topLeft()
                             .add(move);

        return parameters.get().withMinXY((int) topLeft.getX(), (int) topLeft.getY());
    }
}
