package ch.epfl.javelo.gui;

import ch.epfl.javelo.projection.PointCh;

/**
 * Enregistrement représentant un point de passage (waypoint) sur un itinéraire. Il peut s'agir
 * d'un point de départ, d'arrivée ou n'importe quel point placé entre deux.
 *
 * @param pointCh un point dans le système de coordonnée suisse.
 * @param nodeId l'identité du noeud correspondant au point de passage.
 *
 * @author Antoine Cornaz (339526)
 */
public record Waypoint(PointCh pointCh, int nodeId) {}
