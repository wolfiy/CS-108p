package ch.epfl.javelo.gui;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.routing.ElevationProfile;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.geometry.*;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Affine;
import javafx.scene.transform.NonInvertibleTransformException;
import javafx.scene.transform.Transform;

/**
 * Gère l'affichage du profil de l'itinéraire.
 *
 * @author Antoine Cornaz (339526)
 * @author Sébastien Tille (313220)
 */
public final class ElevationProfileManager {

    // Panneaux
    private final BorderPane borderPane;
    private final Pane pane;
    private final Path gridPath;
    private final Group groupPath;
    private final Polygon profilePolygon;
    private final Line line;
    private final Text statistics;
    private final VBox vBox;
    private final ReadOnlyObjectProperty<ElevationProfile> elevationProfileObject;

    // Feuilles de style CSS.
    private final static String ELEVATION_PROFILE_STYLE = "elevation_profile.css";

    // Identités des noeuds.
    private final static String ID_PROFILE_DATA = "profile_data";
    private final static String ID_PROFILE = "profile";
    private final static String ID_GRID = "grid";

    // Classes CSS des étiquettes.
    private final static String LABEL = "grid_label";
    private final static String HORIZONTAL = "grid_horizontal";
    private final static String VERTICAL = "grid_vertical";

    // Texte de statistiques.
    private final static String textStatistic =
            "Longueur : %.1f km" +
            "     Montée : %.0f m" +
            "     Descente : %.0f m" +
            "     Altitude : de %.0f m à %.0f m";

    private final static int DIMENSION = 2;
    private final static int RATIO_KM_M = 1000;

    // Propriétés privées.
    private final ObjectProperty<Rectangle2D> rectangleProperty;
    private final ObjectProperty<Transform> screenToWorldProperty;
    private final ObjectProperty<Transform> worldToScreenProperty;

    // GRILLE
    private final static int MIN_SPACE_DISTANCE_HORIZONTALE = 25;
    private final static int MIN_SPACE_DISTANCE_VERTICALE = 50;
    private final static int NUMBER_OF_EDGES_BOTTOM = 2;

    // Séparation des colonnes. X
    private final static int[] POS_STEPS =
            {1000, 2000, 5000, 10_000, 25_000, 50_000, 100_000};
    // Séparation des lignes. Y
    private final static int[] ELE_STEPS =
            {5, 10, 20, 25, 50, 100, 200, 250, 500, 1_000};

    // Espacements
    private final Insets insets;
    private final static int INSET_TOP = 10;
    private final static int INSET_BOTTOM = 20;
    private final static int INSET_RIGHT = 10;
    private final static int INSET_LEFT = 40;

    // Position mise en évidence
    private final ReadOnlyDoubleProperty profilePositionMap;
    private final SimpleDoubleProperty profilePositionElevation;

    // Textes
    // Paramètres du texte des étiquettes.
    private final static double FONT_SIZE = 10;
    private final static String FONT = "Avenir";
    private final static int ALTITUDE_LABEL_SHIFT = 2;

    /**
     * Constructeur du gestionnaire de profils.
     *
     * @param elevationProfileProp une propriété contenant le profil à afficher.
     * @param profilePositionMap une propriété contenant la position le long du profil à mettre en
     *                       évidence.
     */
    public ElevationProfileManager(ReadOnlyObjectProperty<ElevationProfile> elevationProfileProp,
                                   ReadOnlyDoubleProperty profilePositionMap){

        // SCHEMA...
        this.borderPane = new BorderPane();
        this.borderPane.getStylesheets().add(ELEVATION_PROFILE_STYLE);

        // ...CENTRE
        this.pane = new Pane();
        this.gridPath = new Path();
        this.gridPath.setId(ID_GRID);
        this.groupPath = new Group();
        this.profilePolygon = new Polygon();
        this.profilePolygon.setId(ID_PROFILE);
        this.line = new Line();

        // ...BAS
        this.statistics = new Text();
        this.vBox = new VBox(statistics);
        this.vBox.setId(ID_PROFILE_DATA);

        // Autres
        this.insets = new Insets(INSET_TOP, INSET_RIGHT, INSET_BOTTOM, INSET_LEFT);
        this.elevationProfileObject = elevationProfileProp;
        this.rectangleProperty = new SimpleObjectProperty<>(Rectangle2D.EMPTY);
        this.profilePositionMap = profilePositionMap;
        this.profilePositionElevation = new SimpleDoubleProperty(Double.NaN);

        // Relations JavaFX
        updateRelations();

        // Crée les transformations.
        this.screenToWorldProperty = new SimpleObjectProperty<>();
        this.worldToScreenProperty = new SimpleObjectProperty<>();

        // Mettre le tout à jour lors de la création.
        update();
        setListenerAndBinder(elevationProfileProp, pane);
    }

    /**
     * Permet de mettre à jour les relations enfants/parent des éléments.
     */
    private void updateRelations() {
        borderPane.getChildren().clear();

        ObservableList<Node> childrenPane = pane.getChildren();
        childrenPane.clear();
        childrenPane.add(gridPath);
        childrenPane.add(groupPath);
        childrenPane.add(profilePolygon);
        childrenPane.add(line);

        vBox.getChildren().clear();
        vBox.getChildren().add(statistics);

        borderPane.setCenter(pane);
        borderPane.setBottom(vBox);
    }

    /**
     * Retourne le panneau contenant le dessin du profil, les étiquettes et les statistiques.
     *
     * @return le panneau contenant le dessin du profil.
     */
    public Pane pane(){
        return borderPane;
    }

    /**
     * Permet d'obtenir une propriété en lecture seule contenant la position du curseur de la souris
     * le long du profil (en mètres, arrondie à l'entier le plus proche), ou 'NaN' si celui-ci ne se
     * trouve pas au-dessus du profil.
     *
     * @return la position du curseur, ou 'NaN si celui-ci n'est pas sur un profil.
     */
    public ReadOnlyDoubleProperty mousePositionOnProfileProperty() {
        return profilePositionElevation;
    }


    /**
     * Cette fonction initialise les listeners.
     *
     * @param elevationProfileProp propriété contenant le chemin à représenter.
     * @param pane panneau à observer.
     */
    private void setListenerAndBinder(
            ReadOnlyObjectProperty<ElevationProfile> elevationProfileProp, Pane pane) {

        setListenerPane(pane);
        setBindLine();

        rectangleProperty.bind(Bindings.createObjectBinding(() -> {
        if (pane.getHeight() <= 0 || pane.getWidth() <= 0)
                return new Rectangle2D(0,0,0,0);

            double rectHeight = Math.max(pane.getHeight() - insets.getTop() - insets.getBottom(),
                    0);
            double rectWidth = Math.max(pane.getWidth() - insets.getLeft() - insets.getRight(), 0);

            return new Rectangle2D(insets.getLeft(), insets.getTop(), rectWidth, rectHeight);
            },
            pane.widthProperty(), pane.heightProperty()
        ));

        rectangleProperty.addListener(e -> update());



        elevationProfileProp.addListener(e-> update());
    }

    /**
     * Cette fonction initialise les listeners du panneau.
     *
     * @param pane contenant le chemin à représenter.
     */
    private void setListenerPane(Pane pane) {
        // Pas besoin de witdh, on regarde deja pour le rectangle
        pane.heightProperty().addListener(e -> update());

        pane.setOnMouseExited(e -> profilePositionElevation.setValue(Double.NaN));

        pane.setOnMouseMoved(e -> {
            Rectangle2D rectangle = rectangleProperty.get();
            Point2D positionScreen = new Point2D(e.getX(), e.getY());

            if (rectangle.contains(positionScreen)){
                // Ligne
                Point2D stwPoint = screenToWorldProperty.get().transform(positionScreen);
                profilePositionElevation.setValue(stwPoint.getX());
            } else{
                profilePositionElevation.set(Double.NaN);
            }
        });
    }


    /**
     * Ajoute les liens pour la ligne
     * Déplace la ligne vérticale et la rend transparente si besoin.
     */
    private void setBindLine(){
        line.layoutXProperty().bind(Bindings.createDoubleBinding(() -> {
            if(!Double.isNaN(profilePositionMap.get()) && rectangleProperty.get() != null
                    && elevationProfileObject.get() != null){
                return Math2.clamp(rectangleProperty.get().getMinX(),
                        rectangleProperty.get().getMinX() +
                                profilePositionMap.get() / elevationProfileObject.get().length()
                                        * rectangleProperty.get().getWidth(),
                        rectangleProperty.get().getMaxX());
            }
            return Double.NaN;
        },
            profilePositionMap, rectangleProperty));

        line.visibleProperty().bind(profilePositionMap.greaterThanOrEqualTo(0));
        line.startYProperty().bind(Bindings.select(rectangleProperty, "minY"));
        line.endYProperty().bind(Bindings.select(rectangleProperty, "maxY"));
    }

    /**
     * Permet d'obtenir l'espacement entre les lignes.
     *
     * @return l'espacement selon x si 'x' est vrai, l'espacement selon y sinon.
     */
    private Point2D getSpacingScreen(Point2D spacingWorld) {
        Transform wts = worldToScreenProperty.get();
        return wts.deltaTransform(inverseY(spacingWorld));
    }

    /**
     * inverse la valeur de la composante y d'un point
     * @param point le point
     * @return le point avec (x, -y)
     */
    private Point2D inverseY(Point2D point){
        return new Point2D(point.getX(), -point.getY());
    }

    /**
     * Permet d'obtenir l'espacement de la grille dans les coordonnées du monde réel.
     *
     * @return l'espacement de la grille dans les coordonnées du monde réel.
     */
    private Point2D getSpacingWorldGrid() {
        Point2D minimumDistanceScreen = new Point2D(MIN_SPACE_DISTANCE_VERTICALE,
                                                -MIN_SPACE_DISTANCE_HORIZONTALE);

        Transform stw = screenToWorldProperty.get();

        Point2D minimumDistanceWorld =  stw.deltaTransform(minimumDistanceScreen);
        int distanceWorldX = upperValue((int) minimumDistanceWorld.getX(), POS_STEPS);
        int distanceWorldY = upperValue((int) minimumDistanceWorld.getY(), ELE_STEPS);

        return new Point2D(distanceWorldX, distanceWorldY);
    }
    /**
     * Permet d'obtenir le nombre de lignes de la grille.
     *
     * @param spacing l'espacement entre les lignes.
     * @param shift le décalage depuis le haut gauche de panneau.
     * @return le nombre de lignes.
     */
    private Point2D getNumberOfLineGrid(Point2D spacing, Point2D shift) {
        int x = (int) Math.ceil((rectangleProperty.get().getWidth() + insets.getLeft() - shift.getX())/ spacing.getX());

        double hauteur = ((rectangleProperty.get().getHeight() + insets.getTop()) - shift.getY());
        int y = (int) Math.ceil(hauteur / spacing.getY());
        return new Point2D(x, y);
    }

    /**
     * Permet de créer le grillage.
     *
     * @param decalageGrilleScreen espacement entre 2 lignes dans le refernetiel de l'écran.
     * @param spacingScreen décalage depuis le haut gauche dans le réferentiel de l'écran.
     */
    private void updateGrid(Point2D spacingScreen, Point2D decalageGrilleScreen,
                            Point2D numberOfLine){

        int linesX = (int) numberOfLine.getX();
        int linesY = (int) numberOfLine.getY();

        ObservableList<PathElement> children = gridPath.getElements();
        children.clear();
        createPath(linesX, spacingScreen.getX(), (int) decalageGrilleScreen.getX(),
                (int) insets.getTop(), (int) (pane.getHeight() - insets.getBottom()),
                true, children);
        createPath(linesY, spacingScreen.getY(), (int) decalageGrilleScreen.getY(),
                (int) insets.getLeft(), (int) (pane.getWidth() - insets.getRight()),
                false, children);
    }

    /**
     * Crée les lignes de la grille.
     *
     * @param numberOfLigne nombre de lignes.
     * @param spacing espacement entre les lignes.
     * @param shift décalage dans l'axe oposé.
     * @param shiftStart décalage du départ.
     * @param shiftEnd décalage de l'arivé
     * @param horizontal sens dans laquel la grille est créee.
     * @param list dans laquelle ajoutée les élements.
     */
    private static void createPath(int numberOfLigne, double spacing, int shift,
                                            int shiftStart, int shiftEnd, boolean horizontal,
                                            ObservableList<PathElement> list) {

        for (int i = 0; i < numberOfLigne; i++) {
            double position = spacing * i;

            if(horizontal ) {
                list.add(new MoveTo(position + shift, shiftStart));
                list.add(new LineTo(position + shift, shiftEnd));
            }else {
                list.add(new MoveTo(shiftStart, position + shift));
                list.add(new LineTo(shiftEnd, position + shift));
            }
        }
    }

    /**
     * Permet d'arrondir en fonction des tableaux de position et d'altitude (pour augmenter de 1,
     * 2, 5, 10, ...).
     *
     * @param value la valeur à arrondir.
     * @param table le tableau de référence.
     * @return la valeur arrondie.
     */
    private static int upperValue(int value, int[] table){
        for (int i = 0; i < table.length -1; i++) {
            if(value < table[i]) return table[i];
        }
        return table[table.length-1];
    }

    /**
     * Permet de mettre à jour l'ensemble de l'affichage.
     */
    private void update(){
        if (pane.getHeight() == 0 || pane.getWidth() == 0
                                    || elevationProfileObject.get() == null) return;

        setAffineScreenWorld(pane, elevationProfileObject.get());
        setProfilePolygon(elevationProfileObject.get(), profilePolygon);

        Point2D spacingWorld = getSpacingWorldGrid();
        Point2D spacingScreen = getSpacingScreen(spacingWorld);
        Point2D decalageGrille = decalageGrille(spacingWorld);
        Point2D numberOfLine = getNumberOfLineGrid(spacingScreen, decalageGrille);

        updateGrid(spacingScreen, decalageGrille, numberOfLine);
        updateStats();
        updateRelations();
        updateLabels(spacingScreen, spacingWorld, decalageGrille, numberOfLine);
    }

    /**
     * Décalage de la grille dans le format de l'écran.
     *
     * @param spacingWorld espacement entre les lignes.
     * @return le point à gauche de la ligne du haut.
     */
    private Point2D decalageGrille(Point2D spacingWorld){
        double maxElevationWorld = elevationProfileObject.get().maxElevation();
        double worldSpacingTop = maxElevationWorld % spacingWorld.getY();
        double worldLineTop = maxElevationWorld - worldSpacingTop;
        Point2D worldSpacing = new Point2D(0, worldLineTop);

        return worldToScreenProperty.get().transform(worldSpacing);
    }

    /**
     * Permet de mettre à jour les statistques.
     */
    private void updateStats(){
        ElevationProfile profile = elevationProfileObject.get();
        String stats = String.format(textStatistic, profile.length()/ RATIO_KM_M,
                profile.totalAscent(), profile.totalDescent(),
                profile.minElevation(), profile.maxElevation());
        statistics.setText(stats);
    }

    /**
     * Permet de créer les étiquettes affichant la position et l'altitude autour du profil.
     * @param spacingScreen espacemnent entre les élements au format écran.
     * @param spacingWorld espacemnent entre les élements au format world.
     * @param decalageGrilleScreen décalage du premier élement.
     * @param numberOfLine nombre de lignes à afficher.
     */
    private void updateLabels(Point2D spacingScreen, Point2D spacingWorld,
                              Point2D decalageGrilleScreen, Point2D numberOfLine) {
        Group allTexts = new Group();

        // Position
        updateLabelPosition(spacingScreen, spacingWorld, numberOfLine, allTexts);

        // Altitude
        updateLabelAltitude(spacingScreen, spacingWorld, decalageGrilleScreen,
                numberOfLine, allTexts);

        pane.getChildren().add(allTexts);
    }

    /**
     * Met à jour les labels de l'altitude
     *
     * @param spacingScreen espacemnent entre les élements au format écran.
     * @param spacingWorld espacemnent entre les élements au format world.
     * @param numberOfLine nombre de lignes à afficher.
     * @param allTexts groupe auquel ajouté les élements.
     */
    private void updateLabelPosition(Point2D spacingScreen, Point2D spacingWorld,
                                     Point2D numberOfLine, Group allTexts) {
        for (int i = 0; i < numberOfLine.getX(); ++i) {
            Text posLabel = new Text();
            posLabel.setText(String.valueOf((int)(i * spacingWorld.getX() / RATIO_KM_M)));
            posLabel.setFont(Font.font(FONT, FONT_SIZE));
            posLabel.getStyleClass().add(LABEL);
            posLabel.getStyleClass().add(HORIZONTAL);

            double posShift = posLabel.prefWidth(0)/2.0;
            posLabel.setTextOrigin(VPos.TOP);
            posLabel.setTranslateX(insets.getLeft() + i * spacingScreen.getX() - posShift);
            posLabel.setLayoutY(rectangleProperty.get().getHeight() + FONT_SIZE);

            allTexts.getChildren().add(posLabel);
        }
    }

    /**
     * Met à jour les labels de l'altitude
     *
     * @param spacingScreen espacemnent entre les élements au format écran.
     * @param spacingWorld espacemnent entre les élements au format world.
     * @param decalageGrilleScreen décalage du premier élement.
     * @param numberOfLine nombre de lignes à afficher.
     * @param allTexts groupe auquel ajouté les élements.
     */
    private void updateLabelAltitude(Point2D spacingScreen, Point2D spacingWorld,
                                     Point2D decalageGrilleScreen, Point2D numberOfLine,
                                     Group allTexts) {
        double lowerBound = elevationProfileObject.get().minElevation() % spacingWorld.getY();
        double diff = spacingWorld.getY() - lowerBound;
        double firstEle = elevationProfileObject.get().minElevation() + diff;
        for (int i = 0; i < numberOfLine.getY(); ++i) {
            Text altLabel = new Text();
            String text = String.valueOf(
                    (int)(firstEle + (numberOfLine.getY() - i - 1) * spacingWorld.getY()));
            altLabel.setFont(Font.font(FONT, FONT_SIZE));
            altLabel.setText(text);
            altLabel.getStyleClass().add(LABEL);
            altLabel.getStyleClass().add(VERTICAL);
            altLabel.setTextOrigin(VPos.CENTER);

            double altShift = altLabel.prefWidth(0);
            altLabel.setTranslateX(decalageGrilleScreen.getX() - altShift - ALTITUDE_LABEL_SHIFT);
            altLabel.setTranslateY(i * spacingScreen.getY() + decalageGrilleScreen.getY());

            allTexts.getChildren().add(altLabel);
        }
    }


    /**
     * Par exemple, le point dans le coin haut-gauche du rectangle de la figure 3
     * correspond au couple (0 m, 663 m). Dans le système de coordonnées JavaFX du panneau
     * contenant le rectangle, ce même point a les coordonnées (40, 10).
     * @param pane Panneau central.
     * @param elevationProfile itinéraire.
     */
    private void setAffineScreenWorld(Pane pane, ElevationProfile elevationProfile){
        Affine affine = new Affine();
        // 1 Translation
        affine.prependTranslation(-insets.getLeft(), -insets.getTop());
        // 2 Scale
        double differenceAltitude = elevationProfile.maxElevation() - elevationProfile.minElevation();
        double largeurRectangle = pane.getWidth() - insets.getLeft() - insets.getRight();
        double hauteurRectangle = pane.getHeight() - insets.getBottom() - insets.getTop();
        double scaleX = elevationProfile.length() / largeurRectangle;
        double scaleY = -differenceAltitude / hauteurRectangle;
        affine.prependScale(scaleX, scaleY);
        // 3 Translation
        affine.prependTranslation(0, elevationProfile.maxElevation());

        this.screenToWorldProperty.set(affine);
        try {
            this.worldToScreenProperty.set(affine.createInverse());
        } catch (NonInvertibleTransformException ignored) {
        }
    }

    /**
     * Permet de créer le profil à afficher.
     *
     * @param elevationProfile le profil dont on veut créer le graphe.
     */
    private void setProfilePolygon(ElevationProfile elevationProfile, Polygon polygon){


        int size = (int) rectangleProperty.get().getWidth();
        int sizeArray = DIMENSION * (size + NUMBER_OF_EDGES_BOTTOM);
        double[] profileTable = new double[sizeArray];

        // Rempli le tableau avec les points du profil.
        setProfilArray(elevationProfile, size, profileTable);

        // Rempli la fin du tableau, les 2 coins du bas de la polyline
        setEndArray(sizeArray, profileTable);

        // Affile les valeurs du tableau à la polyline
        setProfilePolygon(polygon, profileTable);
    }

    /**
     * Rempli le tableau avec les valeurs du profil.
     * 
     * @param elevationProfile itinéraire contenant la hauteur.
     * @param size taille du tableau.
     * @param profileTable tableau àr remplir.
     */
    private void setProfilArray(ElevationProfile elevationProfile, int size,
                                double[] profileTable) {

        // Transformations affines passant de l'écran au monde réel.
        Transform stw = screenToWorldProperty.get();
        Transform wts = worldToScreenProperty.get();
        
        for (int i = 0; i < size; i++) {
            // Valeur de X
            profileTable[i*DIMENSION] = i + insets.getLeft();

            // Valeur de Y
            Point2D worldPoint = stw.transform(new Point2D(i + insets.getLeft(), 0));
            double elevation = elevationProfile.elevationAt(worldPoint.getX());
            Point2D screenElevation = wts.transform(0, elevation);
            profileTable[i*DIMENSION + 1] = screenElevation.getY();
        }
    }
    
    /**
     * affile la fin du tableau avec les 2 coins du bas.
     * @param sizeArray taille du tableau.
     * @param profileTable tableau.
     */
    private void setEndArray(int sizeArray, double[] profileTable) {
        // Point en bas à droite
        profileTable[sizeArray - 4] = pane.getWidth() - insets.getRight();
        profileTable[sizeArray - 3] = pane.getHeight() - insets.getBottom();

        // Point en bas à gauche
        profileTable[sizeArray - 2] = insets.getLeft();
        profileTable[sizeArray - 1] = pane.getHeight() - insets.getBottom();
    }

    /**
     * Affile les valeurs du tableau au polygon.
     * @param polygon à mettre à jour.
     * @param profileTable valeurs à données au tableau.
     */
    private void setProfilePolygon(Polygon polygon, double[] profileTable) {
        ObservableList<Double> polygonChildren = polygon.getPoints();
        polygonChildren.clear();

        for (double v : profileTable) {
            polygonChildren.add(v);
        }
    }
}
