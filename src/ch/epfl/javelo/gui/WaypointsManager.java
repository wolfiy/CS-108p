package ch.epfl.javelo.gui;

import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.projection.PointCh;
import ch.epfl.javelo.projection.PointWebMercator;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.shape.SVGPath;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Classe permettant la gestion des points de passage (affichage et interaction).
 *
 * @author Sébastien Tille (313220).
 */
public final class WaypointsManager {

    private final Graph graph;
    private final ObservableList<Waypoint> waypoints;
    private final Consumer<String> consumer;
    private final Pane pane;
    private final ObjectProperty<MapViewParameters> parameters;
    private final List<Node> pins;

    // Point utilisé pour calculer les différences entre le curseur et l'endroit cliqué.
    private Point2D oldPoint = null;

    // Rayon de recherche autour des noeuds pour les points de passage.
    private static final int RADIUS = 500;

    // Chaines représentant le dessin du marqueur.
    private static final String SVG_OUTSIDE = "M-8-20C-5-14-2-7 0 0 2-7 5-14 8-20 20-40-20-40-8-20";
    private static final String SVG_INSIDE = "M0-23A1 1 0 000-29 1 1 0 000-23";
    // Classes JavaFX.
    private final static String PIN_INSIDE = "pin_inside";
    private final static String PIN_OUTSIDE = "pin_outside";
    private final static String PIN = "pin";
    private final static String MIDDLE = "middle";
    private final static String FIRST = "first";
    private final static String LAST = "last";

    // Message d'erreur.
    private final static String ERROR = "Aucune route à proximité!";

    /**
     * Constructeur d'un 'WaypointsManager' (gestionnaire de points de passage).
     *
     * @param graph un graphe.
     * @param parameters une 'MapViewParameters' contenant les paramètres de la carte.
     * @param waypoints une liste de points de passage.
     * @param consumer un consommateur utilisé pour la gestion des erreurs.
     */
    public WaypointsManager(Graph graph, ObjectProperty<MapViewParameters> parameters,
                            ObservableList<Waypoint> waypoints, Consumer<String> consumer){
        this.graph = graph;
        this.waypoints = waypoints;
        this.consumer = consumer;
        this.pane = new Pane();
        this.parameters = parameters;
        this.pins = new ArrayList<>();

        pane.setPickOnBounds(false);
        addListeners();
        updatePins();
    }

    /**
     * Permet de retourner un panneau contenant les points de passage. Simple accesseur.
     *
     * @return un panneau contenant les points de passage.
     */
    public Pane pane(){
        return pane;
    }

    /**
     * Permet d'ajouter le point de passage de coordonnées 'x' et 'y' à la liste de points
     * de passage (système WebMercator) et ajoute un point de passage au noeud du graph le
     * plus proche de celles-ci.
     *
     * @param x coordonnée 'x' du point.
     * @param y coordonnée 'y' du point.
     */
    public void addWaypoint(double x, double y) {
        PointWebMercator pointWM = parameters.get().pointAt(x, y);
        PointCh pointCH = pointWM.toPointCh();
        int node = nodeFromPointCh(pointCH);
        // Recherche du noeud le plus proche. S'il n'y en a pas, stopper l'ajout.


        if(node == -1){
           return;
        }

        // Création et ajout du point de passage.
        Waypoint waypoint = new Waypoint(pointCH, node);
        waypoints.add(waypoint);
    }

    /**
     * Permet de récupérer le noeud le plus proche d'un point.
     *
     * @param point un point dans le système suisse.
     * @return le noeud le plus proche de ce point.
     */
    private int nodeFromPointCh(PointCh point) {
        int node;

        try {
            node = graph.nodeClosestTo(point, RADIUS);
        } catch (IllegalArgumentException exception) {
            node = -1;
        }

        if (node == -1) consumer.accept(ERROR);

        return node;
    }

    /**
     * Permet de créer les points de passage sur la carte.
     *
     * @param waypoints la liste des points de passage.
     */
    private void createWaypointSVG(List<Waypoint> waypoints) {
        int index = 0;
        for (var w : waypoints) {
            // Création des chemins SVG, un pour l'extérieur et un pour le rond intérieur, et
            // assignation des classes correspondantes.
            SVGPath inside = new SVGPath();
            SVGPath outside = new SVGPath();
            outside.setContent(SVG_OUTSIDE);
            inside.setContent(SVG_INSIDE);
            outside.getStyleClass().add(PIN_OUTSIDE);
            inside.getStyleClass().add(PIN_INSIDE);

            // Création d'un groupe représentant le marqueur du point de passage avec ses deux SVG.
            Group pin = new Group(outside, inside);
            pin.getStyleClass().add(PIN);
            pins.add(pin);

            // Gestion des couleurs du marqueur.
            if (index == 0) pin.getStyleClass().add(FIRST);
            else if (index == waypoints.size() - 1) pin.getStyleClass().add(LAST);
            else pin.getStyleClass().add(MIDDLE);

            // Placement du marqueur.
            placeWaypointSVG(pin, w);
            pane.getChildren().add(pin);

            // ======= Gestion des événements ========

            // Attributs utilisés pour la gestion d'évènements.
            final int finalIndex = index;
            final double oldX = pin.getLayoutX(), oldY = pin.getLayoutY();

            // Suppression d'un marqueur.
            pin.setOnMouseReleased(e -> {
                if (!waypoints.isEmpty() && e.isStillSincePress()) {
                    pane.getChildren().clear();
                    waypoints.remove(finalIndex);
                }
            });

            // Déplacement d'un marqueur.
            pin.setOnMouseDragged(e -> {
                Point2D newPoint = new Point2D(e.getSceneX(), e.getSceneY());
                if (oldPoint == null) oldPoint = newPoint;
                Point2D diff = newPoint.subtract(oldPoint);

                pin.setLayoutX(pin.getLayoutX() + diff.getX());
                pin.setLayoutY(pin.getLayoutY() + diff.getY());

                oldPoint = newPoint;

                // Marqueur relâché.
                pin.setOnMouseReleased(ee -> {
                    int i = pins.indexOf(pin);
                    var newPointCh = parameters.get()
                            .pointAt(pin.getLayoutX(), pin.getLayoutY())
                            .toPointCh();
                    int newNode = nodeFromPointCh(newPointCh);

                    // Si aucun noeud n'est à proximité, le marqueur retourne à la position
                    // qu'il occupait avait le déplacement.
                    if (newNode == -1) {
                        pin.setLayoutX(oldX);
                        pin.setLayoutY(oldY);
                        oldPoint = null;
                        return;
                    }

                    // Création du nouveau point de passage et remplacement.
                    Waypoint newWaypoint = new Waypoint(newPointCh, newNode);
                    waypoints.set(i, newWaypoint);
                    oldPoint = null;
                });
            });

            // L'index augmente.
            ++index;
        }
    }

    /**
     * Permet de placer le marqueur du point de passage au bon endroit. Le code dest séparé du
     * reste (createWaypointSVG) pour faciliter la gestion des auditeurs.
     *
     * @param pin un marqueur.
     * @param w le point de passage correspondant.
     */
    private void placeWaypointSVG(Group pin, Waypoint w) {
        PointCh ch = w.pointCh();
        PointWebMercator wm = PointWebMercator.ofPointCh(ch);

        // Placement du marqueur.
        pin.setLayoutX(parameters.get().viewX(wm));
        pin.setLayoutY(parameters.get().viewY(wm));
    }

    /**
     * Permet d'ajouter des auditeurs.
     */
    private void addListeners() {
        // Ajout de marqueurs à l'ajout d'un point de passage.
        waypoints.addListener((Observable o) -> {
            createWaypointSVG(waypoints);
            updatePins();
        });

        // Mettre à jour les marqueurs lors de déplacements/zoom de la carte.
        parameters.addListener(o -> updatePins());
    }

    /**
     * Mets à jour l'affichage des marqueurs.
     */
    private void updatePins() {
        pane.getChildren().clear();
        pins.clear();
        createWaypointSVG(waypoints);
    }
}
