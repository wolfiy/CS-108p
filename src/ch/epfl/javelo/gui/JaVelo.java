package ch.epfl.javelo.gui;

import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.routing.CityBikeCF;
import ch.epfl.javelo.routing.GpxGenerator;
import ch.epfl.javelo.routing.RouteComputer;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.nio.file.Path;
import java.util.function.Consumer;

/**
 * Classe principale du programme.
 *
 * @author Antoine Cornaz (339526)
 */
public final class JaVelo extends Application {

    private final static String GRAPH = "javelo-data";
    private final static String CACHE = "osm-cache";
    private final static String SERVER = "tile.openstreetmap.org";
    private final static int WINDOW_WIDTH = 800;
    private final static int WINDOW_HEIGHT = 600;
    private final static String TITLE = "JaVelo";
    private final static String EXPORT_TEXT = "Exporter GPX";
    private final static String FILE_MENU = "Fichier";
    private final static String EXPORTED_FILE_NAME = "javelo.gpx";

    /**
     * Classe principale.
     *
     * @param args arguments donnés en paramètre pour le programme.
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Application principale de JaVelo.
     *
     * @param primaryStage page principal de l'application.
     * @throws Exception générale sur les fenêtres.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {

        Graph graph = Graph.loadFrom(Path.of(GRAPH));
        Path cache = Path.of(CACHE);
        TileManager tileManager = new TileManager(cache, SERVER);


        /* On a la structure suivante:

                                                    BorderPane
                                                        |
                                         ---------------------------------------
                                         |                                     |
                                    StackPane                               MenuBar
                                    /        \                                 |
                        CentralPane           ErrorPane                      Menu
                        /       \                                              |
            AnnotatedPane       ElevationPane                               MenuItem

         */

        // RouteBean et RouteComputer
        RouteComputer computer = new RouteComputer(graph, new CityBikeCF(graph));
        RouteBean bean = new RouteBean(computer);

        // ErrorPane
        ErrorManager errorManager = new ErrorManager();
        Pane errorPane = errorManager.pane();
        Consumer<String> errorConsumer = errorManager::displayError;

        // AnnotatedMap
        AnnotatedMapManager annotatedMapManager =
                new AnnotatedMapManager(graph, tileManager, bean, errorConsumer);
        Pane annotatedPane = annotatedMapManager.pane();

        // ElevationProfile
        ElevationProfileManager elevationProfileManager = new ElevationProfileManager(
                bean.getElevationProfile(), bean.highlightedPositionProperty());
        Pane elevationPane = elevationProfileManager.pane();
        SplitPane.setResizableWithParent(elevationPane, false);

        // Panneau central.
        SplitPane centralPane = new SplitPane(annotatedPane);
        centralPane.setOrientation(Orientation.VERTICAL);

        // Stack
        StackPane stackPane = new StackPane();
        ObservableList<Node> list = stackPane.getChildren();
        list.add(centralPane);
        list.add(errorPane);

        // Menu
        Menu menu = new Menu(FILE_MENU);
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().add(menu);
        MenuItem menuItem = new MenuItem(EXPORT_TEXT);
        menuItem.setOnAction(e -> export(bean));
        menu.getItems().add(menuItem);
        menuItem.disableProperty().set(true);

        BorderPane borderPane = new BorderPane(stackPane, menuBar,
                                    null, null, null);

        // Toute la fenêtre.
        addListeners(bean, centralPane, elevationPane, menuItem);

        // Lie la position mise en évidence entre
        // AnnotatedMapManager et elevationProfileManager
        bean.highlightedPositionProperty().bind(Bindings.
            when(annotatedMapManager.mousePositionOnRouteProperty().greaterThanOrEqualTo(0)).
            then(annotatedMapManager.mousePositionOnRouteProperty()).
            otherwise(elevationProfileManager.mousePositionOnProfileProperty()));

        primaryStage.setTitle(TITLE);
        primaryStage.setMinWidth(WINDOW_WIDTH);
        primaryStage.setMinHeight(WINDOW_HEIGHT);
        primaryStage.setScene(new Scene(borderPane));
        primaryStage.show();
    }

    /**
     * Ajout des auditeurs.
     *
     * @param routeBean conteneur d'itinéraire.
     * @param centralPane panneau contenant la carte et l'itinéraire.
     * @param elevationPane Panneau contenant l'itinéraire.
     * @param menuItem Menu défilant.
     */
    private void addListeners(RouteBean routeBean, SplitPane centralPane,
                              Pane elevationPane, MenuItem menuItem) {

        routeBean.getRoute().addListener((instance, oldValue, newValue) -> {
            if (oldValue == null && newValue != null) {
                // Ajout de la fenêtre.
                centralPane.getItems().add(elevationPane);
                menuItem.disableProperty().set(false);
            }

            if (oldValue != null && newValue == null) {
                // Suppression de la fenêtre.
                centralPane.getItems().remove(elevationPane);
                menuItem.disableProperty().set(true);
            }
        });
    }

    /**
     * Permet d'écrire les données d'un itinéraire dans un fichier GPX sur le disque.
     * Avec le nom de la constante EXPORTED_FILE_NAME.
     *
     * @param routeBean Itinéraire à exporter.
     */
    private void export(RouteBean routeBean) {
        GpxGenerator.writeGpx(EXPORTED_FILE_NAME, routeBean.getRoute().get(),
                routeBean.getElevationProfile().get());
    }
}
