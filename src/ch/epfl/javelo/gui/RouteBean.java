package ch.epfl.javelo.gui;

import ch.epfl.javelo.Preconditions;
import ch.epfl.javelo.routing.*;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import java.beans.Beans;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Conteneur pour les points de passages. Permet aussi de mettre un point en évidence.
 *
 * @author Antoine Cornaz (339526)
 */
public final class RouteBean extends Beans {

    // DEBUT des paramètres accessibles (getters/setters).
    private final ObservableList<Waypoint> waypoints;
    private final DoubleProperty highlightedPosition;

    // Itinéraire.
    private final ObjectProperty<Route> route;
    private final ObjectProperty<ElevationProfile> elevationProfile;
    // FIN des paramètres accessibles.

    private final RouteComputer routeComputer;

    // Cache avec des routes simples déjà chargé.
    private final Map<NodePair, Route> routeCache;

    private final static int MAX_CACHED_ROUTES = 40;
    private final static int MAX_DISTANCE_TWO_SAMPLES = 5;
    private final static int INITIAL_CIRCLE_POSITION = 0;

    /**
     * Constructeur d'un 'RouteBean'.
     *
     * @param routeComputer un calculateur d'itinéraire.
     * @throws IllegalArgumentException si le calculateur est 'null'.
     */
    public RouteBean (RouteComputer routeComputer) {
        // RouteComputer
        Preconditions.checkArgument(routeComputer != null);
        this.routeComputer = routeComputer;

        // Cache des itinéraires.
        this.routeCache = new LinkedHashMap<>(MAX_CACHED_ROUTES);

        // Liste vide de points de passage.
        this.waypoints = new SimpleListProperty<>(FXCollections.observableArrayList());

        // Met un profil vide.
        this.elevationProfile = new SimpleObjectProperty<>();

        // Met un itinéraire vide.
        this.route = new SimpleObjectProperty<>();

        // Met la position en évidence à la valeur de 0.
        this.highlightedPosition = new SimpleDoubleProperty(INITIAL_CIRCLE_POSITION);

        // Observe lorsque la liste change et calcul les nouvelles valeurs.
        this.waypoints.addListener((ListChangeListener<? super Waypoint>) e -> {
            Route newRoute = createRoad();
            Route oldRoute = this.route.get();

            // Vérifie si les 2 routes sont différentes
            if (newRoute != null && !newRoute.equals(oldRoute)) {
                this.route.set(newRoute);
                this.elevationProfile.set(computeElevationProfile(newRoute));
            } else {
                this.route.set(null);
            }
        });
    }

    /**
     * Permet de récupérer l'indice d'un segment non-nul d'une position donnée.
     *
     * @param position la position.
     * @return l'indice du segment.
     */
    public int indexOfNonEmptySegmentAt(double position) {
        int index = route.get().indexOfSegmentAt(position);
        for (int i = 0; i <= index; i += 1) {
            int node1 = waypoints.get(i).nodeId();
            int node2 = waypoints.get(i + 1).nodeId();
            if (node1 == node2) index += 1;
        }
        return index;
    }

    /**
     * Permet de récupérer la propriété contenant la position à mettre en évidence.
     *
     * @return une propriété contenant la position à mettre en évidence.
     */
    public DoubleProperty highlightedPositionProperty() {
        return highlightedPosition;
    }

    /**
     * Permet de récupérer la position mise en évidence.
     *
     * @return la position mise en évidence.
     */
    public double highlightedPosition() {
        return highlightedPosition.get();
    }

    /**
     * Permet de définir la position à mettre en évidence.
     *
     * @param highlightedPosition la position à mettre en évidence.
     */
    public void setHighlightedPosition(double highlightedPosition) {
        this.highlightedPosition.set(highlightedPosition);
    }

    /**
     * Permet d'obtenir la liste de points de passage.
     *
     * @return la liste observable de points de passage.
     */
    public ObservableList<Waypoint> getWaypoints() {
        return waypoints;
    }

    /**
     * Permet de récupérer la propriété contenant l'itinéraire.
     *
     * @return une propriété en lecture seule contenant l'itinéraire.
     */
    public ReadOnlyObjectProperty<Route> getRoute() {
        return route;
    }

    /**
     * Permet de récupérer une propriété contenant les données d'altitude de l'itinéraire.
     *
     * @return une propriété en lecture seule contenant le profile de l'itinéraire.
     */
    public ReadOnlyObjectProperty<ElevationProfile> getElevationProfile() {
        return elevationProfile;
    }

    /**
     * Permet de récupérer le calculateur de profil en long.
     *
     * @param route un itinéraire.
     * @return le calculateur correspondant.
     */
    private ElevationProfile computeElevationProfile(Route route) {
        // Si la route n'existe pas (p. ex. s'il y a moins de deux points de passage).
        if (this.route == null) return null;
        return ElevationProfileComputer.elevationProfile(route, MAX_DISTANCE_TWO_SAMPLES);
    }

    /**
     * Permet de créer l'itinéraire à afficher.
     *
     * @return l'itinéraire à afficher
     */
    private Route createRoad(){
        // 0. Regarder tous les chemins à calculer;
        // 1. Vérifier si chemin existe déjà;
        // 2. Créer un chemin qui n'existe pas et les ajouter dans la liste;
        // 3. Créer un itinéraire multiple des chemins existants.

        if (waypoints.size() < 2) return null;

        List<Route> routeList = new ArrayList<>();

        // i le départ, i + 1 l'arrivée.
        for (int i = 0; i < waypoints.size() - 1; i++) {
            int startWaypoint = waypoints.get(i).nodeId();
            int endWaypoint = waypoints.get(i + 1).nodeId();

            // Si 2 points de suites au même endroit.
            if (startWaypoint == endWaypoint) return null;
            NodePair nodePair = new NodePair(startWaypoint, endWaypoint);
            Route route = routeCache.get(nodePair);

            if (route == null) {
                // Si la route n'est pas dans le cache.
                // Crée la route.
                route = routeComputer.bestRouteBetween(startWaypoint, endWaypoint);

                if (route == null) return null;

                // Ajoute la route dans le cache.
                routeCache.put(nodePair, route);
            }

            // Ajoute la route dans la liste pour la multiRoute.
            routeList.add(route);
        }

        return new MultiRoute(routeList);
    }

    /**
     * Enregistrement permettant de vérifier si le noeud de départ et d'arrivée existent déjà dans
     * la liste.
     */
    private record NodePair(int startNode, int endNode){}
}
