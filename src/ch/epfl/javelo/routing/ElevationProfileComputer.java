package ch.epfl.javelo.routing;

import ch.epfl.javelo.Functions;
import ch.epfl.javelo.Preconditions;

import java.util.Arrays;
import java.util.List;
import java.util.function.DoubleUnaryOperator;


/**
 * Calculateur de profil en long. Cette classe permet de calculer le profil, c'est-à-dire
 * la hauteur de chaque point d'un itinéraire. Elle permettra l'affichage du graphique qui
 * montre le dénivelé.
 *
 * Cette classe est non-instantiable, et n'a donc pas de constructeur.
 *
 * @author Antoine Cornaz (339526)
 */

public final class ElevationProfileComputer {

    private ElevationProfileComputer(){}

    /**
     * Calculateur de profil en long. Contient le code permettant le calcul du profil en long
     * d'un itinéraire donné.
     *
     * @param route l'itinéraire.
     * @param maxStepLength la distance maximale entre deux échantillons.
     * @return le profil en long de l'itinéraire.
     * @throws IllegalArgumentException si la distance entre les échantillons est négative ou nulle,
     *                                  si la liste d'arêtes est vide.
     */
    public static ElevationProfile elevationProfile(Route route, double maxStepLength){
        Preconditions.checkArgument(maxStepLength > 0);

        if(route == null) return null;

        List<Edge> edgeList = route.edges();
        Preconditions.checkArgument(!edgeList.isEmpty());

        final double routeLength = route.length();
        final int totalSamples = (int) (1 + Math.ceil(routeLength/maxStepLength));
        final double samplesSpacing = routeLength / (totalSamples-1);

        float[] samples = new float[totalSamples];
        for (int i = 0; i < totalSamples; i++){
            samples[i] = (float) route.elevationAt(i * samplesSpacing);
        }

        if(!fillStart(samples)){
            fillEnd(samples);
            fillMiddle(samples);
        }

        return new ElevationProfile(routeLength,samples);
    }

    /**
     * Remplace les valeurs "Float.NaN" au début du tableau au début par la première valeur qui
     * existe. Si aucune valeur n'existe, on remplace par 0.
     *
     * @param samples tableau à compléter.
     * @return est-ce que le tableau était vide.
     */
    private static boolean fillStart(float[] samples){
        int totalSamples = samples.length;
        int i = 0;

        // Mettre i à la valeur après le dernier null du début.
        while (i < totalSamples && Double.isNaN(samples[i])) i++;

        if (i >= totalSamples) {
            // S'il n'y a que des NaN, placer des 0.
            Arrays.fill(samples, 0);
            return true;
        } else {
            // S'il n'y a pas que des NaNs, i est après le dernier null.
            Arrays.fill(samples, 0, i, samples[i]);
        }
        return false;
    }

    /**
     * Remplace les valeurs "Float.NaN" à la fin du tableau par la dernière valeur qui existe.
     *
     * @param samples tableau à compléter.
     * @throws IllegalArgumentException s'il y a moins de deux échantillons.
     */
    private static void fillEnd(float[] samples){
        int totalSamples = samples.length;
        Preconditions.checkArgument(totalSamples>=2);
        int i = totalSamples - 1;

        // Mettre i à la valeur juste avant le premier NaN de la fin. Cette fois pas besoin de
        // vérifier qu'il n'y ait pas que des NaN car déjà fait avant.


        while (i > 0 && Float.isNaN(samples[i])) i--;

        Arrays.fill(samples, i, totalSamples, samples[i]);

    }

    /**
     * Remplace les valeurs "Float.NaN" au milieu du tableau par la droite passant par
     * les points aux 2 extremités du trou.
     * Exemple: 1, *, *,4 → 1, 2, 3, 4
     * Problème si aucune valeure n'existe.
     * @param samples Tableau à compléter.
     * @throws IllegalArgumentException s'il y a moins de deux échantillons.
     */
    private static void fillMiddle(float[] samples){
        int totalSamples = samples.length;
        Preconditions.checkArgument(totalSamples >= 2);

        // Le 0 n'est pas un trou, car deja vérifié.
        int i = 1;
        int start = -100;
        int end;
        boolean isHole = false;

        // Le dernier n'est pas un trou, car déjà vérifié.
        while (i < totalSamples){
            // Vérifie si on passe d'un trou à un autre pas.
            if (Float.isNaN(samples[i]) != isHole){
                isHole = !isHole;
                // Début d'un trou
                if (isHole) start = i;
                // Fin d'un trou
                else {
                    end=i;
                    fill(start, end, samples);
                }
            }
            i++;
        }
    }

    /**
     * Remplace les valeurs "Float.NaN" au milieu du tableau par la droite passant par points
     * aux deux extrêmités du trou.
     * Exemple: 1, *, *,4 → 1, 2, 3, 4
     *
     * @param start valeur de la première sample avec comme valeur NaN.
     * @param end valeur de la première sample après NaN.
     * @param samples Tableau à compléter.
     * @throws IllegalArgumentException si l'intervalle est invalide.
     */
    private static void fill(int start, int end, float[] samples){
        Preconditions.checkArgument(start > 0);
        Preconditions.checkArgument(end > 1);
        Preconditions.checkArgument(start < samples.length-1);
        Preconditions.checkArgument(end < samples.length);

        // Début est la premiere valeur où il y a un NaN et de la valeur apreès le dernier NaN.
        float[] samples2 = new float[]{samples[start-1], samples[end]};
        double xMax = (end - start + 1);

        Preconditions.checkArgument(!Float.isNaN(samples2[0]));
        Preconditions.checkArgument(!Float.isNaN(samples2[1]));

        // Création d'une droite affine pour combler le trou.
        DoubleUnaryOperator sampled = Functions.sampled(samples2,xMax);

        for (int j = start; j < end; j++){
            // Affecter la droite affine aux points.
            samples[j] = (float) sampled.applyAsDouble(j - start + 1);
        }
    }
}