package ch.epfl.javelo.routing;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.projection.PointCh;

import java.util.function.DoubleUnaryOperator;

/**
 * Enregistrement représentant une arête d'un itinéraire.
 *
 * @author Sébastien Tille (313220)
 */
public record Edge(int fromNodeId, int toNodeId, PointCh fromPoint, PointCh toPoint,
                   double length, DoubleUnaryOperator profile) {

    /**
     * Méthode permettant de faciliter la création d'une instance d'Edge.
     *
     * @param graph un graphe.
     * @param edgeId une arête identité.
     * @param fromNodeId l'identité du noeud de départ.
     * @param toNodeId l'identité du noeud d'arrivée.
     * @return une instance d'Edge avec les paramètres précédents.
     */
    public static Edge of(Graph graph, int edgeId, int fromNodeId, int toNodeId) {
        PointCh fromPoint = graph.nodePoint(fromNodeId);
        PointCh toPoint = graph.nodePoint(toNodeId);
        double length = graph.edgeLength(edgeId);
        DoubleUnaryOperator profile = graph.edgeProfile(edgeId);

        return new Edge(fromNodeId, toNodeId, fromPoint, toPoint, length, profile);
    }

    /**
     * Calcul et donne la position le long de l'arête la plus proche du point donné.
     *
     * @param point un point au format suisse.
     * @return la position la plus proche.
     */
    public double positionClosestTo(PointCh point) {
        double ax = fromPoint.e();
        double ay = fromPoint.n();
        double bx = toPoint.e();
        double by = toPoint.n();
        double px = point.e();
        double py = point.n();

        return Math2.projectionLength(ax, ay, bx, by, px, py);
    }

    /**
     * Permet de trouver le point, dans le système suisse, se trouvant à la position donnée le
     * long de l'arête.
     *
     * @param position la position en mètres.
     * @return le point en cette position.
     */
    public PointCh pointAt(double position) {
        // Si la position est à un tiers de l'arête, alors les coordonnées E et N sont à un tiers
        // entre celles des points de départ et d'arrivée.


        double eStart = fromPoint.e();
        double nStart = fromPoint.n();
        double eEnd = toPoint.e();
        double nEnd = toPoint.n();

        // On a besoin d'un "ratio" de la position par rapport à la distance totale entre les
        // deux points.
        double ratio = position / length;

        // On calcule les nouveaux E et N.
        double eNew = Math2.interpolate(eStart, eEnd, ratio);
        double nNew = Math2.interpolate(nStart, nEnd, ratio);

        return new PointCh(eNew, nNew);
    }

    /**
     * Permet de trouver l'altitude à une certaine position le long de l'arête.
     *
     * @param position la position le long de l'arête, en mètres.
     * @return l'altitude à cette position.
     */
    public double elevationAt(double position) {
        return profile.applyAsDouble(position);
    }
}
