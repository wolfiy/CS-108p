package ch.epfl.javelo.routing;

import ch.epfl.javelo.Preconditions;
import ch.epfl.javelo.data.Graph;
import ch.epfl.javelo.projection.PointCh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Classe permettant de calculer le meilleur itinéraire, suivant la fonction de couts donnée. Elle
 * est immuable.
 *
 * @author Antoine Cornaz (339526)
 */
public final class RouteComputer {
    private final Graph graph;
    private final CostFunction costFunction;

    // Ex: distance[5] = 8,  id:5, distance:8.
    // Toutes les distances des noeuds triés par leur identité.
    private final double[] distanceNodes; // distance N dans l'exemple.
    // Tous les prédécesseurs des noeuds triés par leur identité.
    private final int[] predecessor;

    // Utilisé pour représenter les distances inconnues.
    private final static double UNKNOWN_DIST = Double.NEGATIVE_INFINITY;
    // Utilisé pour représenter une distance lointaine.
    private final static double FARTHEST_DIST = Double.POSITIVE_INFINITY;

    /**
     * Construction d'un calculateur d'itinéraire.
     *
     * @param graph Ensemble de noeuds et d'arêtes de toute la Suisse
     * @param costFunction Fonction des couts. Dépends du type de route.
     */
    public RouteComputer(Graph graph, CostFunction costFunction){
        this.graph = graph;
        this.costFunction = costFunction;
        distanceNodes = new double[graph.nodeCount()];
        predecessor = new int[graph.nodeCount()];
    }

    /**
     * Cherche le meilleur itinéraire entre 2 points donnés.
     * Selon les couts de fonctions.
     *
     * @param startNodeId noeud de départ.
     * @param endNodeId noeud d'arrivé.
     * @throws IllegalArgumentException si le noeud d'arrivée et de départ sont le même,
     *                                  si l'identité des noeuds sont inférieures au nombre
     *                                      de noeuds.
     * @return la meilleure 'SingleRoute' en fonction du type de chemin.
     */
    public Route bestRouteBetween(int startNodeId, int endNodeId){
        Preconditions.checkArgument(startNodeId != endNodeId);

        final int numberOfNodes = graph.nodeCount();

        Preconditions.checkArgument(startNodeId <= numberOfNodes);
        Preconditions.checkArgument(endNodeId <= numberOfNodes);

        // DEBUT de l'initialisation.
        // Remplis les tableaux et mets les éléments à +infini et 0.
        for (int i = 0; i < numberOfNodes; ++i){
            distanceNodes[i] = FARTHEST_DIST;
        }

        // Distance du noeud initial à 0.
        distanceNodes[startNodeId] = 0;
        //FIN de l'initialisation.

        // List d'identité des noeuds à explorer.
        PriorityQueue<WeightedNode> exploring = new PriorityQueue<>();

        // Initialise distanceNodes.
        exploring.add(new WeightedNode(startNodeId,0));

        // Coeur de l'algorithme.
        while (!exploring.isEmpty()){
            if (exploration(exploring, endNodeId)) break;
        }

        // Si pas de chemin trouvé.
        if (predecessor[endNodeId] == 0) return null;

        // Construction du chemin depuis la fin.
        List<Edge> edgeList = routeBuilding(startNodeId, endNodeId);

        return new SingleRoute(edgeList);
    }

    /**
     * Construction du chemin depuis la fin.
     *
     * @param startNodeId Noeud de départ.
     * @param endNodeId Noeud de fin.
     */
    private List<Edge> routeBuilding(int startNodeId, int endNodeId) {
        // Liste des arêtes du chemin.
        List<Edge> edgeList = new ArrayList<>();

        while (endNodeId != startNodeId){
            int fromNodeId = predecessor[endNodeId];

            // Cherche l'identité de l'arête;
            int edgeId = -1;
            for(int i = 0; i < graph.nodeOutDegree(fromNodeId); ++i){
                int edgeIdTry = graph.nodeOutEdgeId(fromNodeId, i);
                int nodeIdTry = graph.edgeTargetNodeId(edgeIdTry);
                if (nodeIdTry == endNodeId) edgeId = edgeIdTry;
            }

            // Erreur arête id pas trouvée.
            assert edgeId != -1;


            Edge edge = Edge.of(graph, edgeId, fromNodeId, endNodeId);
            edgeList.add(edge);
            endNodeId = fromNodeId;
        }

        Collections.reverse(edgeList);

        return edgeList;
    }


    /**
     * Explore les noeuds reliés au noeud de la list en_exploration.
     * Remplis cette liste des nouveaux noeuds.
     *
     * @param en_exploration liste d'identité des noeuds.
     * @param endNodeId identité du noeud de destination.
     */
    private boolean exploration(PriorityQueue<WeightedNode> en_exploration, int endNodeId){
        /*
         Cherche le noeud avec la distance la plus petite en vol d'oiseau + distanceParcourue

         3 points
         - arrive
         - proche
         - actuel

         2 distances
         - vol d'oiseau entre arrivée et proche, arrivée et actuel.
         - distance depuis le depart, distance deja effectué pour arriver au point.
        */

        // Enlève le plus petit de la liste en_exploration.
        int closestNodeId = en_exploration.remove().nodeId;

        // Point arrivées
        PointCh endPoint = graph.nodePoint(endNodeId);

        // Si noeud le plus proche est l'arrivée alors, fin de la fonction exploration.
        // Fin de l'algorithme, le chemin le plus court a été trouvé.
        if (closestNodeId == endNodeId) return true;

        double distance = distanceNodes[closestNodeId];
        if (distance == UNKNOWN_DIST){
            // Noeud deja exploré.
            return false;
        }

        // Regarde toutes les arêtes sortantes du noeud et les explores.
        for (int i = 0; i < graph.nodeOutDegree(closestNodeId); i++) {
            int edgeId = graph.nodeOutEdgeId(closestNodeId, i);
            explorationEdge(closestNodeId, edgeId, en_exploration, distance, endPoint);
        }

        // Met la distance minimale pour ne plus être exploré.
        distanceNodes[closestNodeId] = UNKNOWN_DIST;

        // Algorithme pas fini.
        return false;
    }

    /**
     * Explore une arête et ajoute dans exploration les nouveaux noeuds.
     *
     * @param idEdge identité de l'arête à explorer.
     * @param exploringQueue liste d'identité des noeuds.
     * @param distance du noeud de départ.
     */
    private void explorationEdge(int PredecessorNodeId, int idEdge,
                                 PriorityQueue<WeightedNode> exploringQueue,
                                 double distance, PointCh endPoint) {

        // N'
        int endNodeId = graph.edgeTargetNodeId(idEdge);

        // Distance; d
        double addedDistance = graph.edgeLength(idEdge);
        double factor = costFunction.costFactor(PredecessorNodeId, idEdge);

        // Distance artificielle
        distance += factor * addedDistance;

       if (distance < distanceNodes[endNodeId]){
           distanceNodes[endNodeId] = distance;
           predecessor[endNodeId] = PredecessorNodeId;

           PointCh actualPoint = graph.nodePoint(endNodeId);
           double flyingBirdDistance = actualPoint.distanceTo(endPoint);
           double totalDistance = distance + flyingBirdDistance;

           exploringQueue.add(new WeightedNode(endNodeId, (float) totalDistance));
       }
    }

    /**
     * Noeud avec un poids qui correspond à la distance.
     *
     * @param distance poids du noeud.
     * @param nodeId Identité du noeud.
     * @see java.lang.Comparable
     */
    private record WeightedNode(int nodeId, float distance)
            implements Comparable<WeightedNode> {

        /**
         *{@inheritdoc}
         */
        @Override
        public int compareTo(WeightedNode that) {
            return Float.compare(this.distance, that.distance);
        }
    }
}