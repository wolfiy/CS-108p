package ch.epfl.javelo.routing;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Locale;

/**
 * Classe non instantiable permettant la création d'itinéraires au format '.gpx'.
 *
 * @author Sébastien Tille (313220)
 */
public abstract class GpxGenerator {

    /**
     *  Permet de créer la structure d'un fichier GPX contenant les informations sur une 'Route'
     *  et d'y affecter les données correspondantes.
     *
     * @param route un itinéraire.
     * @param profile le profile de l'itinéraire.
     * @return un document contenant les données.
     */
    public static Document createGpx(Route route, ElevationProfile profile) {
        Document doc = newDocument();

        Element root = doc.createElementNS("http://www.topografix.com/GPX/1/1",
                                           "gpx");
        doc.appendChild(root);

        root.setAttributeNS("http://www.w3.org/2001/XMLSchema-instance",
                            "xsi:schemaLocation",
                            "http://www.topografix.com/GPX/1/1 "
                            + "http://www.topografix.com/GPX/1/1/gpx.xsd");
        root.setAttribute("version", "1.1");
        root.setAttribute("creator", "JaVelo");

        Element metadata = doc.createElement("metadata");
        root.appendChild(metadata);

        Element name = doc.createElement("name");
        metadata.appendChild(name);
        name.setTextContent("Route JaVelo");

        // Itérer sur les routes
        Element rte = doc.createElement("rte");
        root.appendChild(rte);

        // La liste d'arêtes
        var edges = route.edges();
        double totalLength = 0;
        int i = 0;

        // Itération sur chacun des points de l'itinéraire.
        for (var p : route.points()) {
            Element rtept = doc.createElement("rtept");
            Element ele = doc.createElement("ele");

            rte.appendChild(rtept);
            rtept.appendChild(ele);

            rtept.setAttribute("lat",
                                String.format(Locale.ROOT, "%.5f", Math.toDegrees(p.lat())));
            rtept.setAttribute("lon",
                                String.format(Locale.ROOT, "%.5f", Math.toDegrees(p.lon())));
            ele.setTextContent(String.format(Locale.ROOT, "%.2f",
                               profile.elevationAt(totalLength)));

            if (i != edges.size()) totalLength += edges.get(i).length();
            ++i;
        }

        return doc;
    }

    /**
     * Permet d'écrire les données d'un itinéraire dans un fichier GPX sur le disque.
     *
     * @param filename le nom du fichier.
     * @param route un itinéraire.
     * @param profile le profile de l'itinéraire.
     */
    public static void writeGpx(String filename, Route route, ElevationProfile profile) {
        try {
            Document doc = createGpx(route, profile);
            Writer w = new PrintWriter(filename);
            Transformer transformer = TransformerFactory.newDefaultInstance()
                                                        .newTransformer();

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(doc), new StreamResult(w));
        } catch (IOException | TransformerException e) {
            throw new Error(e);
        }
    }

    /**
     * Permet de créer un nouveau document.
     *
     * @return un nouveau document.
     */
    private static Document newDocument() {
        try {
            return DocumentBuilderFactory.newDefaultInstance()
                                         .newDocumentBuilder()
                                         .newDocument();
        } catch (ParserConfigurationException e) {
            throw new Error(e);
        }
    }
}
