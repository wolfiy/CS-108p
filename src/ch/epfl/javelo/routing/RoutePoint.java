package ch.epfl.javelo.routing;

import ch.epfl.javelo.projection.PointCh;


/**
 * Cet enregistrement représente le point d'itinéraire le plus proche d'un point de référence donné.
 *
 * @author Antoine Cornaz (339526)
 */
public record RoutePoint(PointCh point, double position, double distanceToReference) {

    /** Point inexistant*/
    public final static RoutePoint NONE =
            new RoutePoint(null, Double.NaN, Double.POSITIVE_INFINITY);

    /**
     * Crée un nouveau point décalé d'une distance donnée.
     *
     * @param positionDifference décalage positif ou négatif en mètres.
     * @return Un nouveau RoutePoint décalé. Le point est toujours sur le chemin.
     */
    public RoutePoint withPositionShiftedBy(double positionDifference){
       return new RoutePoint(point, position + positionDifference, distanceToReference);
    }

    /**
     * Retourne le RoutePoint le plus proche entre les deux 'RoutePoint' existants.
     *
     * @param that Le second RoutePoint, le premier étant l'instance.
     * @return Le point le plus proche du chemin.
     */
    public RoutePoint min(RoutePoint that){
        if (this.distanceToReference <= that.distanceToReference()) {
            return this;
        } else {
            return that;
        }
    }

    /**
     * Retourne le RoutePoint le plus proche entre les deux 'RoutePoint' existants.
     *
     * @param thatPoint le second point.
     * @param thatPosition la position sur la route.
     * @param thatDistanceToReference la distance entre le point et le chemin.
     * @return le point le plus proche du chemin.
     */
    public RoutePoint min(PointCh thatPoint, double thatPosition, double thatDistanceToReference) {
        if (this.distanceToReference <= thatDistanceToReference) {
            return this;
        } else {
            return new RoutePoint(thatPoint, thatPosition, thatDistanceToReference);
        }
    }
}
