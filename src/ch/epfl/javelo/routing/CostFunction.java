package ch.epfl.javelo.routing;

/**
 * Interface qui donne un poids à chaque arête de l'itinéraire. Ce poids sert à déterminer la
 * praticité. Plus celui-ci est élevé, moins la route est agréable à vélo (pente raide par exemple),
 * et plus il est faible, meilleure elle est (piste cyclable). Si une route est inatteignable à vélo
 * (comme une autoroute), ce poids est infini. En théorie, celles-ci ne sont pas utilisées.
 *
 * @author Antoine Cornaz (339526)
 */
public interface CostFunction {
    double costFactor(int nodeId, int edgeId);
}
