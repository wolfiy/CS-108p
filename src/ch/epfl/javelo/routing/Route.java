package ch.epfl.javelo.routing;

import ch.epfl.javelo.projection.PointCh;

import java.util.List;

/**
 * Cette interface représente un itinéraire. Il est utilisé pour implémenter les notions
 * d'itinéraire simple (lien entre deux points) et multiple (avec des points intermédiaires).
 *
 * @author Sébastien Tille (313220)
 */
public interface Route {

    /**
     * Permet d'obtenir l'index de l'arête du segment à la position donnée.
     *
     * @param position une position.
     * @return l'index du segment en cette position.
     */
    int indexOfSegmentAt(double position);

    /**
     * Permet d'obtenir la longueur de l'itinéraire, en mètres.
     *
     * @return la longueur de l'itinéraire en mètres.
     */
    double length();

    /**
     * Permet d'obtenir une liste qui contient l'ensemble des arêtes de l'itinéraire.
     *
     * @return les arêtes de l'itinéraire.
     */
    List<Edge> edges();

    /**
     * Permet d'obtenir une liste contenant la totalité des points situés aux extrémités des
     * arêtes de l'itinéraire.
     *
     * @return les points aux extrémités de l'itinéraire.
     */
    List<PointCh> points();

    /**
     * Permet d'obtenir le point se trouvant à la position donnée le long de l'itinéraire.
     *
     * @param position la position.
     * @return le point en cette position.
     */
    PointCh pointAt(double position);

    /**
     * Permet d'obtenir l'altitude, en mètres, en une certaine position donnée.
     *
     * @param position la position.
     * @return l'altitude en cette position.
     */
    double elevationAt(double position);

    /**
     * Permet d'obtenir l'identité du noeud le plus proche d'une certaine position donnée.
     *
     * @param position la position.
     * @return le noeud le plus proche.
     */
    int nodeClosestTo(double position);

    /**
     * Permet d'obtenir le point de l'itinéraire se trouvant le plus proche du point de
     * référence donné.
     *
     * @param point le point.
     * @return le point de l'itinéraire le plus proche.
     */
    RoutePoint pointClosestTo(PointCh point);

}
