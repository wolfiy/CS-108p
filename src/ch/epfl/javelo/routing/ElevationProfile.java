package ch.epfl.javelo.routing;

import ch.epfl.javelo.Functions;
import ch.epfl.javelo.Preconditions;

import java.util.DoubleSummaryStatistics;
import java.util.function.DoubleUnaryOperator;

/**
 * Classe permettant de représenter le profil en long d'un itinéraire.
 * Celui-ci peut être simple ou multiple.
 *
 * @author Sébastien Tille (313220)
 */
public final class ElevationProfile {

    // Longueur en mètres du profil et tableau d'échantillons.
    private final double length;

    private final float[] elevationSamples;
    private final DoubleSummaryStatistics summaryStatistics;

    private final double totalAscent;
    private final double totalDescent;
    private final DoubleUnaryOperator elevation;

    /**
     * Constructeur de la classe 'ElevationProfile'. Il en créé une instance de longueur 'length'
     * donnée et répartit uniformément les échantillons sur cette distance.
     *
     * @param length longueur en mètres de l'itinéraire.
     * @param elevationSamples tableau contenant des échantillons d'altitude.
     * @throws IllegalArgumentException si la longueur est négative ou nulle ou
     *                                  si le tableau d'échantillon contient moins de deux éléments.
     */
    public ElevationProfile(double length, float[] elevationSamples) {
        Preconditions.checkArgument(length > 0);
        Preconditions.checkArgument(elevationSamples.length >= 2);

        summaryStatistics = new DoubleSummaryStatistics();
        for (float elevationSample : elevationSamples) {
            summaryStatistics.accept(elevationSample);
        }

        this.length = length;
        this.elevationSamples = elevationSamples.clone();

        this.totalAscent = calculateTotalAscent();
        this.totalDescent = calculateTotalDescent();

        this.elevation = calculElevation();
    }

    /**
     * Permet d'obtenir la longueur, en mètres, du profil.
     *
     * @return la longueur du profil en mètres.
     */
    public double length() {
        return length;
    }

    /**
     * Permet d'obtenir l'altitude minimal en mètres du profil.
     *
     * @return l'altitude minimale du profil en mètres.
     */
    public double minElevation() {
        return summaryStatistics.getMin();
    }

    /**
     * Permet d'obtenir l'altitude maximale en mètres du profil.
     *
     * @return l'altitude maximale du profil.
     */
    public double maxElevation() {
        return summaryStatistics.getMax();
    }

    /**
     * Permet d'obtenir le dénivelé positif total du profil.
     *
     * @return le dénivelé positif total.
     */
    public double totalAscent() {
        return totalAscent;
    }

    /**
     * Permet d'obtenir le dénivelé négatif total du profil.
     *
     * @return Le dénivelé négatif total (valeur absolue).
     */
    public double totalDescent() {
        return totalDescent;
    }

    /**
     * Permet d'obtenir l'altitude en une certaine position du profil. Si celle-ci est négative,
     * on retourne l'altitude du premier échantillon, et dans le cas où elle serait plus grande que
     * la distance, le dernier échantillon du tableau.
     *
     * @param position la position.
     * @return l'altitude en cette position.
     */
    public double elevationAt(double position) {
        return this.elevation.applyAsDouble(position);
    }

    private DoubleUnaryOperator calculElevation() {
        return Functions.sampled(elevationSamples, length);
    }


    /**
     * Calcul l'assention totale
     *
     * @return l'assention totale
     */
    private double calculateTotalAscent() {
        double ascent = 0;
        for (int i = 1; i < elevationSamples.length; ++i) {
            if (elevationSamples[i] > elevationSamples[i-1]) {
                ascent += (elevationSamples[i] - elevationSamples[i-1]);
            }
        }

        return ascent;
    }

    /**
     * Calcul la descente totale
     *
     * @return la descente totale
     */
    private double calculateTotalDescent() {
        double descent = 0;
        for (int i = 1; i < elevationSamples.length; ++i) {
            if (elevationSamples[i] < elevationSamples[i-1]) {
                descent += (elevationSamples[i-1] - elevationSamples[i]);
            }
        }

        return descent;
    }
}