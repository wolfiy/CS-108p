package ch.epfl.javelo.routing;

import ch.epfl.javelo.Math2;
import ch.epfl.javelo.Preconditions;
import ch.epfl.javelo.projection.PointCh;

import java.util.ArrayList;
import java.util.List;

import static ch.epfl.javelo.Math2.clamp;
import static java.lang.Math.abs;
import static java.util.Arrays.binarySearch;

/**
 * Classe représentant un itinéraire simple (c'est-à-dire qui ne relie que deux points).
 *
 * @author Sébastien Tille (313220)
 */
public final class SingleRoute implements Route {

    // La liste des arêtes et le tableau trié des longueurs.
    private final List<Edge> edges;
    private final double[] cumulLength;
    private final List<PointCh> points;

    /**
     * Permet de construire un itinéraire simple composé des arêtes passées en argument. La
     * liste de ces arêtes doit impérativement contenir au moins un élément.
     *
     * Ce constructeur créé aussi un tableau, indexé de 0 au nombre d'arêtes,
     * où chaque élément est la longueur de l'itinéraire jusqu'a l'arête de cet index.
     * Cela nous permet d'avoir un tableau trié par ordre croissant, dans
     * lequel il sera possible de plus rapidement chercher un noeud ou une position.
     *
     * @param edges liste contenant les arêtes de l'itinéraire.
     * @throws IllegalArgumentException si la liste d'arêtes est vide.
     */
    public SingleRoute(List<Edge> edges) {
        Preconditions.checkArgument(!edges.isEmpty());
        this.edges = List.copyOf(edges);

        cumulLength = new double[edges.size() + 1];
        cumulLength[0] = 0;
        for (int i = 1; i < edges.size() + 1; ++i) {
            cumulLength[i] = cumulLength[i - 1] + edges.get(i - 1).length();
        }
        this.points = initializePoints();
    }

    /**
     * Permet d'obtenir l'index de l'arête du segment à la position donnée. Dans le cas d'un
     * itinéraire simple comme ici, celui-ci sera toujours 0.
     *
     * @param position une position.
     * @return l'index du segment en cette position.
     */
    @Override
    public int indexOfSegmentAt(double position) {
        return 0;
    }

    /**
     * Permet d'obtenir la longueur de l'itinéraire, en mètres. Il s'agit de l'addition de la
     * longueur de toutes les arêtes bout-à-bout.
     *
     * @return la longueur de l'itinéraire en mètres.
     */
    @Override
    public double length() {
        return cumulLength[cumulLength.length - 1];
    }

    /**
     * Permet d'obtenir une copie de la liste qui contient l'ensemble des arêtes de l'itinéraire.
     *
     * @return les arêtes de l'itinéraire.
     */
    @Override
    public List<Edge> edges() {
        return edges;
    }

    /**
     * Permet d'obtenir la liste contenant la totalité des points situés aux extrémités
     * des arêtes de l'itinéraire.
     *
     * @return les points aux extrémités de l'itinéraire.
     */
    @Override
    public List<PointCh> points() {
        return points;
    }

    /**
     * Permet d'obtenir le point se trouvant à la position donnée le long de l'itinéraire.
     *
     * @param position la position.
     * @return le point en cette position.
     */
    @Override
    public PointCh pointAt(double position) {
        double pos = clamp(0, position, length());
        int index = findEdgeIndex(pos);
        return edges.get(index)
                    .pointAt(pos - cumulLength[index]);
    }

    /**
     * Permet d'obtenir l'altitude, en mètres, en une certaine position donnée.
     *
     * @param position la position.
     * @return l'altitude en cette position.
     */
    @Override
    public double elevationAt(double position) {
        double pos = clamp(0, position, length());
        int index = findEdgeIndex(pos);
        return edges.get(index)
                    .elevationAt(pos - cumulLength[index]);
    }

    /**
     * Permet d'obtenir l'identité du noeud le plus proche d'une certaine position donnée.
     *
     * @param position la position.
     * @return le noeud le plus proche.
     */
    @Override
    public int nodeClosestTo(double position) {
        double pos = clamp(0, position, length());
        int leftNodeIndex = findEdgeIndex(pos);

        Edge edge = edges.get(leftNodeIndex);

        // On trouve les noeuds qui encerclent la position.
        int leftNode = edge.fromNodeId();
        int rightNode = edge.toNodeId();

        return (pos - cumulLength[leftNodeIndex] <= edge.length()/2.0) ? leftNode : rightNode;
    }

    /**
     * Permet d'obtenir le point de l'itinéraire se trouvant le plus proche du point de
     * référence donné.
     *
     * @param point le point.
     * @return le point de l'itinéraire le plus proche.
     */
    @Override
    public RoutePoint pointClosestTo(PointCh point) {
        double pos;
        double dist = Double.MAX_VALUE;
        int index = -1;

        // On cherche la bonne arête, un peu comme pour nodeClosestTo.
        for (int i = 0; i < edges.size(); ++i) {
            // Calculer la distance entre le point 'point' et celui situé au début de l'arête.
            double closeDist = distanceToEdge(point, i);
            // Si cette distance est plus petite, on la garde pour la suite.
            if (closeDist < dist) {
                dist = closeDist;
                index = i;
            }
        }

        // Erreur: l'arête n'existe pas.
        assert index >= 0;

        PointCh pointClosestTo = closestPoint(point, index);
        PointCh startPoint = edges.get(index).fromPoint();

        pos = startPoint.distanceTo(pointClosestTo);
        pos += cumulLength[index];
        return new RoutePoint(pointClosestTo, pos, dist);
    }

    private double distanceToEdge(PointCh pointCh, int edgeId){
        PointCh endPoint = closestPoint(pointCh, edgeId);
        return endPoint.distanceTo(pointCh);
    }

    private PointCh closestPoint(PointCh pointCh, int edgeId) {
        Edge edge = edges.get(edgeId);
        double projection = edge.positionClosestTo(pointCh);
        projection = Math2.clamp(0, projection, edge.length());

        return edge.pointAt(projection);
    }

    /**
     * Permet de trouver l'index d'une arête de l'itinéraire.
     *
     * @param position la position sur l'itinéraire.
     * @return l'index de l'arête.
     */
    private int findEdgeIndex(double position){
        int edgeId = binarySearch(cumulLength, position);
        if (position == length()) edgeId = edgeId - 1;
        if (edgeId >= 0) return edgeId;
        else return abs(edgeId) - 2;
    }

    /**
     * Permet d'obtenir une liste contenant la totalité des points situés aux extrémités
     * des arêtes de l'itinéraire.
     *
     * @return les points aux extrémités de l'itinéraire.
     */
    private List<PointCh> initializePoints() {
        List<PointCh> pointsList = new ArrayList<>();

        // On prend le premier point (0) à part, car il y a un point de plus qu'il n'y a d'arêtes.
        // Sur la figure 5, c'est le point 0 en rouge.
        pointsList.add(edges.get(0).pointAt(0));

        // Pour le reste des points, on itère sur l'entièreté des arêtes, mais on récupère le point
        // d'arrivée. Sur la figure, cela correspond à récupérer les points 1 à 5.
        for (Edge edge : edges) {
            pointsList.add(edge.toPoint());
        }

        return List.copyOf(pointsList);
    }
}
