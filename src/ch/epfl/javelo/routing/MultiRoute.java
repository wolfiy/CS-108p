package ch.epfl.javelo.routing;

import ch.epfl.javelo.Preconditions;
import ch.epfl.javelo.projection.PointCh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ch.epfl.javelo.Math2.clamp;
import static java.lang.Math.abs;
import static java.util.Arrays.binarySearch;

/**
 * Classe représentant un itinéraire multiple, c'est-à-dire reliant plusieurs points.
 * Elle est immuable.
 *
 * @author Sébastien Tille (313220)
 */
public final class MultiRoute implements Route {

    // Liste contenant les segments de l'itinéraire et tableau des longueurs de ceux-ci.
    private final List<Route> segments;
    private final double[] cumulLenght;

    /**
     * Permet de construire un itinéraire multiple. Celui-ci est constitué
     * d'itinéraires simples ou multiples. Afin de simplifier la recherche d'index
     * (c.-à-d. savoir sur quel itinéraire nous nous trouvons) dans les méthodes plus bas,
     * un tableau contenant les longueurs de chaque segment est initialisé.
     *
     * @param segments la liste des segments de l'itinéraire.
     * @throws IllegalArgumentException si la liste de segment est vide.
     */
    public MultiRoute(List<Route> segments) {
        Preconditions.checkArgument(!segments.isEmpty());
        this.segments = List.copyOf(segments);

        cumulLenght = new double[segments.size() + 1];
        cumulLenght[0] = 0;
        for (int i = 1; i < segments.size() + 1; ++i) {
            cumulLenght[i] = cumulLenght[i - 1] + segments.get(i - 1).length();
        }
    }

    /**
     * Permet d'obtenir l'index de l'arête du segment à la position donnée.
     *
     * @param position une position.
     * @return l'index du segment en cette position.
     */
    @Override
    public int indexOfSegmentAt(double position) {
        // On vérifie que la position donnée est comprise entre 0 et la longueur
        // de la 'MultiRoute'.
        double pos = clamp(0, position, length());

        // On parcourt tous les segments de la liste, jusqu'à celui où se trouve la
        // position comprise. Pour chaque segment, on appelle récursivement
        // jusqu'à n'avoir plus que des 'SingleRoute', et on trouve la longueur
        // de chacunes d'elles, pour en déduire la nombre de sous-segments.
        // https://piazza.com/class/kzifjghz6po4se?cid=707

        int indexAt = 0;
        int i = 0;

        // Si l'index a été trouvé, la position relative doit être nulle ou négative.
        double relPos = pos;

        /*
         * Sur l'exemple de l'énoncé:
         * 1. Parcourir les segments pour trouver celui qui contient 5500.
         * 2.1. 1er segment trop petit → appeler segmentAt(longueur)
         * 2.2. Ajouter 2 + 1 à indexAt
         * 3. Position restante <= longueur du 2e segment → appeler sur le 2e
         *    avec 2500
         */
        do {
            // On prend les segments (SingleRoute) les uns après les autres.
            // On aura besoin de leur longueur pour trouver le nombre d'index.
            Route seg = segments.get(i);
            double segSize = seg.length();

            if (relPos > segSize) {
                indexAt += seg.indexOfSegmentAt(segSize) + 1;
                relPos -= segSize;
            }

            if (segSize >= relPos) {
                indexAt += seg.indexOfSegmentAt(relPos);
                break;
            }

            ++i;
        } while (relPos > 0);

        return indexAt;
    }

    /**
     * Permet d'obtenir la longueur de l'itinéraire, en mètres.
     *
     * @return la longueur de l'itinéraire en mètres.
     */
    @Override
    public double length() {
        double totalLength = 0;
        for (Route segment : segments) {
            totalLength += segment.length();
        }

        return totalLength;
    }

    /**
     * Permet d'obtenir une liste qui contient l'ensemble des arêtes de l'itinéraire.
     *
     * @return les arêtes de l'itinéraire.
     */
    @Override
    public List<Edge> edges() {
        List<Edge> edgesList = new ArrayList<>();
        for (Route segment : segments) {
            edgesList.addAll(segment.edges());
        }
        return Collections.unmodifiableList(edgesList);
    }

    /**
     * Permet d'obtenir une liste contenant la totalité des points situés
     * aux extrémités des arêtes de l'itinéraire.
     *
     * @return les points aux extrémités de l'itinéraire.
     */
    @Override
    public List<PointCh> points() {
        List<PointCh> pointsList = new ArrayList<>();

        // On ajoute tous les points, en ignorant les doublons.
        for (Route segment : segments) {
            pointsList.addAll(segment.points());
        }

        // Trouver les doublons et les enlever.
        PointCh checkIt;
        for (int i = 1; i < segments.size(); ++i) {
            checkIt = segments.get(i).pointAt(0);
            pointsList.remove(checkIt);
        }

        return pointsList;
    }

    /**
     * Permet d'obtenir le point se trouvant à la position donnée le long de l'itinéraire.
     *
     * @param position la position.
     * @return le point en cette position.
     */
    @Override
    public PointCh pointAt(double position) {
        double pos = clamp(0, position, length());
        int index = search(pos);
        pos -= cumulLenght[index];
        return segments.get(index).pointAt(pos);
    }

    /**
     * Permet d'obtenir l'altitude, en mètres, en une certaine position donnée.
     *
     * @param position la position.
     * @return l'altitude en cette position.
     */
    @Override
    public double elevationAt(double position) {
        double pos = clamp(0, position, length());
        int index = search(pos);
        return segments.get(index).elevationAt(pos - cumulLenght[index]);
    }

    /**
     * Permet d'obtenir l'identité du noeud le plus proche d'une certaine position donnée.
     *
     * @param position la position.
     * @return le noeud le plus proche.
     */
    @Override
    public int nodeClosestTo(double position) {
        double pos = clamp(0, position, length());
        int index = indexOfSegmentAt(pos);
        return segments.get(index).nodeClosestTo(pos - cumulLenght[index]);
    }

    /**
     * Permet d'obtenir le point de l'itinéraire se trouvant
     * le plus proche du point de référence donné.
     *
     * @param point le point.
     * @return le point de l'itinéraire le plus proche.
     */
    @Override
    public RoutePoint pointClosestTo(PointCh point) {

        // D'abord on itère sur les différentes 'Route', pour voir laquelle est
        // la plus proche du point donné.
        // Idée: https://piazza.com/class/kzifjghz6po4se?cid=639

        double addedLength = 0;
        RoutePoint min = RoutePoint.NONE;
        for (Route segment : segments) {
            min = segment.pointClosestTo(point).withPositionShiftedBy(addedLength).min(min);
            addedLength += segment.length();
        }

        return min;
    }

    /**
     * Cherche l'index su segment, comme dans 'SingleRoute'.
     *
     * @param position la position sur l'itinéraire.
     * @return l'index.
     */
    private int search(double position) {
        double pos = clamp(0, position, length());
        int routeIndex = binarySearch(cumulLenght, pos);
        if (pos == length()) routeIndex = routeIndex - 1;
        if (routeIndex >= 0) return routeIndex;
        else return abs(routeIndex) - 2;
    }
}
