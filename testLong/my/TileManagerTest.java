package my;

import ch.epfl.javelo.gui.TileManager;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TileManagerTest {
    @Test
    void testChargerImage3() {
        //dosier où sont stocké les tuiles
        Path path = Path.of("tuile");
        String serverName = "https://tile.openstreetmap.org";

        // Charge les 105 premieres tuiles
        final int tuileSize = 105;
        TileManager tileManager = new TileManager(path, serverName);

        for (int i = 0; i < tuileSize; ++i) {
            assertNotNull(tileManager.imageForTileAt(new TileManager.TileId(19, 0, i)));
        }

        // Charge depuis la 10e jusqu'a la premiere. S'il y a un C ou un b qui apparait, c'est faux.
        // Normalement des a, A et b devraient apparaitre.
        for (int i = 10; i > 0; --i) {
            assertNotNull(tileManager.imageForTileAt(new TileManager.TileId(19, 0, i)));
        }

    }
}
