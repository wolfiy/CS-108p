# JaVelo - Projet de PPOO
JaVelo est une application permettant de générer un itinéraire adapté au vélo en Suisse. Il 
s'agit du projet du cours de pratique de la programmation orienté objet de l'EPFL, année 2022. 
([english](readme-en.md)).
![Application](javelo.png)
**Remarque:** les données fournies dans le dossier "osm-data" ne couvrent malheureusement que la 
partie ouest du territoire suisse; nous mettrons à jour les données lorsque nous aurons accès à 
l'entièreté des données.

## Fonctionnalités
Il est possible de générer un itinéraire en cliquant à deux endroits de la carte. Un panneau 
contenant le profil de cet itinéraire s'ouvrira alors affichant la longueur du chemin et diverses 
informations concernant l'altitude. Il est possible de mettre en évidence un point en plaçant le 
curseur sur celui-ci (cf. ligne noire et cercle blanc sur l'image). 

Le point de départ est en vert, le point d'arrivée en rouge. Il est bien entendu possible d'ajouter 
plusieurs points de passage qui seront alors bleus. Si un point est placé le long de 
l'itinéraire, celui-ci deviendra automatiquement un point intermédiaire. Les points peuvent être 
déplacés en glissant.

La carte peut évidemment être déplacée en la glissant et le niveau de zoom peut être 
modifié à l'aide de la molette. L'itinéraire peut être exporté à l'aide du menu "Fichier".

L'énoncé du projet se trouve à [cette adresse](https://cs108.epfl.ch/archive/22/archive.html) (sous "Projet").

## Execution du programme
Il vous faudra JavaFX 18. Ensuite, modifiez les configurations (_edit configurations..._) afin 
qu'elles soient similaires aux suivantes:
```
--module-path /Users/wolfiy/software/java/javafx-sdk-18/lib/ --add-modules javafx.controls
```
et lancer le programme.
Pour plus d'information: [installation de JavaFX](https://cs108.epfl.ch/archive/22/g/openjfx.html).
